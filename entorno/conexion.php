<?php
    require('configuracion.php');
    class Conexion{
        
        public $conn = null;
        public $recordSet = null;
        public $sentenciaSql = null;
        private static $instancia = null;
        private $message = null;
        
        function __construct() {        
            $this->conn=mysqli_connect(SERVERNAME,USER,PASSWORD,DATABASE);         
            
            if (!$this->conn) {
                //$this->message = mysqli_error_list();			
                $error = $this->obtenerError();			
                throw new Exception('No fue posible conectar a la base de datos: ', E_USER_ERROR);
            }
        }
        
        public function preparar($sentenciaSql){
            $this->sentenciaSql = $sentenciaSql;
        }

        public function ejecutar() {        
            $this->recordSet = mysqli_query($this->conn, $this->sentenciaSql);       
            if(!$this->recordSet){            
                $error = $this->obtenerError();
                throw new Exception('No fue posible guardar la informacion. '.$error['message'], E_USER_ERROR);
            }
            return $this->recordSet;
        }
        
        public function obtenerObjeto() {
            return mysqli_fetch_object($this->recordSet);
        }
        
        public function obtenerArray() {
            return mysqli_fetch_array($this->recordSet);
        }

        public function obtenerRegistros(){
            return mysqli_fetch_all($this->recordSet);
        }
        
        public function obtenerRow() {
            return mysqli_fetch_row($this->recordSet);
        }
        
        public function obtenerNumeroRegistros(){
            return mysqli_num_rows($this->recordSet);
        }

        public function obtenerUltimoId(){
            return mysqli_insert_id($this->recordSet);
        }

        public function begin_transaction($estado){
            $this->conn->begin_transaction($estado);
        }

        public function commit(){
            $this->conn->commit();
        }

        public function rollback(){
            $this->conn->rollback();
        }

        public static function singleton(){
            if (!isset(self::$instancia)){
            $miclase = __CLASS__;
            self::$instancia = new $miclase;
            }
            return self::$instancia;
            }
        
        public function obtenerNombreColumnas(){
            $arrNombreColumnas = array();
            foreach(mysqli_fetch_field($this->recordSet) as $fieldData) {    
                $nombreColumna = $fieldData['Name'];
                $arrNombreColumnas[] = $nombreColumna;
            }
            return $arrNombreColumnas;
        }
        private function obtenerError(){
            $resultado = array();
            $errors = mysqli_error($this->conn);
            if( ($errors) != null){
                var_dump("Texto Error: ".$errors);
                if(is_array($errors)){
                    foreach( $errors as $error){
                        $resultado['SQLSTATE'] = $error[ 'SQLSTATE'];
                        $resultado['code'] = $error[ 'code'];
                        $resultado['message'] = $error[ 'message'];
                    }    
                }
                else{
                    $resultado['message'] = $errors;
                }            
            }
            return $resultado;
        }
        function __destruct() {
            if ($this->conn)
                mysqli_close($this->conn);
        }
        
    }
?>
