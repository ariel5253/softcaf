<?php
    require_once '../entorno/conexion.php';
    require '../modelo/guiasAprendizaje.M.php';
    require '../modelo/administrador/personas.M.php';
    require '../modelo/administrador/resultadoAprendizaje.M.php';
    require '../modelo/administrador/resultadosGuias.M.php';
    $respuesta = array();
    if (isset ($_POST['hidAccion'])){   
        switch ($_POST['hidAccion']) {  
            case 'ADICIONAR':
                try{
                    //Consultar
                    $consul = new GuiasAprendizaje();
                    $resul = $consul->ultimo_id();
                    if ($rowBuscar = $consul->conn->obtenerObjeto()){ 
                        $idGuia = $rowBuscar->id;
                    }

                    //subir guia
                    $nombreArchivo = $_POST["txtNombre"];
                    $archivo = $_FILES["txtFileGuia"];
                    $parts = explode(".",$_FILES['txtFileGuia']['name']);
                    $extension = end($parts);
                    $dir_subida = "../archivos/guias/";
                    $fichero_temporal = $archivo['tmp_name'];
                    $fichero_subido = $dir_subida.$nombreArchivo."_".$idGuia.".".$extension;

                    //Guia de aprendizaje
                    $guia = new GuiasAprendizaje();
                    $guia->setIdGuiasAprendizajePk($_POST['hidIdGuia']);
                    $guia->setNombre($_POST['txtNombre']);
                    $guia->setPalabrasClave($_POST['txtPalabras']);
                    $guia->setUrl($fichero_subido);
                    $guia->setIdFichaFk($_POST['cmbFicha']);
                    $guia->setIdUsuarioCreacion($_POST['hidIdSesion']);
                    $guia->setIdUsuarioActualizacion($_POST['hidIdSesion']);
                    $guia->setIdCompetenciaFk($_POST['cmbComp']);
                    $resultado1 = $guia->agregar();

                    
                    $rapGui = new ResultadosGuias();

                    $cantidadRap = $_POST['cmbRap'];
                    for ($i =0;$i<count($cantidadRap);$i++){
                        $rapGui->setIdResultadosAprendizajeFk($cantidadRap[$i]);
                        $rapGui->setIdGuiasAprendizajeFk($idGuia);
                        $rapGui->setIdUsuarioCreacion($_POST['hidIdSesion']);
                        $rapGui->setIdUsuarioActualizacion($_POST['hidIdSesion']);
                        $resultadoRap = $rapGui->agregar();
                    }

                    

                    if ($resultado1 = true) {
                        // Guarda en BD URL: $fichero_subido
                        if (is_uploaded_file($fichero_temporal)) {
                            move_uploaded_file($fichero_temporal,$fichero_subido);
                            $respuesta['respuesta'] = "La información se adicionó correctamente.";
                        }
                    }

                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible adicionar la información, consulte con el administrador.";
                }
                //Respuesta del retorno
                $respuesta['accion']='ADICIONAR';
                echo json_encode($respuesta);
            break;
            case 'CONSULTAR':
                try{
                    
                    $guia = new GuiasAprendizaje();
                    $guia->setIdGuiasAprendizajePk($_POST['hidIdGuia']);
                    $resultado = $guia->consultar();
                    $numeroRegistros = $guia->conn->obtenerNumeroRegistros();
                    $respuesta['numeroRegistros']=$numeroRegistros;

                    if($numeroRegistros === 1){
                        $rowBuscar=$guia->conn->obtenerObjeto();
                        $respuesta['datos']=$rowBuscar;
                        $respuestas=$rowBuscar;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='CONSULTAR';
                echo json_encode($respuesta);
            break;
            case 'MODIFICAR':
                try{

                    $guia = new GuiasAprendizaje();
                    $guia->setIdGuiasAprendizajePk($_POST['hidIdGuia']);
                    $guia->setNombre($_POST['txtNombre']);
                    $guia->setPalabrasClave($_POST['txtPalabras']);
                    $guia->setIdFichaFk($_POST['cmbFicha']);
                    $guia->setIdCompetenciaFk($_POST['cmbComp']);
                    $guia->setIdUsuarioActualizacion($_POST['hidIdSesion']);
                    
                    $archivo = $_FILES["txtFileGuia"];
                    $nombreArchivo = $_POST["txtNombre"];
                    $parts = explode(".",$_FILES['txtFileGuia']['name']);
                    $extension = end($parts);
                    $dir_subida = "../archivos/guias/";
                    $fichero_temporal = $archivo['tmp_name'];
                    $fichero_subido = $dir_subida.$nombreArchivo."_".$_POST['hidIdGuia'].".".$extension;
                    
                    $archi = $_FILES['txtFileGuia']['name'];
                    $fichero_subido2 = $dir_subida.$nombreArchivo."_".$_POST['hidIdGuia'].".pdf";

                    if ($_POST['hidNombre'] != $_POST['txtNombre']) {

                        if ($archi != '') {
                            if ($extension == 'pdf') {
                                // Guarda en BD URL: $fichero_subido
                                if (is_uploaded_file($fichero_temporal)) {

                                    move_uploaded_file($fichero_temporal,$fichero_subido);
                                    unlink($_POST['hidUrl']);

                                    $guia->setUrl($fichero_subido);
                                    $resultado = $guia->modificar();
                                    $respuesta['respuesta'] = "la informacion se actualizo con exito";
                                }
                            }else {
                                $respuesta['respuesta'] ="Error, la extencion no es valida solo archivos pdf";
                            }
                        }else{
                            $guia->setUrl($fichero_subido2);
                            $resultado = $guia->modificar();
                            if ($resultado=true) {
                                rename($_POST['hidUrl'],$fichero_subido2);
                                $respuesta['respuesta'] = "la informacion se actualizo con exito";
                            }
                        }
                    }else {
                        if ($archi != '') {
                            if ($extension == 'pdf') {
                                // Guarda en BD URL: $fichero_subido
                                if (is_uploaded_file($fichero_temporal)) {
                                    move_uploaded_file($fichero_temporal,$fichero_subido);
                                    
                                    $guia->setUrl($fichero_subido);
                                    $resultado = $guia->modificar();
                                    $respuesta['respuesta'] = "la informacion se actualizo con exito";
                                }
                            }else {
                                $respuesta['respuesta'] ="Error, la extencion no es valida solo archivos pdf";
                            }
                        }else {
                            $guia->setUrl($fichero_subido2);
                            $resultado = $guia->modificar();
                            $respuesta['respuesta'] = "la informacion se actualizo con exito";
                        }
                    }
                    
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible modificar la información, consulte con el administrador.";
                }
                //Respuesta del retorno
                $respuesta['accion']='MODIFICAR';
                echo json_encode($respuesta);
            break;
            case 'LISTAR':
                try{
                    $guia = new GuiasAprendizaje();
                    $resultado = $guia->consultarParaListarGuias();
                    $numeroRegistros = $guia->conn->obtenerNumeroRegistros();

                    if(isset($resultado)){
                        $rowConsulta = $guia->conn->obtenerRegistros();
                        $respuesta['listagui']=$rowConsulta;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='LISTAR';
                echo json_encode($respuesta);
            break;
            case 'LISTARCOMPETENCIA':
                try{
                    $guia = new GuiasAprendizaje();
                    $guia->setIdFichaFk($_POST['cmbFicha']);
                    $resultado = $guia->consultarParaListarCompetencias();
                    $numeroRegistros = $guia->conn->obtenerNumeroRegistros();

                    if(isset($resultado)){
                        $rowConsulta = $guia->conn->obtenerRegistros();
                        $respuesta['listaCom']=$rowConsulta;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='LISTAR';
                echo json_encode($respuesta);
            break;
            case 'LISTARFICHA':
                try{
                    $persona = new Personas();
                    $persona->setIdPersonaPk($_POST['hidPersona']);
                    $resultado = $persona->listarFichaPorIdPersona();
                    $numeroRegistros = $persona->conn->obtenerNumeroRegistros();

                    if(isset($resultado)){
                        $rowConsulta = $persona->conn->obtenerRegistros();
                        $respuesta['listaFic']=$rowConsulta;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='LISTAR';
                echo json_encode($respuesta);
            break;
            case 'CONSULTARREDINSTRUCTOR':
                try{
                    
                    $persona = new Personas();
                    $persona->setIdPersonaPk($_POST['hidPersona']);
                    $resultado = $persona->consultarRedIns();
                    $numeroRegistros = $persona->conn->obtenerNumeroRegistros();
                    $respuesta['numeroRegistros']=$numeroRegistros;

                    if($numeroRegistros === 1){
                        $rowBuscar=$persona->conn->obtenerObjeto();
                        $respuesta['datos']=$rowBuscar;
                        $respuestas=$rowBuscar;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='CONSULTAR';
                echo json_encode($respuesta);
            break;
            case 'LISTARRAP':
                try{
                    $rap = new ResultadoAprendizaje();
                    $rap->setIdComptenciaFk($_POST['cmbComp']);
                    $resultado = $rap->consultarPorCompetencia();
                    $numeroRegistros = $rap->conn->obtenerNumeroRegistros();

                    if(isset($resultado)){
                        $rowConsulta = $rap->conn->obtenerRegistros();
                        $respuesta['listaRap']=$rowConsulta;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='LISTAR';
                echo json_encode($respuesta);
            break;
        }
    }
?>