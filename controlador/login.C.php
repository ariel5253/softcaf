<?php
    session_start(); 
    extract($_REQUEST);
    require_once '../entorno/conexion.php';
    require '../modelo/usuarios.M.php';
    
    if (isset ($_POST['accion'])){  
        switch ($_POST['accion']) {  
            case 'CONSULTAR':
                try{
                    define("CLAVE_SECRETA", "6Ld29hsbAAAAAI-F21sUap7Dknl8rMrWTBlCVZ4e");

                    if (!isset($_POST["g-recaptcha-response"]) || empty($_POST["g-recaptcha-response"])) {

                        $respuesta['respuesta'] = "Para ingresar a nuestra plataforma debe seleccionar el reCAPTCHA ";

                    }else {

                        $token = $_POST["g-recaptcha-response"];
                        $verificado = verificarToken($token, CLAVE_SECRETA);

                        if ($verificado) {
                            if ($txtCorreo != '' && $txtContraseña != '') {
                                //consulta del usuario 
                                $usuario = new Usuarios();
                                $usuario->setUsuario($_POST['txtCorreo']);
                                $resultado = $usuario->consultarPorUsuario();
                                $numeroRegistrosCon = $usuario->conn->obtenerNumeroRegistros();
                                if ($numeroRegistrosCon === 1) {
                                    $rowBuscar = $usuario->conn->obtenerObjeto();
                                    $contraseña = $rowBuscar->password;
                                    if (password_verify($_POST['txtContraseña'],$contraseña)) {
                                        $usu = new Usuarios();
                                        $usu->setUsuario($_POST['txtCorreo']);
                                        $resultado = $usu->consultarUsuarioRol();
                                        $numeroRegistros = $usu->conn->obtenerNumeroRegistros();
                                        $respuesta['numeroRegistros']=$numeroRegistros;
                                        if($numeroRegistros === 1){
                                            if ($rowBuscar = $usu->conn->obtenerObjeto()){ 
                                                $_SESSION["rol_usuario"] = $rowBuscar->rol_usuario; 
                                                $_SESSION["id_usuario"]=$rowBuscar->id_usuario;
                                                $_SESSION["nombre_usuario"]=$rowBuscar->nombre_usuario;
                                                $_SESSION["nombre_persona"]=$rowBuscar->nombre_persona;
                                                $_SESSION["id_persona"]=$rowBuscar->id_persona;
                                                $respuesta['rol'] = $rowBuscar->rol_usuario; 
                                                $respuesta['respuesta'] = "Bienvenido(a) ".$_SESSION["nombre_persona"]."";
                                            }
                                        }
                                    }else {
                                        $respuesta['respuesta']='Credenciales no valida';
                                    }
                                }
                            }else {
                                $respuesta['respuesta'] = "Para ingresar a nuestra plataforma debe de llenar los campos.";
                            }
                            

                        }else {
                            $respuesta['respuesta'] = "Parece ser un robot.";
                        }
                    }
    
                }catch(Exception $e){
                    $respuesta['respuesta'] = "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='CONSULTAR'; 
                echo json_encode($respuesta);
            break;
        }
    }

    

    function verificarToken($token, $claveSecreta){
        # La API en donde verificamos el token
        $url = "https://www.google.com/recaptcha/api/siteverify";
        # Los datos que enviamos a Google
        $datos = [
            "secret" => $claveSecreta,
            "response" => $token,
        ];
        // Crear opciones de la petición HTTP
        $opciones = array(
            "http" => array(
                "header" => "Content-type: application/x-www-form-urlencoded\r\n",
                "method" => "POST",
                "content" => http_build_query($datos), # Agregar el contenido definido antes
            ),
        );
        # Preparar petición
        $contexto = stream_context_create($opciones);
        # Hacerla
        $resultado = file_get_contents($url, false, $contexto);
        # Si hay problemas con la petición (por ejemplo, que no hay internet o algo así)
        # entonces se regresa false. Este NO es un problema con el captcha, sino con la conexión
        # al servidor de Google
        if ($resultado === false) {
            # Error haciendo petición
            return false;
        }

        # En caso de que no haya regresado false, decodificamos con JSON
        # https://parzibyte.me/blog/2018/12/26/codificar-decodificar-json-php/

        $resultado = json_decode($resultado);
        # La variable que nos interesa para saber si el usuario pasó o no la prueba
        # está en success
        $pruebaPasada = $resultado->success;
        # Regresamos ese valor, y listo (sí, ya sé que se podría regresar $resultado->success)
        return $pruebaPasada;
    }
?>