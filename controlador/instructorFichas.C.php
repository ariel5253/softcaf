<?php
    require_once '../entorno/conexion.php';
    require '../modelo/instructoresFichas.M.php'; 
    $respuesta = array();
    if (isset ($_POST['hidAccion'])){  
        switch ($_POST['hidAccion']) { 
            /* 
            case 'ADICIONAR':
                try{
                    //subir guia
                    $nombreArchivo = $_POST["txtNombreMaterial"];
                    $archivo = $_FILES["txtFileMaterial"];
                    $parts = explode(".",$_FILES['txtFileMaterial']['name']);
                    $extension = end($parts);
                    $dir_subida = "../archivos/materialApoyo/";
                    $fichero_temporal = $archivo['tmp_name'];
                    $fichero_subido = $dir_subida.$nombreArchivo."_".$_POST['hidIdGuia'].".".$extension;

                    //Guia de aprendizaje
                    $material = new MaterialesApoyo();
                    $material->setNombre($_POST['txtNombreMaterial']);
                    $material->setPalabrasClave($_POST['txtPalabrasMaterial']);
                    $material->setUrl($fichero_subido);
                    $material->setIdGuiasAprendizajeFk($_POST['hidIdGuia']);
                    $resultado = $material->agregar();

                    if ($resultado = true) {
                        // Guarda en BD URL: $fichero_subido
                        if (is_uploaded_file($fichero_temporal)) {
                            move_uploaded_file($fichero_temporal,$fichero_subido);
                            $respuesta['respuesta'] = "La información se adicionó correctamente.";
                        }
                    }

                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible adicionar la información, consulte con el administrador.";
                }
                //Respuesta del retorno
                $respuesta['accion']='ADICIONAR';
                echo json_encode($respuesta); 
            break;
            */
            case 'LISTAR':
                try{
                    $instructorficha = new InstructorFicha();
                    $instructorficha->setIdInstructorfk($_POST['hidInstructor']);
                    $resultado = $instructorficha->consultarParaListar();
                    $numeroRegistros = $instructorficha->conn->obtenerNumeroRegistros();

                    if(isset($resultado)){
                        $rowConsulta = $instructorficha->conn->obtenerRegistros();
                        $respuesta['listaFic']=$rowConsulta;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='LISTAR';
                echo json_encode($respuesta);
            break;
            /*
            case 'CONSULTAR':
                try{
                    
                    $material = new MaterialesApoyo();
                    $material->setIdMaterialesApoyoPk($_POST['hidIdMaterial']);
                    $resultado = $material->consultar();
                    $numeroRegistros = $material->conn->obtenerNumeroRegistros();
                    $respuesta['numeroRegistros']=$numeroRegistros;

                    if($numeroRegistros === 1){
                        $rowBuscar=$material->conn->obtenerObjeto();
                        $respuesta['datos']=$rowBuscar;
                        $respuestas=$rowBuscar;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='CONSULTAR';
                echo json_encode($respuesta);
            break;
            case 'MODIFICAR':
                try{

                    $material = new MaterialesApoyo();
                    $material->setIdMaterialesApoyoPk($_POST['hidIdMaterial']);
                    $material->setNombre($_POST['txtNombreMaterial']);
                    $material->setPalabrasClave($_POST['txtPalabrasMaterial']);
                    $material->setIdGuiasAprendizajeFk($_POST['hidIdGuia']);
                    
                    $archivo = $_FILES["txtFileMaterial"];
                    $nombreArchivo = $_POST["txtNombreMaterial"];
                    $parts = explode(".",$_FILES['txtFileMaterial']['name']);
                    $extension = end($parts);
                    $dir_subida = "../archivos/materialApoyo/";
                    $fichero_temporal = $archivo['tmp_name'];
                    $fichero_subido = $dir_subida.$nombreArchivo."_".$_POST['hidIdGuia'].".".$extension;
                    
                    $archi = $_FILES['txtFileMaterial']['name'];
                    $fichero_subido2 = $dir_subida.$nombreArchivo."_".$_POST['hidIdGuia'].".pdf";

                    if ($_POST['hidNombre'] != $_POST['txtNombreMaterial']) {

                        if ($archi != '') {
                            if ($extension == 'pdf') {
                                // Guarda en BD URL: $fichero_subido
                                if (is_uploaded_file($fichero_temporal)) {

                                    move_uploaded_file($fichero_temporal,$fichero_subido);
                                    unlink($_POST['hidUrl']);

                                    $material->setUrl($fichero_subido);
                                    $resultado = $material->modificar();
                                    $respuesta['respuesta'] = "la informacion se actualizo con exito";
                                }
                            }else {
                                $respuesta['respuesta'] ="Error, la extencion no es valida solo archivos pdf";
                            }
                        }else{
                            $material->setUrl($fichero_subido2);
                            $resultado = $material->modificar();
                            if ($resultado=true) {
                                rename($_POST['hidUrl'],$fichero_subido2);
                                $respuesta['respuesta'] ="la informacion se actualizo con exito";
                            }
                            
                        }
                    }else {
                        if ($archi != '') {
                            if ($extension == 'pdf') {
                                // Guarda en BD URL: $fichero_subido
                                if (is_uploaded_file($fichero_temporal)) {
                                    move_uploaded_file($fichero_temporal,$fichero_subido);
                                    
                                    $material->setUrl($fichero_subido);
                                    $resultado = $material->modificar();
                                    $respuesta['respuesta'] = "la informacion se actualizo con exito";
                                }
                            }else {
                                $respuesta['respuesta'] ="Error, la extencion no es valida solo archivos pdf";
                            }
                        }else {
                            $material->setUrl($fichero_subido2);
                            $resultado = $material->modificar();
                            $respuesta['respuesta'] = "La información se adicionó correctamente";
                        }
                    }
                    
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible modificar la información, consulte con el administrador.";
                }
                //Respuesta del retorno
                $respuesta['accion']='MODIFICAR';
                echo json_encode($respuesta);
            break;
            
            case 'ELIMINAR':
                try{
                    $guia = new Guia();
                    $guia->setIdGuiaPk($_POST['id_persona_pk']);
                    $resultado = $persona->eliminar();
                    $respuesta['respuesta'] = "La información se eliminó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible eliminar la información, consulte con el administrador.";                    
                }
                //Respuesta del retorno
                $respuesta['accion']='ELIMINAR'; 
                echo json_encode($respuesta);
            break;
            */
        }
    }
?>