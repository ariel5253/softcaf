<?php
    require_once '../../entorno/conexion.php';
    require '../../modelo/administrador/competencias.M.php'; 
    $respuesta = array();
    if (isset ($_POST['hidAccion'])){  
        switch ($_POST['hidAccion']) { 
            case 'ADICIONAR':
                try{
                    $competencia = new Competencias();
                    $competencia->setCodigo($_POST['txtCodigo']);
                    $competencia->setNombre($_POST['txtNombre']);
                    $competencia->setIdProgramaFormacionFk($_POST['hidProgramaFormacion']);
                    $competencia->setIdUsuarioCreacion($_POST['hidIdSesion']);
                    $competencia->setIdUsuarioActualizacion($_POST['hidIdSesion']);
                    $resultado = $competencia->agregar();
                    $respuesta['respuesta'] = "La información se adicionó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible adicionar la información, consulte con el administrador.";
                }
                //Respuesta del retorno
                $respuesta['accion']='ADICIONAR';
                echo json_encode($respuesta);
            break;
            case 'CONSULTAR':
                try{
                    
                    $competencia = new Competencias();
                    $competencia->setIdCompetenciaPk($_POST['hidCompetencia']);
                    $resultado = $competencia->consultarPorId();
                    $numeroRegistros = $competencia->conn->obtenerNumeroRegistros();
                    if($numeroRegistros === 1){
                        $rowBuscar=$competencia->conn->obtenerObjeto();
                        $respuesta['datos']=$rowBuscar;
                        $respuestas=$rowBuscar;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='CONSULTAR';
                echo json_encode($respuesta);
            break;
            case 'MODIFICAR':
                try{
                    $competencia = new Competencias();
                    $competencia->setIdCompetenciaPk($_POST['hidCompetencia']);
                    $competencia->setCodigo($_POST['txtCodigo']);
                    $competencia->setNombre($_POST['txtNombre']);
                    $competencia->setIdProgramaFormacionFk($_POST['hidProgramaFormacion']);
                    $competencia->setIdUsuarioActualizacion($_POST['hidIdSesion']);
                    $resultado = $competencia->modificar();
                    $respuesta['respuesta'] = "la informacion se actualizo con exito";
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='MODIFICAR';
                echo json_encode($respuesta);
            break;
            case 'LISTAR':
                try{
                    $competencia = new Competencias();
                    $resultado = $competencia->listarCompetencia();
                    $numeroRegistros = $competencia->conn->obtenerNumeroRegistros();
                    if(isset($resultado)){
                        $rowConsulta = $competencia->conn->obtenerRegistros();
                        $respuesta['listaCom']=$rowConsulta;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='LISTAR';
                echo json_encode($respuesta);
            break;
        }
    }
?>