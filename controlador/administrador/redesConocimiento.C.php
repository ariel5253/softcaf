<?php
    require_once '../../entorno/conexion.php';
    require '../../modelo/administrador/redesConocimiento.M.php'; 
    $respuesta = array();
    if (isset ($_POST['hidAccion'])){  
        switch ($_POST['hidAccion']) { 
            case 'ADICIONAR':
                try{
                    $red = new RedesConocimiento();
                    $red->setNombre($_POST['txtNombre']);
                    $red->setIdUsuarioCreacion($_POST['hidIdSesion']);
                    $red->setIdUsuarioActualizacion($_POST['hidIdSesion']);
                    $resultado = $red->agregar();
                    $respuesta['respuesta'] = "La información se adicionó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible adicionar la información, consulte con el administrador.";
                }
                //Respuesta del retorno
                $respuesta['accion']='ADICIONAR';
                echo json_encode($respuesta);
            break;
            case 'CONSULTAR':
                try{
                    
                    $red = new RedesConocimiento();
                    $red->setIdRedesConocimientoPk($_POST['hidRedConocimiento']);
                    $resultado = $red->consultarPorId();
                    $numeroRegistros = $red->conn->obtenerNumeroRegistros();
                    if($numeroRegistros === 1){
                        $rowBuscar=$red->conn->obtenerObjeto();
                        $respuesta['datos']=$rowBuscar;
                        $respuestas=$rowBuscar;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='CONSULTAR';
                echo json_encode($respuesta);
            break;
            case 'MODIFICAR':
                try{
                    $red = new RedesConocimiento();
                    $red->setIdRedesConocimientoPk($_POST['hidRedConocimiento']);
                    $red->setNombre($_POST['txtNombre']);
                    $red->setIdUsuarioActualizacion($_POST['hidIdSesion']);
                    $resultado = $red->modificar();
                    $respuesta['respuesta'] = "la informacion se actualizo con exito";
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='MODIFICAR';
                echo json_encode($respuesta);
            break;
            case 'LISTAR':
                try{
                    $red = new RedesConocimiento();
                    $resultado = $red->listarRed();
                    $numeroRegistros = $red->conn->obtenerNumeroRegistros();
                    if(isset($resultado)){
                        $rowConsulta = $red->conn->obtenerRegistros();
                        $respuesta['listaRed']=$rowConsulta;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='LISTAR';
                echo json_encode($respuesta);
            break;
        }
    }
?>