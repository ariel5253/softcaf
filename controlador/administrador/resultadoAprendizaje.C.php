<?php
    require_once '../../entorno/conexion.php';
    require '../../modelo/administrador/resultadoAprendizaje.M.php'; 
    $respuesta = array();
    if (isset ($_POST['hidAccion'])){  
        switch ($_POST['hidAccion']) { 
            case 'ADICIONAR':
                try{
                    $rap = new ResultadoAprendizaje();
                    $rap->setNombre($_POST['txtNombre']);
                    $rap->setIdComptenciaFk($_POST['hidCompetencia']);
                    $rap->setIdUsuarioCreacion($_POST['hidIdSesion']);
                    $rap->setIdUsuarioActualizacion($_POST['hidIdSesion']);
                    $resultado = $rap->agregar();
                    $respuesta['respuesta'] = "La información se adicionó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible adicionar la información, consulte con el administrador.";
                }
                //Respuesta del retorno
                $respuesta['accion']='ADICIONAR';
                echo json_encode($respuesta);
            break;
            case 'CONSULTAR':
                try{
                    
                    $rap = new ResultadoAprendizaje();
                    $rap->setIdResultadoAprendizajePk($_POST['hidResultadoA']);
                    $resultado = $rap->consultarPorId();
                    $numeroRegistros = $rap->conn->obtenerNumeroRegistros();
                    if($numeroRegistros === 1){
                        $rowBuscar=$rap->conn->obtenerObjeto();
                        $respuesta['datos']=$rowBuscar;
                        $respuestas=$rowBuscar;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='CONSULTAR';
                echo json_encode($respuesta);
            break;
            case 'MODIFICAR':
                try{
                    $rap = new ResultadoAprendizaje();
                    $rap->setIdResultadoAprendizajePk($_POST['hidResultadoA']);
                    $rap->setNombre($_POST['txtNombre']);
                    $rap->setIdComptenciaFk($_POST['hidCompetencia']);
                    $rap->setIdUsuarioActualizacion($_POST['hidIdSesion']);
                    $resultado = $rap->modificar();
                    $respuesta['respuesta'] = "la informacion se actualizo con exito";
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='MODIFICAR';
                echo json_encode($respuesta);
            break;
            case 'LISTAR':
                try{
                    $rap = new ResultadoAprendizaje();
                    $resultado = $rap->listar();
                    $numeroRegistros = $rap->conn->obtenerNumeroRegistros();
                    if(isset($resultado)){
                        $rowConsulta = $rap->conn->obtenerRegistros();
                        $respuesta['listaCom']=$rowConsulta;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='LISTAR';
                echo json_encode($respuesta);
            break;
        }
    }
?>