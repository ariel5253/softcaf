<?php
    require_once '../../entorno/conexion.php';
    require '../../modelo/administrador/centrosFormacion.M.php'; 
    $respuesta = array();
    if (isset ($_POST['hidAccion'])){  
        switch ($_POST['hidAccion']) { 
            case 'ADICIONAR':
                try{
                    $centros = new CentrosFormacion();
                    $centros->setNombre($_POST['txtNombre']);
                    $centros->setIdMunicipioFk($_POST['hidMunicipio']);
                    $centros->setIdUsuarioCreacion($_POST['hidIdSesion']);
                    $centros->setIdUsuarioActualizacion($_POST['hidIdSesion']);
                    $resultado = $centros->agregar();
                    $respuesta['respuesta'] = "La información se adicionó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible adicionar la información, consulte con el administrador.";
                }
                //Respuesta del retorno
                $respuesta['accion']='ADICIONAR';
                echo json_encode($respuesta);
            break;
            case 'CONSULTAR':
                try{
                    
                    $centros = new CentrosFormacion();
                    $centros->setIdCentrosFormacionPk($_POST['hidCentroFormacion']);
                    $resultado = $centros->consultarPorId();
                    $numeroRegistros = $centros->conn->obtenerNumeroRegistros();
                    if($numeroRegistros === 1){
                        $rowBuscar=$centros->conn->obtenerObjeto();
                        $respuesta['datos']=$rowBuscar;
                        $respuestas=$rowBuscar;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='CONSULTAR';
                echo json_encode($respuesta);
            break;
            case 'MODIFICAR':
                try{
                    $centros = new CentrosFormacion();
                    $centros->setIdCentrosFormacionPk($_POST['hidCentroFormacion']);
                    $centros->setNombre($_POST['txtNombre']);
                    $centros->setIdMunicipioFk($_POST['hidMunicipio']);
                    $centros->setIdUsuarioActualizacion($_POST['hidIdSesion']);
                    $resultado = $centros->modificar();
                    $respuesta['respuesta'] = "la informacion se actualizo con exito";
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='MODIFICAR';
                echo json_encode($respuesta);
            break;
            case 'LISTAR':
                try{
                    $centros = new CentrosFormacion();
                    $resultado = $centros->listarCentros();
                    $numeroRegistros = $centros->conn->obtenerNumeroRegistros();
                    if(isset($resultado)){
                        $rowConsulta = $centros->conn->obtenerRegistros();
                        $respuesta['listaCentro']=$rowConsulta;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='LISTAR';
                echo json_encode($respuesta);
            break;
        }
    }
?>