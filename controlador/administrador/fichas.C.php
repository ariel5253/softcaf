<?php
    require_once '../../entorno/conexion.php';
    require '../../modelo/administrador/fichas.M.php'; 
    $respuesta = array();
    if (isset ($_POST['hidAccion'])){  
        switch ($_POST['hidAccion']) { 
            case 'ADICIONAR':
                try{
                    $fichas = new Fichas();
                    $fichas->setCodigo($_POST['txtNumeroFicha']);
                    $fichas->setIdProgramaFormacionFk($_POST['hidPrograma']);
                    $fichas->setIdCentroFormacionFk($_POST['hidCentroFormacion']);
                    $fichas->setIdUsuarioCreacion($_POST['hidIdSesion']);
                    $fichas->setIdUsuarioActualizacion($_POST['hidIdSesion']);
                    $resultado = $fichas->agregar();
                    $respuesta['respuesta'] = "La información se adicionó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible adicionar la información, consulte con el administrador.";
                }
                //Respuesta del retorno
                $respuesta['accion']='ADICIONAR';
                echo json_encode($respuesta);
            break;
            case 'CONSULTAR':
                try{
                    
                    $fichas = new Fichas();
                    $fichas->setIdFichaPk($_POST['hidFicha']);
                    $resultado = $fichas->consultarPorId();
                    $numeroRegistros = $fichas->conn->obtenerNumeroRegistros();
                    if($numeroRegistros === 1){
                        $rowBuscar=$fichas->conn->obtenerObjeto();
                        $respuesta['datos']=$rowBuscar;
                        $respuestas=$rowBuscar;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='CONSULTAR';
                echo json_encode($respuesta);
            break;
            case 'MODIFICAR':
                try{
                    $fichas = new Fichas();
                    $fichas->setIdFichaPk($_POST['hidFicha']);
                    $fichas->setCodigo($_POST['txtNumeroFicha']);
                    $fichas->setIdProgramaFormacionFk($_POST['hidPrograma']);
                    $fichas->setIdCentroFormacionFk($_POST['hidCentroFormacion']);
                    $fichas->setIdUsuarioActualizacion($_POST['hidIdSesion']);
                    $resultado = $fichas->modificar();
                    $respuesta['respuesta'] = "la informacion se actualizo con exito";
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='MODIFICAR';
                echo json_encode($respuesta);
            break;
            case 'LISTAR':
                try{
                    $fichas = new Fichas();
                    $resultado = $fichas->listarFichas();
                    $numeroRegistros = $fichas->conn->obtenerNumeroRegistros();
                    if(isset($resultado)){
                        $rowConsulta = $fichas->conn->obtenerRegistros();
                        $respuesta['listaFic']=$rowConsulta;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='LISTAR';
                echo json_encode($respuesta);
            break;
        }
    }
?>