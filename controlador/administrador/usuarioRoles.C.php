<?php
    require_once '../../entorno/conexion.php';
    require '../../modelo/administrador/usuarioRoles.M.php'; 
    require '../../modelo/administrador/roles.M.php'; 
    $respuesta = array();
    if (isset ($_POST['hidAccion'])){   
        switch ($_POST['hidAccion']) { 
            case 'ADICIONAR':
                try{
                    $usuRoles = new UsuarioRoles();
                    $usuRoles->setIdUsuarioFk($_POST['hidUsuario']);
                    $usuRoles->setIdRolFk($_POST['cmbRol']);
                    $usuRoles->setEstado($_POST['cmbEstado']);
                    $usuRoles->setIdUsuarioCreacion($_POST['hidIdSesion']);
                    $usuRoles->setIdUsuarioActualizacion($_POST['hidIdSesion']);
                    $resultado = $usuRoles->agregar();
                    $respuesta['respuesta'] = "La información se adicionó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible adicionar la información, consulte con el administrador.";
                }
                //Respuesta del retorno
                $respuesta['accion']='ADICIONAR';
                echo json_encode($respuesta);
            break;
            case 'CONSULTAR':
                try{
                    
                    $usuRoles = new UsuarioRoles();
                    $usuRoles->setIdUsuariosRolesPk($_POST['hidUsuarioRol']);
                    $resultado = $usuRoles->consultarPorId();
                    $numeroRegistros = $usuRoles->conn->obtenerNumeroRegistros();
                    if($numeroRegistros === 1){
                        $rowBuscar=$usuRoles->conn->obtenerObjeto();
                        $respuesta['datos']=$rowBuscar;
                        $respuestas=$rowBuscar;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='CONSULTAR';
                echo json_encode($respuesta);
            break;
            case 'MODIFICAR':
                try{
                    $usuRoles = new UsuarioRoles();
                    $usuRoles->setIdUsuariosRolesPk($_POST['hidUsuarioRol']);
                    $usuRoles->setIdUsuarioFk($_POST['hidUsuario']);
                    $usuRoles->setIdRolFk($_POST['cmbRol']);
                    $usuRoles->setEstado($_POST['cmbEstado']);
                    $usuRoles->setIdUsuarioActualizacion($_POST['hidIdSesion']);
                    $resultado = $usuRoles->modificar();
                    $respuesta['respuesta'] = "la informacion se actualizo con exito";
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='MODIFICAR';
                echo json_encode($respuesta);
            break;
            case 'LISTAR':
                try{
                    $usuRoles = new UsuarioRoles();
                    $resultado = $usuRoles->listarUsuarioRol();
                    $numeroRegistros = $usuRoles->conn->obtenerNumeroRegistros();
                    if(isset($resultado)){
                        $rowConsulta = $usuRoles->conn->obtenerRegistros();
                        $respuesta['listaUsuRol']=$rowConsulta;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='LISTAR';
                echo json_encode($respuesta);
            break;
            case 'LISTARROL':
                try{
                    $rol = new Roles();
                    $resultado = $rol->listarRol();
                    $numeroRegistros = $rol->conn->obtenerNumeroRegistros();
                    if(isset($resultado)){
                        $rowConsulta = $rol->conn->obtenerRegistros();
                        $respuesta['listaRol']=$rowConsulta;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='LISTAR';
                echo json_encode($respuesta);
            break;
        }
    }
?>