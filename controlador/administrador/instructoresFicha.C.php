<?php
    require_once '../../entorno/conexion.php';
    require '../../modelo/administrador/instructoresFichas.M.php'; 
    $respuesta = array();
    if (isset ($_POST['hidAccion'])){  
        switch ($_POST['hidAccion']) { 
            case 'ADICIONAR':
                try{
                    $insFic = new instructoresFicha();
                    $insFic->setIdInstructorFk($_POST['hidInstructor']);
                    $insFic->setIdFichaFk($_POST['hidFicha']);
                    $insFic->setEstado($_POST['cmbEstado']);
                    $insFic->setIdUsuarioCreacion($_POST['hidIdSesion']);
                    $insFic->setIdUsuarioActualizacion($_POST['hidIdSesion']);
                    $resultado = $insFic->agregar();
                    $respuesta['respuesta'] = "La información se adicionó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible adicionar la información, consulte con el administrador.";
                }
                //Respuesta del retorno
                $respuesta['accion']='ADICIONAR';
                echo json_encode($respuesta);
            break;
            case 'CONSULTAR':
                try{
                    
                    $insFic = new instructoresFicha();
                    $insFic->setIdInstructoresFichaPk($_POST['hidInstructorFicha']);
                    $resultado = $insFic->consultarPorId();
                    $numeroRegistros = $insFic->conn->obtenerNumeroRegistros();
                    if($numeroRegistros === 1){
                        $rowBuscar=$insFic->conn->obtenerObjeto();
                        $respuesta['datos']=$rowBuscar;
                        $respuestas=$rowBuscar;
                    }
    
                }catch(Exception $e){ 
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='CONSULTAR';
                echo json_encode($respuesta);
            break;
            case 'MODIFICAR':
                try{
                    $insFic = new instructoresFicha();
                    $insFic->setIdInstructoresFichaPk($_POST['hidInstructorFicha']);
                    $insFic->setIdInstructorFk($_POST['hidInstructor']);
                    $insFic->setIdFichaFk($_POST['hidFicha']);
                    $insFic->setEstado($_POST['cmbEstado']);
                    $insFic->setIdUsuarioActualizacion($_POST['hidIdSesion']);
                    $resultado = $insFic->modificar();
                    $respuesta['respuesta'] = "la informacion se actualizo con exito";
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='MODIFICAR';
                echo json_encode($respuesta);
            break;
            case 'LISTAR':
                try{
                    $insFic = new instructoresFicha();
                    $resultado = $insFic->listarInstructorFicha();
                    $numeroRegistros = $insFic->conn->obtenerNumeroRegistros();
                    if(isset($resultado)){
                        $rowConsulta = $insFic->conn->obtenerRegistros();
                        $respuesta['listaInsFic']=$rowConsulta;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='LISTAR';
                echo json_encode($respuesta);
            break;
        }
    }
?>