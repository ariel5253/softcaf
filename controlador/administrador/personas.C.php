<?php
    require_once '../../entorno/conexion.php';
    require '../../modelo/administrador/personas.M.php'; 
    require '../../modelo/administrador/usuarios.M.php'; 
    require '../../modelo/administrador/roles.M.php'; 
    require '../../modelo/administrador/usuarioRoles.M.php'; 
    require '../../modelo/administrador/instructores.M.php'; 
    
    $respuesta = array();
    if (isset ($_POST['hidAccion'])){  
        switch ($_POST['hidAccion']) { 
            case 'ADICIONAR':
                try{
                    //$con = Conexion::singleton();
                    
                    //$con->begin_transaction(MYSQLI_TRANS_START_READ_ONLY);
                    //Insertar Persona
                    $persona = new Personas();
                    $persona->setNombres($_POST['txtNombres']);
                    $persona->setApellido($_POST['txtapellidos']);
                    $persona->setTipoSangre($_POST['cmbTipoSangre']);
                    $persona->setTipoDocumento($_POST['cmbTipoDocumento']);
                    $persona->setNumeroDocumento($_POST['txtDocumento']);
                    $persona->setEdad($_POST['txtEdad']);
                    $persona->setGenero($_POST['cmbGenero']);
                    $persona->setTelefono($_POST['txtTelefono']);
                    $persona->setCorreo($_POST['txtCorreo']);
                    $persona->setDireccion($_POST['txtDireccion']);
                    $persona->setIdUsuarioCreacion($_POST['hidIdSesion']);
                    $persona->setIdUsuarioActualizacion($_POST['hidIdSesion']);
                    $resultadoPersona = $persona->agregar();
                    
                    //Consultar Ultimo Id Persona
                    $resultadoConsultaPersona = $persona->consultarUltimoId();
                    $numeroRegistrosPersona = $persona->conn->obtenerNumeroRegistros();
                    if($numeroRegistrosPersona === 1){ 
                        $rowBuscarPersona=$persona->conn->obtenerObjeto();
                        $idPersona = $rowBuscarPersona->id_persona;
                        // Insertar Usuario
                        $usuario = new Usuarios();
                        $usuario->setUsuario($_POST['txtCorreo']);
                        $usuario->setPassword(password_hash($_POST['txtDocumento'],PASSWORD_DEFAULT));
                        $usuario->setIdPersonaFk($idPersona);
                        $usuario->setIdUsuarioCreacion($_POST['hidIdSesion']);
                        $usuario->setIdUsuarioActualizacion($_POST['hidIdSesion']);
                        $resultadoUsuario = $usuario->agregar();
                        //Consultar Ultimo Id Usuario
                        $resultadoConsultaUsuario = $usuario->consultarUltimoId();
                        $numeroRegistrosUsuario = $usuario->conn->obtenerNumeroRegistros();
                        if ($numeroRegistrosUsuario === 1) {
                            $rowBuscarUsuario=$usuario->conn->obtenerObjeto();
                            $idUsuario = $rowBuscarUsuario->id_usuario;
                            // Insertar Usuario Rol
                            $usuRol = new UsuarioRoles();
                            $usuRol->setIdUsuarioFk($idUsuario);
                            $usuRol->setIdRolFk($_POST['cmbRol']);
                            $usuRol->setEstado('ACTIVO');
                            $usuRol->setIdUsuarioCreacion($_POST['hidIdSesion']);
                            $usuRol->setIdUsuarioActualizacion($_POST['hidIdSesion']);
                            $resultadoUsuarioRol = $usuRol->agregar();
                            // Insertar Instructor
                            if ($_POST['cmbRol'] == 2) {
                                $instructor = new Instructores();
                                $instructor->setIdPersonaFk($idPersona);
                                $instructor->setIdRedConocimientoFk($_POST['hidRedConocimiento']);
                                $instructor->setIdUsuarioCreacion($_POST['hidIdSesion']);
                                $instructor->setIdUsuarioActualizacion($_POST['hidIdSesion']);
                                $resultadoInstructor = $instructor->agregar();
                                $respuesta['respuesta'] = "Agregado Correctamente";
                                //$con->commit();
                            }
                            
                        }
                    }
                    
                }catch(Exception $e){
                    //$con->rollback();
                    $respuesta['respuesta'] ="Error, no fué posible adicionar la información, consulte con el administrador.";
                }
                //Respuesta del retorno
                $respuesta['accion']='ADICIONAR';
                echo json_encode($respuesta);
            break;
            case 'CONSULTAR':
                try{
                    
                    $persona = new Personas();
                    $persona->setIdPersonaPk($_POST['hidPersona']);
                    $resultado = $persona->consultarPorId();
                    $numeroRegistros = $persona->conn->obtenerNumeroRegistros();
                    if($numeroRegistros === 1){
                        $rowBuscar=$persona->conn->obtenerObjeto();
                        $respuesta['datos']=$rowBuscar;
                        $respuestas=$rowBuscar;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='CONSULTAR';
                echo json_encode($respuesta);
            break;
            case 'CONSULTARRED':
                try{
                    
                    $persona = new Personas();
                    $persona->setIdPersonaPk($_POST['hidPersona']);
                    $resultado = $persona->consultarRedPorPersona();
                    $numeroRegistros = $persona->conn->obtenerNumeroRegistros();
                    if($numeroRegistros === 1){
                        $rowBuscar=$persona->conn->obtenerObjeto();
                        $respuesta['datos']=$rowBuscar;
                        $respuestas=$rowBuscar;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='CONSULTAR';
                echo json_encode($respuesta);
            break;
            case 'MODIFICAR':
                //Se modifica persona, usuario y la red a la que pertenece la persona
                try{
                    $persona = new Personas();
                    $persona->setIdPersonaPk($_POST['hidPersona']);
                    $persona->setNombres($_POST['txtNombres']);
                    $persona->setApellido($_POST['txtapellidos']);
                    $persona->setTipoSangre($_POST['cmbTipoSangre']);
                    $persona->setTipoDocumento($_POST['cmbTipoDocumento']);
                    $persona->setNumeroDocumento($_POST['txtDocumento']);
                    $persona->setEdad($_POST['txtEdad']);
                    $persona->setGenero($_POST['cmbGenero']);
                    $persona->setTelefono($_POST['txtTelefono']);
                    $persona->setDireccion($_POST['txtDireccion']);
                    $persona->setIdUsuarioActualizacion($_POST['hidIdSesion']);
                    $persona->setCorreo($_POST['txtCorreo']);
                    $resultadoPersona = $persona->modificar();
                    if ($_POST['hidCorreo'] != $_POST['txtCorreo']) {
                        //Consultar Usuario Por usuario
                        $conUsuario = new Usuarios();
                        $conUsuario->setUsuario($_POST['hidCorreo']);
                        $resultadoConsultaId = $conUsuario->consultarPorUsuario();
                        $numeroRegistros = $conUsuario->conn->obtenerNumeroRegistros();
                        if($numeroRegistros === 1){
                            $rowBuscarUsuario=$conUsuario->conn->obtenerObjeto();
                            $idUsuario = $rowBuscarUsuario->id_usuario;
                            $password = $rowBuscarUsuario->password;
                            $idPersona = $rowBuscarUsuario->id_persona;
                            //Actualizar Usuario
                            $usuario = new Usuarios();
                            $usuario->setIdUsuarioPk($idUsuario);
                            $usuario->setUsuario($_POST['txtCorreo']);
                            $usuario->setPassword($password);
                            $usuario->setIdPersonaFk($idPersona);
                            $usuario->setIdUsuarioActualizacion($_POST['hidIdSesion']);
                            $resultadoUsuario = $usuario->modificar();
                        }
                    }
                    // Actualizar Instructor
                    if ($_POST['cmbRol'] == 2) {
                        // Consultar Instructor Por ID Persona
                        $conInstructor = new Instructores();
                        $conInstructor->setIdPersonaFk($_POST['hidPersona']);
                        $resultadoConsultaIns = $conInstructor->consultarPorIdPersona();
                        $numeroRegistrosIns = $conInstructor->conn->obtenerNumeroRegistros();
                        if ($numeroRegistrosIns === 1) {
                            $rowBuscarIns=$conInstructor->conn->obtenerObjeto();
                            $idInstructor = $rowBuscarIns->id_instructor;
                            // // Actualizar Instructor
                            $instructor = new Instructores();
                            $instructor->setIdInstructorPk($idInstructor);
                            $instructor->setIdPersonaFk($_POST['hidPersona']);
                            $instructor->setIdRedConocimientoFk($_POST['hidRedConocimiento']);
                            $instructor->setIdUsuarioActualizacion($_POST['hidIdSesion']);
                            $resultadoInstructor = $instructor->modificar();
                            $respuesta['respuesta'] = "la informacion se actualizo con exito";
                        }
                        
                    }
                    $respuesta['respuesta'] = "la informacion se actualizo con exito";
                    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='MODIFICAR';
                echo json_encode($respuesta);
            break;
            case 'LISTAR':
                try{
                    $persona = new Personas();
                    $resultado = $persona->listarPersona();
                    $numeroRegistros = $persona->conn->obtenerNumeroRegistros();
                    if(isset($resultado)){
                        $rowConsulta = $persona->conn->obtenerRegistros();
                        $respuesta['listaPer']=$rowConsulta;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='LISTAR';
                echo json_encode($respuesta);
            break;
            case 'LISTARROL':
                try{
                    $rol = new Roles();
                    $resultado = $rol->listarRol();
                    $numeroRegistros = $rol->conn->obtenerNumeroRegistros();
                    if(isset($resultado)){
                        $rowConsulta = $rol->conn->obtenerRegistros();
                        $respuesta['listaRol']=$rowConsulta;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='LISTAR';
                echo json_encode($respuesta);
            break;
        }
    }
?>