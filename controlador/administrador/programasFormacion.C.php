<?php
    require_once '../../entorno/conexion.php';
    require '../../modelo/administrador/programasFormacion.M.php'; 
    $respuesta = array();
    if (isset ($_POST['hidAccion'])){  
        switch ($_POST['hidAccion']) { 
            case 'ADICIONAR':
                try{
                    $programa = new ProgramasFormacion();
                    $programa->setCodigo($_POST['txtCodigo']);
                    $programa->setNombre($_POST['txtNombre']);
                    $programa->setIdRedesConocimientoFk($_POST['hidRedConocimiento']);
                    $programa->setIdUsuarioCreacion($_POST['hidIdSesion']);
                    $programa->setIdUsuarioActualizacion($_POST['hidIdSesion']);
                    $resultado = $programa->agregar();
                    $respuesta['respuesta'] = "La información se adicionó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible adicionar la información, consulte con el administrador.";
                }
                //Respuesta del retorno
                $respuesta['accion']='ADICIONAR';
                echo json_encode($respuesta);
            break;
            case 'CONSULTAR':
                try{
                    
                    $programa = new ProgramasFormacion();
                    $programa->setIdProgramaFormacionPk($_POST['hidProgramaFormacion']);
                    $resultado = $programa->consultarPorId();
                    $numeroRegistros = $programa->conn->obtenerNumeroRegistros();
                    if($numeroRegistros === 1){
                        $rowBuscar=$programa->conn->obtenerObjeto();
                        $respuesta['datos']=$rowBuscar;
                        $respuestas=$rowBuscar;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='CONSULTAR';
                echo json_encode($respuesta);
            break;
            case 'MODIFICAR':
                try{
                    $programa = new ProgramasFormacion();
                    $programa->setIdProgramaFormacionPk($_POST['hidProgramaFormacion']);
                    $programa->setCodigo($_POST['txtCodigo']);
                    $programa->setNombre($_POST['txtNombre']);
                    $programa->setIdRedesConocimientoFk($_POST['hidRedConocimiento']);
                    $programa->setIdUsuarioActualizacion($_POST['hidIdSesion']);
                    $resultado = $programa->modificar();
                    $respuesta['respuesta'] = "la informacion se actualizo con exito";
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='MODIFICAR';
                echo json_encode($respuesta);
            break;
            case 'LISTAR':
                try{
                    $programa = new ProgramasFormacion();
                    $resultado = $programa->listarPrograma();
                    $numeroRegistros = $programa->conn->obtenerNumeroRegistros();
                    if(isset($resultado)){
                        $rowConsulta = $programa->conn->obtenerRegistros();
                        $respuesta['listaPro']=$rowConsulta;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='LISTAR';
                echo json_encode($respuesta);
            break;
        }
    }
?>