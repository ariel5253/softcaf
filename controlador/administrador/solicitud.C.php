<?php
    require_once '../../entorno/conexion.php';
    require '../../modelo/administrador/solicitud.M.php'; 
    require '../../modelo/administrador/personas.M.php'; 
    require '../../modelo/administrador/programasFormacion.M.php'; 
    require '../../modelo/administrador/instructoresFichas.M.php'; 
    $respuesta = array();
    if (isset ($_POST['hidAccion'])){  
        switch ($_POST['hidAccion']) { 
            case 'ADICIONAR':
                try{
                    $solicitud = new Solicitud();
                    $solicitud->setInstructorFk($_POST['hidInstructor']);
                    $solicitud->setFichaFk($_POST['cmbFicha']);
                    $solicitud->setDescripcion($_POST['txtAsunto']);
                    $solicitud->setEstado($_POST['hidEstado']);
                    $solicitud->setIdUsuarioCreacion($_POST['hidIdSesion']);
                    $solicitud->setIdUsuarioActualizacion($_POST['hidIdSesion']);
                    $resultado = $solicitud->agregar();
                    $respuesta['respuesta'] = "La información se adicionó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible adicionar la información, consulte con el administrador.";
                }
                //Respuesta del retorno
                $respuesta['accion']='ADICIONAR';
                echo json_encode($respuesta);
            break;
            case 'CONSULTAR':
                try{
                    
                    $red = new RedesConocimiento();
                    $red->setIdRedesConocimientoPk($_POST['hidRedConocimiento']);
                    $resultado = $red->consultarPorId();
                    $numeroRegistros = $red->conn->obtenerNumeroRegistros();
                    if($numeroRegistros === 1){
                        $rowBuscar=$red->conn->obtenerObjeto();
                        $respuesta['datos']=$rowBuscar;
                        $respuestas=$rowBuscar;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='CONSULTAR';
                echo json_encode($respuesta);
            break;
            case 'MODIFICAR': 
                try{
                    $solicitud = new Solicitud();
                    $solicitud->setIdSolicitudPk($_POST['hidSolicitud']);
                    $solicitud->setEstado($_POST['hidEstado']);
                    $solicitud->setIdUsuarioActualizacion($_POST['hidIdSesion']);
                    $resultado = $solicitud->modificar();

                    $insFic = new instructoresFicha();
                    $insFic->setIdInstructorFk($_POST['hidInstructor']);
                    $insFic->setIdFichaFk($_POST['hidFicha']);
                    $insFic->setEstado($_POST['hidEstadoInsFic']);
                    $insFic->setIdUsuarioCreacion($_POST['hidIdSesion']);
                    $insFic->setIdUsuarioActualizacion($_POST['hidIdSesion']);
                    $resultado = $insFic->agregar();
                    
                    $respuesta['respuesta'] = "la informacion se actualizo con exito";
                    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='MODIFICAR';
                echo json_encode($respuesta);
            break;
            case 'LISTAR':
                try{
                    $solicitud = new Solicitud();
                    $resultado = $solicitud->ListarSolicitud();
                    $numeroRegistros = $solicitud->conn->obtenerNumeroRegistros();
                    if(isset($resultado)){
                        $rowConsulta = $solicitud->conn->obtenerRegistros();
                        $respuesta['listaSol']=$rowConsulta;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='LISTAR';
                echo json_encode($respuesta);
            break;
            case 'LISTARFICHA':
                try{
                    $pro = new ProgramasFormacion();
                    $pro->setIdProgramaFormacionPk($_POST['hidPrograma']);
                    $resultado = $pro->consultarFichasPorPrograma();
                    $numeroRegistros = $pro->conn->obtenerNumeroRegistros();

                    if(isset($resultado)){
                        $rowConsulta = $pro->conn->obtenerRegistros();
                        $respuesta['listaFic']=$rowConsulta;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='LISTAR';
                echo json_encode($respuesta);
            break;
            case 'CONSULTARINSTRUCTOR':
                try{
                    
                    $per = new Personas();
                    $per->setIdPersonaPk($_POST['hidPersona']);
                    $resultado = $per->consultarInstructorPorIdPersona();
                    $numeroRegistros = $per->conn->obtenerNumeroRegistros();
                    if($numeroRegistros === 1){
                        $rowBuscar=$per->conn->obtenerObjeto();
                        $respuesta['datos']=$rowBuscar;
                        $respuestas=$rowBuscar;
                    }
    
                }catch(Exception $e){
                    echo "Error";
                }
                //Retornar del retorno
                $respuesta['accion']='CONSULTAR';
                echo json_encode($respuesta);
            break;
        }
    }
?>