<div class="container bg-white">
    
    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4">
                            <div class="text-center my-5">
                                <h1 class="h4 text-gray-900 mb-4">Perfil</h1>
                            </div>
                            <img class="card-img-top" src="../../componentes/img/usuario.jpg" alt="Card image" style="width:100%">
                            <form class="user mb-5">
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user"
                                        id="txtNombre" value="Jhonn Faiber Sanchez Cupitre">
                                </div>
                                <div class="form-group">
                                    <input type="number" class="form-control form-control-user"
                                        id="txtDocumento" value="1193081391">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control form-control-user"
                                        id="txtCorreo" value="cupitre12@gmail.com">
                                </div>
                                <div class="form-group">
                                    <a href="" class="btn btn-primary btn-user btn-block">
                                        Actualizar
                                    </a>
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-4"></div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>