<?php
    extract($_REQUEST);
    if (isset($pg)) {
        $pagina=$pg.".php";
    } else {
        $pagina="inicio.V.php";
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Bienvenidos</title>

    <!-- Custom fonts for this template-->
    <link href="../libreria/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="../componentes/css/sb-admin-2.min.css" rel="stylesheet">
    <link href="../componentes/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom styles for this page -->
    <link href="../libreria/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>
<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <div><?php include "menu.V.php" ?></div>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            
            <div id="content"><?php include $pagina; ?></div>
            
            <div><?php include "footer.V.php" ?></div>

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Bootstrap core JavaScript-->
    <script src="../libreria/jquery/jquery.min.js"></script>
    <script src="../libreria/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="../libreria/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="../js/sb-admin-2.min.js"></script>
    
    <!-- kinic-->
    <!-- Page level plugins -->
    <script src="../libreria/datatables/jquery.dataTables.min.js"></script>
    <script src="../libreria/datatables/dataTables.bootstrap4.min.js"></script>

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

</body>

</html>