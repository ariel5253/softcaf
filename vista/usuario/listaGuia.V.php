<?php
    session_start();
    if (!isset($_SESSION["id_usuario"])) {
        header("location: ../index.php?pg=login.V"); 
    }
?>

<script src="../../js/listaGuia.js"></script>

<div class="container bg-white py-3">
    <div class="card shadow">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Guias De La Red De Conocimiento</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped" id="dataTable2" width="100%" cellspacing="0">
                    <thead align="center" class="thead-dark">
                        <tr>
                            <th>N°</th>
                            <th>Nombre</th>
                            <th>Palabras Clave</th>
                            <th>Ficha</th>
                            <th>Competencia</th>
                            <th>Guia</th>
                            <th>Material</th>
                        </tr>
                    </thead> 
                    <tbody align="center">
                        <tr id="filao" class="primeraFilao">
                            <td></td>
                            <td id="aguiao"></td>
                            <td id="bguiao"></td>
                            <th id="cguiao"></th>
                            <th id="dguiao"></th> 
                            <th id="eguiao"><a id="aArchivoo" class="fas fa-file-pdf"></a></th>
                            <th id="fguiao">
                                <input type="button" id="btnVer" class="btn btn-secondary" name="btnVer" value="Ver material" data-toggle="modal" data-target="#modalMaterial">
                            </th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!--Hidden-->
<input type="hidden" id="hidIdSesion" name="hidIdSesion" value="<?php echo($_SESSION["id_usuario"]); ?>">
<input type="hidden" id="hidPersona" name="hidPersona" value="<?php echo($_SESSION["id_persona"]); ?>">
<input type="hidden" id="hidRedC" name="hidRedC"> 
<input type="hidden" id="hidIns" name="hidIns"> 
<input type="hidden" id="hidIdGuia" name="hidIdGuia"> 
<!-- The Modal -->
<div class="modal fade" id="modalMaterial" style="background: rgba(0, 0, 0, 0.8);">
    <div class="modal-dialog modal-xl">
      <div class="modal-content" style="background: rgba(0, 0, 0, 0.4); color: beige;">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Material de apoyo</h4>
          <button type="button" class="close" data-dismiss="modal" onclick="location.reload();">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body" style="background: rgba(0, 0, 0, 0.1);">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="table-responsive text-secondary">
                        <table class="table table-striped table-hover" id="dataTable3" width="100%" cellspacing="0">
                            <thead align="center" class="thead-dark">
                                <tr>
                                    <th>Nombre</th>
                                    <th>Palabra Clave</th>
                                    <th>Guia</th>
                                    <th>Archivo</th>
                                </tr>
                            </thead>
                            <tbody align="center">
                                <tr id="filamo" class="primeraFilamo">
                                    <td id="aguiamo"></td>
                                    <td id="bguiamo"></td>
                                    <th id="cguiamo"></th>
                                    <th id="eguiamo"><a id="aArchivomo" class="fas fa-file-pdf"></a></th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="location.reload();">Cerrar</button>
        </div>
        
      </div>
    </div>
  </div>

<script>ListaGuiaAp();</script>