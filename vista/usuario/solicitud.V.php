<?php
    session_start();
    if (!isset($_SESSION["id_usuario"])) {
        header("location: ../index.php?pg=login.V");
    }
?>

<script src="../../js/administrador/solicitud.js"></script>

<div class="container bg-white">
    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-3 bg-info">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-3"></div>
                        <div class="col-lg-6 shadow-lg my-5 bg-light">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h2 text-gray-900 mb-4">Solicitud</h1>
                                </div>
                                <!--Inicio de formulario-->
                                <form class="user" name="frmSolicitud" id="frmSolicitud" enctype="multipart/form-data" method="post">
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <input type="text" class="form-control form-control-user" name="txtAsunto" id="txtAsunto" placeholder="Asunto" maxlength="50">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <input type="text" class="form-control form-control-user" name="txtPrograma" id="txtPrograma" placeholder="Digite El Nombre Del Programa">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <select class="form-control"  name="cmbFicha" id="cmbFicha" aria-label="Default select example" style="font-size: 0.8rem; border-radius: 10rem; height: 50px;">
                                                <option value="" selected="selected">Seleccione La Ficha</option>
                                            </select>
                                        </div>
                                        
                                    </div>
                                    <hr>
                                    <div class="form-group row" align="center">
                                        <div class="col-sm-6">
                                            <button type="button" class="btn btn-success btn-circle btn-lg" id="btnAgregar"><i class="fas fa-check"></i></button>
                                        </div>
                                        <div class="col-sm-6">
                                            <a class="btn btn-warning btn-circle btn-lg" data-toggle="modal" data-target="#modalSolicitudUsuario">
                                                <i class="fas fa-clipboard-list"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <hr>
                                    <input type="hidden" id="hidEstado" name="hidEstado" value="En Proceso">
                                    <input type="hidden" id="hidPrograma" name="hidPrograma">
                                    <input type="hidden" id="hidInstructor" name="hidInstructor">
                                    <input type="hidden" id="hidAccion" name="hidAccion">
                                    <!--Agregar con la seseion-->
                                    <input type="hidden" id="hidIdSesion" name="hidIdSesion" value="<?php echo($_SESSION["id_usuario"]); ?>">
                                    <input type="hidden" id="hidPersona" name="hidPersona" value="<?php echo($_SESSION["id_persona"]); ?>">
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-3"></div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

<!-- The Modal -->
<div class="modal fade" id="modalSolicitudUsuario" style="background: rgba(0, 0, 0, 0.8);" align="center">
    <div class="modal-dialog modal-xl">
      <div class="modal-content" style="background: rgba(0, 0, 0, 0.4); color: beige; width: 80%;">
      
        <!-- Modal Header -->
        <div class="modal-header"> 
          <h4 class="modal-title">Lista De Solicitudes</h4>
          <button type="button" class="close" data-dismiss="modal" onclick="location.reload();">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body" style="background: rgba(0, 0, 0, 0.1);">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="table-responsive text-secondary">
                        <table class="table table-striped table-hover" id="dataTable13" width="100%" cellspacing="0">
                            <thead align="center" class="thead-dark">
                                <tr>
                                    <th>N°</th>
                                    <th>Asunto</th>
                                    <th>Ficha</th>
                                    <th>Estado</th>
                                </tr>
                            </thead>
                            <tbody align="center">
                                <tr id="filaSol" class="primeraFilaSol">
                                    <td></td>
                                    <td id="asol"></td>
                                    <td id="bsol"></td>
                                    <th id="csol"><input type="button" id="btnEstado" class="btn btn-secondary" name="btnEstado" disabled></th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="location.reload();">Cerrar</button>
        </div>
        
      </div>
    </div>
  </div>
<script>ListarParaUsuario();</script>
<script>ConsultarInstructor();</script>
