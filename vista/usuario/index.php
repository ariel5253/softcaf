<?php
    session_start();

    extract($_REQUEST);

    error_reporting(0);
    
    if (!isset($_SESSION["id_usuario"])) {
        header("location: ../index.php?pg=login.V");
    }

    if (isset($pg)) {
        $pagina=$pg.".php";
    } else {
        $pagina="inicio.V.php";
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Usuario</title>

    <!-- Custom fonts for this template-->
    <link href="../../libreria/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="../../componentes/css/sb-admin-2.min.css" rel="stylesheet">
    <link href="../../componentes/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom styles for this page -->
    <link href="../../libreria/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

    <script src="../../libreria/jquery/jquery.min.js"></script>
    <script src="../../libreria/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="../../libreria/jquery-easing/jquery.easing.min.js"></script>

    <!-- escript para autocomplete -->
    <link type="text/css" href="../../libreria/autocomplete/jquery/css/ui-lightness/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
    <link href="../../libreria/autocomplete/jquery/css/prettyLoader.css" rel="stylesheet" type="text/css" />
    <script src="../../libreria/autocomplete/jquery/js/jquery-1.7.1.min.js" ></script>
    <script src="../../libreria/autocomplete/jquery/js/jquery-ui-1.8.18.custom.min.js" ></script>
    <script src="../../libreria/autocomplete/jquery/js/jquery.prettyLoader.js"></script>
    <!-- fin script --> 
    
    <!-- kinic-->
    <!-- Page level plugins -->
    <script src="../../libreria/datatables/jquery.dataTables.min.js"></script> 
    <script src="../../libreria/datatables/dataTables.bootstrap4.min.js"></script>
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Inicio menu principal -->
        <div><?php include "menu.V.php" ?></div>
        <!-- Fin menu principal -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Inicio Menu 2 -->
                <div><?php include "../menud.V.php" ?></div>
                <!-- Fin Menu 2 -->

                <!-- Inicio Contenido De Inicio -->
                <div><?php include $pagina; ?></div>
                <!-- Fin Contenido De Inicio -->
                
            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <div><?php include "../footer.V.php" ?></div>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>    
    <!-- End of Page Wrapper -->
    <div><?php include "../modal.V.php" ?></div>
    <div><?php include "modalMaterial.V.php" ?></div>
    <div><?php include "modalSolicitudUsu.V.php" ?></div>
    <!-- Bootstrap core JavaScript-->

    <script src="../../js/sb-admin-2.min.js"></script>

</body>

</html>