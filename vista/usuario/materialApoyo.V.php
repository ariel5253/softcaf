<?php
    session_start();
    if (!isset($_SESSION["id_usuario"])) {
        header("location: ../index.php?pg=login.V");
    }
?>

<script src="../../js/materialApoyo.js"></script>

<div class="container bg-white">
    
    <!-- Outer Row -->
    <div class="row justify-content-center my-2">

        <div class="col-xl-10 col-lg-12 col-md-9" id="contMaterial">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-5 d-none d-lg-block bg-guia-image"></div>
                        <div class="col-lg-7">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Material De Apoyo</h1>
                                </div>
                                <!--Inicio de formulario-->
                                <form class="user" name="frmMaterialApoyo" id="frmMaterialApoyo" enctype="multipart/form-data" method="post">
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <input type="text" class="form-control form-control-user" name="txtNombreMaterial" id="txtNombreMaterial" placeholder="Nombre Del Material">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <input type="text" class="form-control form-control-user" name="txtPalabrasMaterial" id="txtPalabrasMaterial" placeholder="Palabras Claves De Material">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <input type="file"  id="txtFileMaterial" name="txtFileMaterial" accept=".pdf">
                                        </div>
                                    </div> 
                                    <hr>
                                    <hr>
                                    <div class="form-group row" align="center">
                                        <div class="col-sm-4">
                                            <button type="button" class="btn btn-success btn-circle btn-lg" id="btnAgregar"><i class="fas fa-plus-circle"></i></button>
                                            <p>Agregar</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <button type="button" class="btn btn-warning btn-circle btn-lg" id="btnListarFasPro" data-toggle="modal" data-target="#listaMatrialA"><i class="fab fa-readme"></i></button>
                                            <p>Listar</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <button type="button" class="btn btn-info btn-circle btn-lg" id="btnModificar"><i class="far fa-edit"></i></button>
                                            <p>Actualizar</p>
                                        </div>
                                    </div>
                                    <!--Hidden-->
                                    <input type="hidden" id="hidIdGuia" name="hidIdGuia" value="<?php echo $_REQUEST["bd"]; ?>">
                                    <input type="hidden" id="hidIdMaterial" name="hidIdMaterial">
                                    <input type="hidden" id="hidNombre" name="hidNombre">
                                    <input type="hidden" id="hidUrl" name="hidUrl">
                                    <input type="hidden" id="hidAccion" name="hidAccion">
                                    <!--sesion-->
                                    <input type="hidden" id="hidIdSesion" name="hidIdSesion" value="<?php echo($_SESSION["id_usuario"]); ?>">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <!-- Modal listar Fase -->
    <div class="modal fade" id="listaMatrialA">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
            
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Material De Apoyo</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="dataTable2" width="100%" cellspacing="0">
                                    <thead align="center" class="thead-dark">
                                        <tr>
                                            <th>N°</th>
                                            <th>Nombre</th>
                                            <th>Palabra Clave</th>
                                            <th>Guía</th>
                                            <th>Archivo</th>
                                            <th id="thfguia">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody align="center">
                                        <tr id="fila" class="primeraFila"> 
                                            <td></td>
                                            <td id="aguia"></td>
                                            <td id="bguia"></td>
                                            <th id="cguia"></th>
                                            <th id="eguia"><a id="aArchivo" class="fas fa-file-pdf"></a></th>
                                            <th id="fguia">
                                                <input type="button" id="btnEditar" class="btn btn-secondary" name="btnEditar" value="Editar">
                                            </th>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Modal footer -->
                <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
                
            </div>
        </div>
    </div>
    
</div>
<script>Listar();</script>
