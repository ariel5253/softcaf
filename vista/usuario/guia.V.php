<?php
    session_start();
    if (!isset($_SESSION["id_usuario"])) {
        header("location: ../index.php?pg=login.V");
    }
?>

<script src="../../js/guia.js"></script>

<div class="container bg-white">
    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-5 d-none d-lg-block bg-guia-image"></div>
                        <div class="col-lg-7">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Guía De Aprendizaje</h1>
                                </div>
                                <!--Inicio de formulario-->
                                <form class="user" name="frmGuia" id="frmGuia" enctype="multipart/form-data" method="post">
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <input type="text" class="form-control form-control-user" name="txtNombre" id="txtNombre" placeholder="Nombre">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <input type="text" class="form-control form-control-user" name="txtPalabras" id="txtPalabras" placeholder="Palabras Claves"> 
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <select class="form-control"  name="cmbFicha" id="cmbFicha" aria-label="Default select example" style="font-size: 0.8rem; border-radius: 10rem; height: 50px;">
                                                <option value="" selected="selected">Seleccione Ficha</option>
                                            </select>
                                        </div>
                                        
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <select class="form-select form-control" size="5" aria-label="size 5 Default select example" name="cmbComp" id="cmbComp" style="font-size: 0.8rem; overflow:scroll;">
                                                <option selected>Seleccione Competencias</option>
                                            </select>
                                        </div>
                                    </div> 

                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <select class="form-select form-control" name="cmbRap[]" id="cmbRap" multiple aria-label="multiple select example" style="font-size: 0.8rem; overflow:scroll;">
                                                <option selected>Seleccione Resultados De Aprendizaje</option>
                                            </select>
                                        </div>
                                    </div> 
                                    
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <input type="file"  id="txtFileGuia" name="txtFileGuia" accept=".pdf">
                                        </div>
                                    </div>
                                    <hr>
                                    <hr>
                                    <div class="form-group row" align="center">
                                        <div class="col-sm-4">
                                            <button type="button" class="btn btn-success btn-circle btn-lg" id="btnAgregar"><i class="fas fa-plus-circle"></i></button>
                                            <p>Agregar</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <button type="button" class="btn btn-warning btn-circle btn-lg" id="btnListarFasPro" data-toggle="modal" data-target="#listaGuiaA"><i class="fab fa-readme"></i></button>
                                            <p>Listar</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <button type="button" class="btn btn-info btn-circle btn-lg" id="btnModificar"><i class="far fa-edit"></i></button>
                                            <p>Actualizar</p>
                                        </div>
                                    </div>
                                    <!--Hidden-->
                                    <input type="hidden" id="hidIdGuia" name="hidIdGuia">
                                    <input type="hidden" id="hidNombre" name="hidNombre">
                                    <input type="hidden" id="hidUrl" name="hidUrl">
                                    <input type="hidden" id="hidAccion" name="hidAccion">

                                    <!--Agregar con la seseion-->
                                    <input type="hidden" id="hidIdSesion" name="hidIdSesion" value="<?php echo($_SESSION["id_usuario"]); ?>">
                                    <input type="hidden" id="hidPersona" name="hidPersona" value="<?php echo($_SESSION["id_persona"]); ?>">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <!-- Modal listar Fase -->
    <div class="modal fade" id="listaGuiaA">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
            
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Guías Que Le Pertenece</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="card shadow">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="dataTable1" width="100%" cellspacing="0">
                                    <thead align="center" class="thead-dark">
                                        <tr>
                                            <th>N°</th>
                                            <th>Nombre</th>
                                            <th>Palabra Clave</th>
                                            <th>Ficha</th>
                                            <th>Competencia</th>
                                            <th>Guia</th> 
                                            <th>Acciones</th>
                                            <th>Ver</th>
                                        </tr>
                                    </thead>
                                    <tbody align="center">
                                        <tr id="fila" class="primeraFila">
                                            <td></td>
                                            <td id="aguia"></td>
                                            <td id="bguia"></td>
                                            <th id="cguia"></th>
                                            <th id="dguia"></th>
                                            <th id="eguia"><a id="aArchivo" class="fas fa-file-pdf"></a></th>
                                            <th id="fguia">
                                                <input type="button" name="btnEditar" class="btn btn-secondary" id="btnEditar" value="Consultar" data-dismiss="modal">
                                            </th>
                                            <th id="gguia">
                                                <a class="btn btn-secondary btn-user btn-block" id="IdAgregarMaterial">
                                                    Material
                                                </a>
                                            </th>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Modal footer -->
                <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
                
            </div>
        </div>
    </div>
</div>

<script>Listar();</script>
<script>ListarFic();</script>