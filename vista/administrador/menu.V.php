<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-dark sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php?pg=inicio.V">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-user"></i>
        </div>
        <div class="sidebar-brand-text mx-1" style="font-size: 8px;"><?php echo($_SESSION["nombre_usuario"]); ?></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">
 
    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="index.php?pg=inicio.V">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span><?php echo($_SESSION["rol_usuario"]); ?></span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Interface
    </div>

    <li class="nav-item">
        <a class="nav-link collapsed" href="index.php?pg=solicitudAdm.V">
            <i class="fas fa-inbox"></i>
            <span>Solicitudes De Asignación De Fichas</span>
        </a>
    </li>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"
            aria-expanded="true" aria-controls="collapsePages">
            <i class="fas fa-fw fa-file-alt"></i>
            <span>Gestionar Usuarios</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="index.php?pg=personas.V">Usuario</a>
                <a class="collapse-item" href="index.php?pg=usuarioRoles.V">Asignación De Roles</a>
                <a class="collapse-item" href="index.php?pg=instructoresFicha.V">Asignación De Fichas</a>
            </div>
        </div>
    </li>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="index.php?pg=centrosFormacion.V" >
            <i class="fas fa-fw fa-clipboard-list"></i>
            <span>Centro De Formación</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseP"
            aria-expanded="true" aria-controls="collapsePages">
            <i class="fas fa-fw fa-file-alt"></i>
            <span>Competencias</span>
        </a>
        <div id="collapseP" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="index.php?pg=competencias.V">Agregar Competencia</a>
                <a class="collapse-item" href="index.php?pg=resultadoAprendizaje.V">Asignación De RAP</a>
            </div>
        </div>
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="index.php?pg=fichas.V" >
            <i class="fas fa-fw fa-clipboard-list"></i>
            <span>Ficha</span>
        </a>
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="index.php?pg=programasFormacion.V" >
            <i class="fas fa-fw fa-clipboard-list"></i>
            <span>Programa De Formacion</span>
        </a>
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="index.php?pg=redesConocimiento.V" >
            <i class="fas fa-fw fa-clipboard-list"></i>
            <span>Red De Conocimiento</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->