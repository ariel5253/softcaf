<?php
    session_start();
    if (!isset($_SESSION["id_usuario"])) {
        header("location: ../index.php?pg=login.V");
    }
?>

<script src="../../js/administrador/usuarioRoles.js"></script>

<div class="container bg-white">
    
    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5"> 
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Asignación de Roles</h1>
                                </div>
                                <!--Inicio de formulario-->
                                <form class="user was-validated" id="frmUsuarioRol" name="frmUsuarioRol">
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <input type="text" class="form-control form-control-user" id="txtUsurio" name="txtUsurio" placeholder="Usuario"  required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <input type="text" class="form-control form-control-user" id="txtPersona" name="txtPersona" placeholder="Nombre de la persona" disabled required>
                                        </div> 
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-6 mb-3 mb-sm-0">
                                            <select class="form-control" name="cmbRol" id="cmbRol" style="font-size: 0.8rem; border-radius: 10rem; height: 50px;" required>
                                                <option value="" selected="selected">Rol</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-6 mb-3 mb-sm-0">
                                            <select class="form-control" name="cmbEstado" id="cmbEstado" style="font-size: 0.8rem; border-radius: 10rem; height: 50px;" required>
                                                <option value="" selected="selected">Estado</option>
                                                <option value="ACTIVO">Activo</option>
                                                <option value="INACTIVO">Inactivo</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>
                                    <hr>
                                    <div class="form-group row" align="center">
                                        <div class="col-sm-4">
                                            <button type="button" class="btn btn-success btn-circle btn-lg" id="btnAgregar"><i class="fas fa-plus-circle"></i></button>
                                            <p>Agregar</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <button type="button" class="btn btn-warning btn-circle btn-lg" id="btnListarFasPro" data-toggle="modal" data-target="#modalUsuarioRol"><i class="fab fa-readme"></i></button>
                                            <p>Listar</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <button type="button" class="btn btn-info btn-circle btn-lg" id="btnModificar"><i class="far fa-edit"></i></button>
                                            <p>Actualizar</p>
                                        </div>
                                    </div>
                                    <!--Hidden-->
                                    <input type="hidden" id="hidUsuarioRol" name="hidUsuarioRol">
                                    <input type="hidden" id="hidUsuario" name="hidUsuario">
                                    <input type="hidden" id="hidAccion" name="hidAccion">
                                    <input type="hidden" id="hidIdSesion" name="hidIdSesion" value="<?php echo($_SESSION["id_usuario"]); ?>">
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-6 d-none d-lg-block bg-usuarioRol-image"></div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
<!-- The Modal -->
<div class="modal fade" id="modalUsuarioRol" style="background: rgba(0, 0, 0, 0.8);">
        <div class="modal-dialog modal-xl">
            <div class="modal-content" style="background: rgba(0, 0, 0, 0.4); color: beige;">
            
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Asignacion De Roles</h4>
                    <button type="button" class="close" data-dismiss="modal" onclick="location.reload();">&times;</button>
                </div>
                
                <!-- Modal body -->
                <div class="modal-body" style="background: rgba(0, 0, 0, 0.1);">
                    <div class="container">
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <div class="table-responsive text-secondary">
                                <table class="table table-striped table-hover" id="dataTable11" width="100%" cellspacing="0">
                                    <thead align="center" class="thead-dark">
                                        <tr>
                                            <th>N°</th>
                                            <th>Nombre Usuario</th>
                                            <th>Usuario</th>
                                            <th>Rol</th>
                                            <th>Estado</th>
                                            <th>Acción</th>
                                        </tr>
                                    </thead>
                                    <tbody align="center">
                                        <tr id="filaUsu" class="primeraFilaUsu">
                                            <td></td>
                                            <td id="ausu"></td>
                                            <td id="busu"></td>
                                            <td id="cusu"></td>
                                            <td id="dusu"></td>
                                            <td id="eusu">
                                                <input type="button" name="btnEditarUsuarioRol" class="btn btn-secondary" id="btnEditarUsuarioRol" value="Consultar" data-dismiss="modal">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="location.reload();">Cerrar</button>
                </div>
                
            </div>
        </div>
    </div>
    <script>ListarRol();</script>
<script>Listar();</script>
