<?php
    session_start();
    if (!isset($_SESSION["id_usuario"])) {
        header("location: ../index.php?pg=login.V");
    }
?>

<script src="../../js/administrador/programasFormacion.js"></script>

<div class="container bg-white">
    
    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-7">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Programas De Formación</h1>
                                </div>
                                <!--Inicio de formulario-->
                                <form class="user was-validated" id="frmProgramaFormacion" name="frmProgramaFormacion">
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <input type="number" class="form-control form-control-user" id="txtCodigo" name="txtCodigo" placeholder="Ingrese Código De Programa" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <input type="text" class="form-control form-control-user" id="txtNombre" name="txtNombre" placeholder="Ingrese Nombre Del Programa" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <input type="text" class="form-control form-control-user" id="txtRed" name="txtRed" placeholder="Ingrese Red De Conocimiento" required>
                                        </div>
                                    </div>
                                    <hr>
                                    <hr>
                                    <div class="form-group row" align="center">
                                        <div class="col-sm-6">
                                            <button type="button" class="btn btn-success btn-circle btn-lg" id="btnAgregar"><i class="fas fa-plus-circle"></i></button>
                                            <p>Agregar</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <button type="button" class="btn btn-info btn-circle btn-lg" id="btnModificar"><i class="far fa-edit"></i></button>
                                            <p>Actualizar</p>
                                        </div>
                                    </div>
                                    <!--Hidden-->
                                    <input type="hidden" id="hidProgramaFormacion" name="hidProgramaFormacion">
                                    <input type="hidden" id="hidRedConocimiento" name="hidRedConocimiento">
                                    <input type="hidden" id="hidAccion" name="hidAccion">
                                    <input type="hidden" id="hidIdSesion" name="hidIdSesion" value="<?php echo($_SESSION["id_usuario"]); ?>">
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-5 d-none d-lg-block bg-programas-image"></div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Programas De Formación</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped" id="dataTable7" width="100%" cellspacing="0">
                    <thead align="center" class="thead-dark">
                        <tr>
                            <th>N°</th>
                            <th>Código</th>
                            <th>Programa De Formación</th>
                            <th>Red De Conocimiento</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tbody align="center">
                        <tr id="filapro" class="primeraFilapro">
                            <td></td>
                            <td id="apro"></td>
                            <td id="bpro"></td>
                            <td id="cpro"></td>
                            <td id="dpro">
                                <input type="button" name="btnEditar" class="btn btn-secondary" id="btnEditar" value="Consultar">
                            </td>
                        </tr>
                    </tbody>
                </table> 
            </div>
        </div>
    </div>
</div>
<script>Listar();</script>