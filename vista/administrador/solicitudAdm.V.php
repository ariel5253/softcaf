<?php
    session_start();
    if (!isset($_SESSION["id_usuario"])) {
        header("location: ../index.php?pg=login.V");
    }
?>

<script src="../../js/administrador/solicitud.js"></script>

<div class="container bg-white py-5" align="center">
    
    <div class="row py-5">
        <div class="col-sm-4 py-5"></div>
        <div class="col-sm-4 py-5" data-toggle="modal" data-target="#modalSolicitudAdminis">
            <button class="btn btn-outline-info">
                <h5 class="card-title">Lista De Solicitudes</h5>
                <i class="fas fa-file-invoice fa-10x"></i>
            </button>
        </div>
        <div class="col-sm-4 py-5"></div>
    </div>
</div>

<!-- The Modal -->
<div class="modal fade" id="modalSolicitudAdminis" style="background: rgba(0, 0, 0, 0.8);" align="center">
    <div class="modal-dialog modal-xl">
      <div class="modal-content" style="background: rgba(0, 0, 0, 0.4); color: beige; width: 90%;">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Lista De Solicitudes</h4>
          <button type="button" class="close" data-dismiss="modal" onclick="location.reload();">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body" style="background: rgba(0, 0, 0, 0.1);">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="table-responsive text-secondary">
                        <table class="table table-striped table-hover" id="dataTable14" width="100%" cellspacing="0">
                            <thead align="center" class="thead-dark">
                                <tr>
                                    <th>N°</th>
                                    <th>Instructor</th>
                                    <th>Asunto</th>
                                    <th>Ficha</th>
                                    <th>Estado</th>
                                </tr>
                            </thead>
                            <tbody align="center">
                                <tr id="filaAdm" class="primeraFilaAdm">
                                    <td></td>
                                    <td id="aadm"></td>
                                    <td id="badm"></td>
                                    <td id="cadm"></td>
                                    <th id="dadm"><input type="button" id="btnEstado" class="btn btn-secondary" name="btnEstado"></th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="hidEstado" name="hidEstado" value="Aprobado">
        <input type="hidden" id="hidIdSesion" name="hidIdSesion" value="<?php echo($_SESSION["id_usuario"]); ?>">
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="location.reload();">Cerrar</button>
        </div>
        
      </div>
    </div>
  </div>

<script>ListarParaAdministrador();</script>