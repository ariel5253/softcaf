<?php
    session_start();
    if (!isset($_SESSION["id_usuario"])) {
        header("location: ../index.php?pg=login.V");
    }
?>

<script src="../../js/administrador/resultadoAprendizaje.js"></script>

<div class="container bg-white">
    
    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5"> 
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-5 d-none d-lg-block bg-competencia-image"></div>
                        <div class="col-lg-7">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Resultado De Aprendizaje</h1>
                                </div>
                                <!--Inicio de formulario-->
                                <form class="user was-validated" id="frmRap" name="frmRap">
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <input type="text" class="form-control form-control-user" id="txtNombre" name="txtNombre" placeholder="Ingrese Nombre Del Resultado De Aprendizaje" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <input type="text" class="form-control form-control-user" id="txtCompetencia" name="txtCompetencia" placeholder="Ingrese El Nombre De La Competencia" required>
                                        </div>
                                    </div>
                                    <hr>
                                    <hr>
                                    <div class="form-group row" align="center">
                                        <div class="col-sm-4">
                                            <button type="button" class="btn btn-success btn-circle btn-lg" id="btnAgregar"><i class="fas fa-plus-circle"></i></button>
                                            <p>Agregar</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <button type="button" class="btn btn-warning btn-circle btn-lg" id="btnListarFasPro" data-toggle="modal" data-target="#modalRap"><i class="fab fa-readme"></i></button>
                                            <p>Listar</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <button type="button" class="btn btn-info btn-circle btn-lg" id="btnModificar"><i class="far fa-edit"></i></button>
                                            <p>Actualizar</p>
                                        </div>
                                    </div>
                                    <!--Hidden-->
                                    <input type="hidden" id="hidResultadoA" name="hidResultadoA">
                                    <input type="hidden" id="hidCompetencia" name="hidCompetencia">
                                    <input type="hidden" id="hidAccion" name="hidAccion">
                                    <input type="hidden" id="hidIdSesion" name="hidIdSesion" value="<?php echo($_SESSION["id_usuario"]); ?>">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
<!-- The Modal -->
<div class="modal fade" id="modalRap" style="background: rgba(0, 0, 0, 0.8);">
        <div class="modal-dialog modal-xl">
            <div class="modal-content" style="background: rgba(0, 0, 0, 0.4); color: beige;">
            
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Resultados De Aprendizaje</h4>
                    <button type="button" class="close" data-dismiss="modal" onclick="location.reload();">&times;</button>
                </div>
                
                <!-- Modal body -->
                <div class="modal-body" style="background: rgba(0, 0, 0, 0.1);">
                    <div class="container">
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <div class="table-responsive text-secondary">
                                <table class="table table-striped table-hover" id="tablaComp" width="100%" cellspacing="0">
                                    <thead align="center" class="thead-dark">
                                        <tr>
                                            <th>N°</th>
                                            <th>Nombre</th>
                                            <th>Competencia</th>
                                            <th>Acción</th>
                                        </tr>
                                    </thead>
                                    <tbody align="center">
                                        <tr id="fila" class="primeraFila">
                                            <td></td>
                                            <td id="aRap"></td>
                                            <td id="bRap"></td>
                                            <td id="cRap">
                                                <input type="button" name="btnEditar" class="btn btn-secondary" id="btnEditar" value="Consultar" data-dismiss="modal">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="location.reload();">Cerrar</button>
                </div>
                
            </div>
        </div>
    </div>
<script>Listar();</script>