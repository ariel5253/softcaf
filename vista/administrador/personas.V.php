<?php
    session_start();
    if (!isset($_SESSION["id_usuario"])) {
        header("location: ../index.php?pg=login.V");
    }
?>

<script src="../../js/administrador/personas.js"></script>

<div class="container bg-white"> 
      
    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Nuevo Usuario</h1>
                                </div>
                                <!--Inicio de formulario-->
                                <form class="user was-validated" id="frmPersona" name="frmPersona">
                                    <hr>
                                    <div class="text-center">
                                        <h5 class="h5 text-gray-900 mb-4">Datos Personales</h5>
                                    </div>
                                    <hr>
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <input type="text" class="form-control form-control-user" id="txtNombres"
                                                placeholder="Ingrese Nombres" name="txtNombres" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <input type="text" class="form-control form-control-user" id="txtapellidos"
                                                placeholder="Ingrese Apellidos" name="txtapellidos" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <select class="form-control" name="cmbTipoDocumento" id="cmbTipoDocumento" style="font-size: 0.8rem; border-radius: 10rem; height: 50px;" required>
                                                <option value="" selected="selected">Seleccione Tipo De Documento</option>
                                                <option value="CE">C.E</option>
                                                <option value="NIP">N.I.P</option>
                                                <option value="NIC">N.I.C</option>
                                                <option value="TI">T.I</option>
                                                <option value="CC">C.C</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <input type="number" name="txtDocumento" class="form-control form-control-user" id="txtDocumento"
                                                placeholder="Ingrese Numero De Documento" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <select class="form-control" name="cmbTipoSangre" id="cmbTipoSangre" style="font-size: 0.8rem; border-radius: 10rem; height: 50px;" required>
                                                <option value="" selected="selected">Seleccione Tipo Sangre</option>
                                                <option value="O+">O+</option>
                                                <option value="A+">A+</option>
                                                <option value="A-">A-</option>
                                                <option value="B+">B+</option>
                                                <option value="B-">B-</option>
                                                <option value="AB-">AB-</option>
                                                <option value="AB+">AB+</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <select class="form-control" name="cmbGenero" id="cmbGenero" style="font-size: 0.8rem; border-radius: 10rem; height: 50px;" required>
                                                <option value="" selected="selected">Seleccione Género</option>
                                                <option value="Femenino">Femenino</option>
                                                <option value="Masculino">Masculino</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <input type="number" name="txtEdad" class="form-control form-control-user" id="txtEdad"
                                                placeholder="Ingrese Edad" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <input type="number" class="form-control form-control-user" id="txtTelefono"
                                                placeholder="Ingrese Teléfono" name="txtTelefono" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <input type="email" class="form-control form-control-user" id="txtCorreo"
                                                placeholder="Ingrese Correo Electrónico" name="txtCorreo" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <input type="text" class="form-control form-control-user" id="txtDireccion"
                                                placeholder="Ingrese La  Dirección" name="txtDireccion" required>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="text-center">
                                        <h5 class="h5 text-gray-900 mb-4">Otros Datos</h5>
                                    </div>
                                    <hr>
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <select class="form-control" name="cmbRol" id="cmbRol" style="font-size: 0.8rem; border-radius: 10rem; height: 50px;" required>
                                                <option value="" selected="selected">Seleccione Rol</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control form-control-user" id="txtRedConocimiento"
                                                placeholder="Digite La Red De Conocimiento" name="txtRedConocimiento" required>
                                        </div>
                                    </div>
                                    <hr>
                                    <hr>
                                    <div class="form-group row" align="center">
                                        <div class="col-sm-4">
                                            <button type="button" class="btn btn-success btn-circle btn-lg" id="btnAgregar"><i class="fas fa-plus-circle"></i></button>
                                            <p>Agregar</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <button type="button" class="btn btn-warning btn-circle btn-lg" id="btnListarFasPro" data-toggle="modal" data-target="#modalPersona"><i class="fab fa-readme"></i></button>
                                            <p>Listar</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <button type="button" class="btn btn-info btn-circle btn-lg" id="btnModificar"><i class="far fa-edit"></i></button>
                                            <p>Actualizar</p>
                                        </div>
                                    </div>
                                    <!--Hidden-->
                                    <input type="hidden" id="hidPersona" name="hidPersona">
                                    <input type="hidden" id="hidCorreo" name="hidCorreo">
                                    <input type="hidden" id="hidAccion" name="hidAccion">
                                    <input type="hidden" id="hidRedConocimiento" name="hidRedConocimiento">
                                    <input type="hidden" id="hidIdSesion" name="hidIdSesion" value="<?php echo($_SESSION["id_usuario"]); ?>">
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-6" style="background: orange;"></div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>
<!-- The Modal -->
<div class="modal fade" id="modalPersona" style="background: rgba(0, 0, 0, 0.8);">
        <div class="modal-dialog modal-xl">
            <div class="modal-content" style="background: rgba(0, 0, 0, 0.4); color: beige;">
            
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Usuarios</h4>
                    <button type="button" class="close" data-dismiss="modal" onclick="location.reload();">&times;</button>
                </div>
                
                <!-- Modal body -->
                <div class="modal-body" style="background: rgba(0, 0, 0, 0.1);">
                    <div class="container">
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <div class="table-responsive text-secondary">
                                <table class="table table-striped table-hover" id="dataTable10" width="100%" cellspacing="0">
                                    <thead align="center" class="thead-dark">
                                        <tr>
                                            <th>N°</th>
                                            <th>Nombre</th>
                                            <th>Tipo Documento</th>
                                            <th>Número Documento</th>
                                            <th>Teléfono</th>
                                            <th>Correo Electrónico</th>
                                            <th>Acción</th>
                                        </tr>
                                    </thead>
                                    <tbody align="center">
                                        <tr id="filaPer" class="primeraFilaPer">
                                            <td></td>
                                            <td id="aper"></td>
                                            <td id="bper"></td>
                                            <td id="cper"></td>
                                            <td id="dper"></td>
                                            <td id="eper"></td>
                                            <td id="fper">
                                                <input type="button" name="btnEditar" class="btn btn-secondary" id="btnEditar" value="Consultar" data-dismiss="modal">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="location.reload();">Cerrar</button>
                </div>
                
            </div>
        </div>
    </div>
<script>Listar();</script>
<script>ListarRol();</script>