<div class="container bg-white">
    
    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-block bg-usuario-image"></div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Datos De Cuenta</h1>
                                </div>
                                <!--Inicio de formulario-->
                                <form class="user">
                                    <div class="form-group">
                                        <input type="email" class="form-control form-control-user" id="txtCorreo"
                                            placeholder="Correo Electrónico">
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 mb-3 mb-sm-0">
                                            <input type="password" class="form-control form-control-user"
                                                id="txtPassword" placeholder="Contraseña">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <a href="" class="btn btn-success btn-user btn-block">
                                                Registrar
                                            </a>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <a href="" class="btn btn-danger btn-user btn-block">
                                                Cancelar
                                            </a>
                                        </div>
                                    </div>
                                </form>
                                <hr>
                                <div class="text-center">
                                    <a class="small" href="index.php?pg=login.V">Ya tienes una cuenta? Acceso! </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>