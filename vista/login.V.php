<script src="../js/login.js"></script>
<div class="container bg-white">
    
    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Iniciar Sesión</h1>
                                </div>
                                <form class="user" name="frmLogin" id="frmLogin" method="POST">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-user" name="txtCorreo" id="txtCorreo" aria-describedby="emailHelp"
                                            placeholder="Correo Electrónico">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control form-control-user" name="txtContraseña" id="txtContraseña" placeholder="Contraseña">
                                    </div>
                                    <div class="col-sm-12 mb-3 mb-sm-0" align="center">
                                        <div class="g-recaptcha" style="margin-top: 10px; margin-bottom: 10px;" data-sitekey="6Ld29hsbAAAAAFmOvTS_WH4ofGoEYn5TEvseYVo1"></div>
                                    </div>
                                    <hr>
                                    <div class="form-group row"> 
                                        <div class="col-sm-12">
                                            <input type="button" name="btnIngresar" id="btnIngresar" value="Ingresar" class="btn btn-success btn-user btn-block" onclick="login();">
                                        </div>
                                    </div>
                                    <!--Hidden-->
                                    <input type="hidden" id="accion" name="accion" value="">
                                </form>
                                <hr>
                                <div class="text-center">
                                    <a class="small" href="index.php?pg=olvideContraseña.V">¿Has Olvidado Tu Contraseña?</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>