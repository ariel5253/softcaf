<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; Todos Los Derechos Reservados | Tecnólogo en Análisis y Desarrollo de Sistemas de Información 2052637</span>
        </div>
    </div>
</footer>
<!-- End of Footer -->