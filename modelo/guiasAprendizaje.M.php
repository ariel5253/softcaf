<?php
 class GuiasAprendizaje{ //Clase de GuiasAprendizaje.
        //Atributos.
        private $idGuiasAprendizajePk;
        private $nombre;
        private $palabrasClave;
        private $url;
        private $idFichaFk;
        private $idCompetenciaFk;
        private $fechaCreacion;
        private $fechaActualizacion;
        private $idUsuarioCreacion;
        private $idUsuarioActualizacion;
        public  $conn=null;

        //Los métodos get y set,para mostrar (get) o modificar (set) el valor de un atributo.
       //Set y get del atributo idGuiasAprendizajePk.
        public function getIdGuiasAprendizajePk(){  
            return $this->idGuiasAprendizajePk;
        }
        public function setIdGuiasAprendizajePk($idGuiasAprendizajePk){
            $this->idGuiasAprendizajePk=$idGuiasAprendizajePk;
        }
        //Set y get del atributo nombre.
        public function  setNombre($nombre){
            $this->nombre=$nombre;
        }
        public function getNombre(){
            return $this->nombre;
        }
        //Set y get del atributo palabrasClave.
        public function  setPalabrasClave($palabrasClave){
            $this->palabrasClave=$palabrasClave;
        }
        public function getPalabrasClave(){
            return $this->palabrasClave;
        }
        //Set y get del atributo url.
        public function  setUrl($url){
            $this->url=$url;
        }
        public function getUrl(){
            return $this->url;
        }
        //Set y get del atributo idFichaFk.
        public function setIdFichaFk($idFichaFk){
            $this->idFichaFk=$idFichaFk;
        }
        public function  getIdFichaFk(){
            return $this ->idFichaFk;
        }
        //Set y get del atributo idCompetenciaFk.
        public function setIdCompetenciaFk($idCompetenciaFk){
            $this->idCompetenciaFk=$idCompetenciaFk;
        }
        public function  getIdCompetenciaFk(){
            return $this ->idCompetenciaFk;
        }
        //Set y get del atributo fechaCreación.
        public function getFechaCreacion(){ 
            return $this->fechaCreacion;
        }
        public function setFechaCreacion($fechaCreacion){ 
            $this->fechaCreacion =$fechaCreacion;
        }
        //Set y get del atributo fechaActualización.
        public function getFechaActualizacion(){ 
            return $this->fechaActualizacion;
        }
        public function setFechaActualizacion($fechaActualizacion){
             $this->fechaActualizacion =$fechaActualizacion;
            }
        //Set y get del atributo idUsuarioCreación.
        public function getIdUsuarioCreacion(){ 
            return $this->idUsuarioCreacion;
        }
        public function setIdUsuarioCreacion($idUsuarioCreacion){ 
            $this->idUsuarioCreacion =$idUsuarioCreacion;
        }
        //Set y get del atributo idUsuarioActualización.
        public function getIdUsuarioActualizacion(){ 
            return $this->idUsuarioActualizacion;
        }
        public function setIdUsuarioActualizacion($idUsuarioActualizacion){ 
            $this->idUsuarioActualizacion =$idUsuarioActualizacion;
        }
        //conexion
        public function __construct(){
            $this->conn = new Conexion(); 
        } 

        public function agregar(){
            $sentenciaSql = "INSERT INTO guias_aprendizaje(nombre
                                                            ,palabrasClave
                                                            ,ficha
                                                            ,competencia
                                                            ,url
                                                            ,fecha_creacion
                                                            ,fecha_actualizacion
                                                            ,id_usuario_creacion
                                                            ,id_usuario_actualizacion)
                                                        VALUES ('$this->nombre'
                                                            ,'$this->palabrasClave'
                                                            ,$this->idFichaFk
                                                            ,$this->idCompetenciaFk
                                                            ,'$this->url'
                                                            ,curdate()
                                                            ,curdate()
                                                            ,$this->idUsuarioCreacion
                                                            ,$this->idUsuarioActualizacion
                                                            );";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
        }

        public function modificar(){
            $sentenciaSql = "UPDATE guias_aprendizaje SET nombre='$this->nombre'
                                                            ,palabrasClave='$this->palabrasClave'
                                                            ,ficha=$this->idFichaFk
                                                            ,competencia=$this->idCompetenciaFk
                                                            ,url='$this->url'
                                                            ,fecha_actualizacion=curdate()
                                                            ,id_usuario_actualizacion=$this->idUsuarioActualizacion
                                                    WHERE id_guias_aprendizaje_pk = $this->idGuiasAprendizajePk;";
            $this->conn->Preparar($sentenciaSql);
            $this->conn->Ejecutar();     
        }

        public function ultimo_id(){
            $sentenciaSql = "SELECT MAX(id_guias_aprendizaje_pk)+1 AS id FROM guias_aprendizaje;";        
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true;
        }

        public function consultarParaListarGuias(){
            $sentenciaSql = "SELECT 
                                gu.id_guias_aprendizaje_pk
                                ,gu.nombre AS nombre_guia
                                ,gu.palabrasClave AS palabras_guia
                                ,fi.codigo AS codigo_ficha
                                ,co.nombre AS nombre_competencia
                                ,gu.url AS url_guia
                                ,gu.ficha AS id_ficha
                                ,gu.competencia AS id_competencia
                                ,gu.id_usuario_creacion
                                ,red.id_redes_conocimiento_pk AS id_red
                            FROM 
                                guias_aprendizaje AS gu
                                inner join fichas AS fi ON fi.id_ficha_pk = gu.ficha
                                inner join competencias AS co ON co.id_competencia_pk = gu.competencia
                                inner join programas_formacion AS pro ON pro.id_programas_formacion_pk = fi.programa
                                inner join redes_conocimiento AS red ON red.id_redes_conocimiento_pk = pro.red_conocimiento;";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function consultarParaListarCompetencias(){
            $sentenciaSql = "SELECT
                                competencias.*
                            FROM
                            fichas AS fi
                                INNER JOIN programas_formacion ON programas_formacion.id_programas_formacion_pk = fi.programa
                                INNER JOIN competencias ON competencias.programa = programas_formacion.id_programas_formacion_pk
                            WHERE fi.id_ficha_pk = $this->idFichaFk;";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function consultar(){
            if ($this->idGuiasAprendizajePk != '' ){
                $sentenciaSql = "SELECT * FROM guias_aprendizaje WHERE id_guias_aprendizaje_pk = $this->idGuiasAprendizajePk;";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        /*
        public function agregar(){
            $sentenciaSql = "call agregar_guia('$this->nombre','$this->palabrasClave',$this->idFichaFk,$this->idCompetenciaFk,'$this->url',$this->idUsuarioCreacion,$this->idUsuarioActualizacion);";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
        }

        public function modificar(){
            $sentenciaSql = "call modificar_guia($this->idGuiasAprendizajePk,'$this->nombre','$this->palabrasClave',$this->idFichaFk,$this->idCompetenciaFk,'$this->url',$this->idUsuarioActualizacion);";
            $this->conn->Preparar($sentenciaSql);
            $this->conn->Ejecutar();     
        }

        public function eliminar(){
            $sentenciaSql = "call eliminar_guia($this->idGuiaPk);";        
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
        }

        public function ultimo_id(){
            $sentenciaSql = "call consultar_ultimo_id_guia();";        
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true;
        }

        public function consultarParaListarGuias(){
            $sentenciaSql = "call listar_guia();";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function consultarParaListarCompetencias(){
            $sentenciaSql = "call listar_competencia($this->idFichaFk);";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function consultar(){
            if ($this->idGuiasAprendizajePk != '' ){
                $sentenciaSql = "call consultar_guia($this->idGuiasAprendizajePk);";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }
        */

        public function __destruct() {
            unset($this->idGuiasAprendizajePk);
            unset($this->nombre);
            unset($this->palabrasClave);
            unset($this->url);
            unset($this->idFichaFk);
            unset($this->idCompetenciaFk);
            unset($this->fechaCreacion);
            unset($this->fechaActualizacion);
            unset($this->idUsuarioCreacion);
            unset($this->idUsuarioActualizacion);
            unset($this->conn);
        }
    }
?>