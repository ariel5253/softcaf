<?php 
    class Departamentos{ //Clase de Departamentos.
        //Atributos.
        private $idDepartamentoPk;
        private $nombre;
        private $fechaCreacion;
        private $fechaActualizacion;
        private $idUsuarioCreacion;
        private $idUsuarioActualizacion;
        public  $conn=null;
        //Los métodos get y set,para mostrar(get) o modificar (set) el valor de un atributo.
       //Set y get del atributo idDepartamentoPk.
        public function setIdDepartamentoPk($idDepartamentoPk){
            $this->idDepartamentoPk=$idDepartamentoPk;
        }
        public function getIdDepartamentoPk(){
            return $this->idDepartamentoPk;
        }
        //Set y get del atributo Nombre.
        public function getNombre(){
            return $this->nombre;
        }
        public function setNombre($nombre){
            $this->nombre = $nombre;
        }
        //Set y get del atributo fechaCreación.
        public function getFechaCreacion(){ 
            return $this->fechaCreacion;
        }
        public function setFechaCreacion($fechaCreacion){ 
            $this->fechaCreacion =$fechaCreacion;
        }
        //Set y get del atributo fechaActualización.
        public function getFechaActualizacion(){ 
            return $this->fechaActualizacion;
        }
        public function setFechaActualizacion($fechaActualizacion){
             $this->fechaActualizacion =$fechaActualizacion;
            }
        //Set y get del atributo idUsuarioCreación.
        public function getIdUsuarioCreacion(){ 
            return $this->idUsuarioCreacion;
        }
        public function setIdUsuarioCreacion($idUsuarioCreacion){ 
            $this->idUsuarioCreacion =$idUsuarioCreacion;
        }
        //Set y get del atributo idUsuarioActualización.
        public function getIdUsuarioActualizacion(){ 
            return $this->idUsuarioActualizacion;
        }
        public function setIdUsuarioActualizacion($idUsuarioCreacion){ 
            $this->idUsuarioActualizacion =$idUsuarioActualizacion;
        }
    }
?>
    
      
   



