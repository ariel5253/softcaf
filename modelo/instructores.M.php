<?php
    class Instructores{ //Clase de Instructores.
        //Atributos.
        private $idInstructorPk;
        private $idPersonaFk;
        private $idRedConocimientoFk;
        private $fechaCreacion;
        private $fechaModificacion;
        private $idUsuarioCreacion;
        private $idUsuarioModificacion;
        public $conn=null;
        //Los métodos get y set,para mostrar(get) o modificar (set) el valor de un atributo.
        //Set y get del atributo idInstructorPk.
        public function getIdInstructorPk(){
            return $this->idInstructorPk;
        }
        public function setIdInstructorPk($idInstructorPk){
            $this->idInstructorPk = $idInstructorPk;
        }
        //Set y get del atributo idPersonaFk.
        public function getIdPersonaFk(){
            return $this->idPersonaFk;
        }
        public function setIdPersonaFk($idPersonaFk){
            $this->idPersonaFk = $idPersonaFk;
        }    
        //Set y get del atributo idRedConocimientoFk.
        public function getIdRedConocimientoFk(){
            return $this->idRedConocimientoFk;
        }
        public function setIdRedConocimientoFk($idRedConocimientoFk){
            $this->idRedConocimientoFk = $idRedConocimientoFk;
        }
        //Set y get del atributo fechaCreación.
        public function getFechaCreacion(){ 
            return $this->fechaCreacion;
        }
        public function setFechaCreacion($fechaCreacion){ 
            $this->fechaCreacion =$fechaCreacion;
        }
        //Set y get del atributo fechaActualización.
        public function getFechaActualizacion(){ 
            return $this->fechaActualizacion;
        }
        public function setFechaActualizacion($fechaActualizacion){
             $this->fechaActualizacion =$fechaActualizacion;
            }
        //Set y get del atributo idUsuarioCreación.
        public function getIdUsuarioCreacion(){ 
            return $this->idUsuarioCreacion;
        }
        public function setIdUsuarioCreacion($idUsuarioCreacion){ 
            $this->idUsuarioCreacion =$idUsuarioCreacion;
        }
        //Set y get del atributo idUsuarioActualización.
        public function getIdUsuarioActualizacion(){ 
            return $this->idUsuarioActualizacion;
        }
        public function setIdUsuarioActualizacion($idUsuarioCreacion){ 
            $this->idUsuarioActualizacion =$idUsuarioActualizacion;
        }
    }
?>