<?php
    class Usuarios{  //Clase de Usuarios.
        //Atributos.
        private $idUsuarioPk;
        private $usuario;
        private $password;
        private $idPersonaFk;
        private $fechaCreacion;
        private $fechaActualizacion;
        private $idUsuarioCreacion;
        private $idUsuarioActualizacion;
        public  $conn=null;
        //Los métodos get y set,(get) o modificar (set) el valor de un atributo.
       //Set y get del atributo idUsuarioPk.
        public function getIdUsuarioPk(){
            return $this->idUsuarioPk;
        }
        public function setIdUsuarioPk($idUsuarioPk){
            $this->idUsuarioPk = $idUsuarioPk;
        }
        //Set y get del atributo Usuario.
        public function getUsuario(){
            return $this->usuario;
        }
        public function setUsuario($usuario){
            $this->usuario = $usuario;
        }  
        //Set y get del atributo password.
        public function getPassword(){
            return $this->password;
        }
        public function setPassword($password){
            $this->password = $password;
        }
        //Set y get del atributo idPersonaFk.
        public function getIdPersonaFk(){
            return $this->idPersonaFk;
        }
        public function setIdPersonaFk($idPersonaFk){
            $this->idPersonaFk = $idPersonaFk;
        }  
        //Set y get del atributo fechaCreación.
        public function getFechaCreacion(){ 
            return $this->fechaCreacion;
        }
        public function setFechaCreacion($fechaCreacion){ 
            $this->fechaCreacion =$fechaCreacion;
        }
        //Set y get del atributo fechaActualización.
        public function getFechaActualizacion(){ 
            return $this->fechaActualizacion;
        }
        public function setFechaActualizacion($fechaActualizacion){
             $this->fechaActualizacion =$fechaActualizacion;
            }
        //Set y get del atributo idUsuarioCreación.
        public function getIdUsuarioCreacion(){ 
            return $this->idUsuarioCreacion;
        }
        public function setIdUsuarioCreacion($idUsuarioCreacion){ 
            $this->idUsuarioCreacion =$idUsuarioCreacion;
        }
        //Set y get del atributo idUsuarioActualización.
        public function getIdUsuarioActualizacion(){ 
            return $this->idUsuarioActualizacion;
        }
        public function setIdUsuarioActualizacion($idUsuarioActualizacion){ 
            $this->idUsuarioActualizacion =$idUsuarioActualizacion;
        }

        //conexion
        public function __construct(){
            $this->conn = new Conexion(); 
        }

        /*
        public function consultar(){
            if ($this->usuario != '' && $this->password != '' ){
                $sentenciaSql = "call consultar_usuario('$this->usuario','$this->password');";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }
        */

        public function consultar(){
            if ($this->usuario != '' && $this->password != '' ){
                $sentenciaSql = "SELECT
                                    *
                                FROM
                                    usuarios
                                WHERE usuario like '$this->usuario' && password like '$this->password';";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function consultarUsuarioRol(){
            if ($this->usuario != ''){
                $sentenciaSql = "SELECT
                                    usu.id_usuario_pk AS id_usuario
                                    ,usu.usuario AS nombre_usuario
                                    ,per.id_persona_pk AS id_persona
                                    ,concat(per.nombres,' ',per.apellido) AS nombre_persona
                                    ,rol.nombre AS rol_usuario
                                FROM
                                    
                                    usuario_roles AS usr
                                    inner join usuarios AS usu ON usu.id_usuario_pk = usr.usuarios
                                    inner join roles AS rol ON rol.id_rol_pk = usr.roles
                                    inner join personas AS per ON per.id_persona_pk = usu.persona
                                WHERE usu.usuario like '%$this->usuario%'";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function consultarPorUsuario(){
            if ($this->usuario != '' ){
                $sentenciaSql = "SELECT 
                                    usu.id_usuario_pk AS id_usuario
                                    ,usu.password AS password
                                FROM 
                                    usuarios AS usu
                                WHERE usuario like '%$this->usuario%';";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }


        public function __destruct() {
            unset($this->idUsuarioPk);
            unset($this->usuario);
            unset($this->password);
            unset($this->idPersonaFk);
            unset($this->fechaCreacion);
            unset($this->fechaActualizacion);
            unset($this->idUsuarioCreacion);
            unset($this->idUsuarioActualizacion);
            unset($this->conn);
        }
   }
?>
