<?php
    class MaterialesApoyo{ //Clase de MaterialesApoyo.
        //Atributos.
        private $idMaterialesApoyoPk;
        private $nombre;
        private $palabrasClave;
        private $url;
        private $idGuiaAprendizajeFk;
        private $fechaCreacion;
        private $fechaActualizacion;
        private $idUsuarioCreacion;
        private $idUsuarioActualizacion;
        public $conn=null;
        //Los métodos get y set,para mostrar(get) o modificar (set) el valor de un atributo.
        //Set y get del atributo idmaterialesApoyoPk.
        public function getIdMaterialesApoyoPk(){
            return $this->idMaterialesApoyoPk;
        }
        public function setIdMaterialesApoyoPk($idMaterialesApoyoPk){
            $this->idMaterialesApoyoPk = $idMaterialesApoyoPk;
        }
        //Set y get del atributo nombre.
        public function getNombre(){
            return $this->nombre;
        }
        public function setNombre($nombre){
            $this->nombre = $nombre;
        }    
        //Set y get del atributo idGuiaAprendizajeFk.
        public function getIdGuiasAprendizajeFk(){
            return $this->idGuiaAprendizajeFk;
        }
        public function setIdGuiasAprendizajeFk($idGuiaAprendizajeFk){
            $this->idGuiaAprendizajeFk = $idGuiaAprendizajeFk;
        }
        //Set y get del atributo palabrasClave.
        public function getPalabrasClave(){
            return $this->palabrasClave;
        }
        public function setPalabrasClave($palabrasClave){
            $this->palabrasClave= $palabrasClave;
        }
        //Set y get del atributo url.
        public function  setUrl($url){
            $this->url=$url;
        }
        public function getUrl(){
            return $this->url;
        }
        //Set y get del atributo fechaCreación.
        public function getFechaCreacion(){ 
            return $this->fechaCreacion;
        }
        public function setFechaCreacion($fechaCreacion){ 
            $this->fechaCreacion =$fechaCreacion;
        }
        //Set y get del atributo fechaActualización.
        public function getFechaActualizacion(){ 
            return $this->fechaActualizacion;
        }
        public function setFechaActualizacion($fechaActualizacion){
             $this->fechaActualizacion =$fechaActualizacion;
            }
        //Set y get del atributo idUsuarioCreación.
        public function getIdUsuarioCreacion(){ 
            return $this->idUsuarioCreacion;
        }
        public function setIdUsuarioCreacion($idUsuarioCreacion){ 
            $this->idUsuarioCreacion =$idUsuarioCreacion;
        }
        //Set y get del atributo idUsuarioActualización.
        public function getIdUsuarioActualizacion(){ 
            return $this->idUsuarioActualizacion;
        }
        public function setIdUsuarioActualizacion($idUsuarioActualizacion){ 
            $this->idUsuarioActualizacion =$idUsuarioActualizacion; 
        }
        //conexion
        public function __construct(){
            $this->conn = new Conexion(); 
        }

        public function agregar(){
            $sentenciaSql = "INSERT INTO materiales_apoyo(nombre
                                                        ,guia_aprendizaje
                                                        ,palabras_clave
                                                        ,url
                                                        ,fecha_creacion
                                                        ,fecha_actualizacion
                                                        ,id_usuario_creacion
                                                        ,id_usuario_actualizacion)
                                                    VALUES ('$this->nombre'
                                                            ,$this->idGuiaAprendizajeFk
                                                            ,'$this->palabrasClave'
                                                            ,'$this->url'
                                                            ,curdate()
                                                            ,curdate()
                                                            ,$this->idUsuarioCreacion
                                                            ,$this->idUsuarioActualizacion);";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
        }

        public function modificar(){
            $sentenciaSql = "UPDATE materiales_apoyo SET nombre='$this->nombre'
                                                        ,guia_aprendizaje=$this->idGuiaAprendizajeFk
                                                        ,palabras_clave='$this->palabrasClave'
                                                        ,url='$this->url'
                                                        ,fecha_actualizacion=curdate()
                                                        ,id_usuario_actualizacion=$this->idUsuarioActualizacion
                                                    WHERE id_materiales_apoyo_pk = $this->idMaterialesApoyoPk;";
            $this->conn->Preparar($sentenciaSql);
            $this->conn->Ejecutar();     
        }

        public function consultarParaListar(){ 
            $sentenciaSql = "SELECT 
                                ma.id_materiales_apoyo_pk
                                ,ma.nombre AS nombre_material
                                ,gui.nombre AS nombre_guia
                                ,ma.palabras_clave AS palabras_material
                                ,ma.url AS url_material
                                ,ma.guia_aprendizaje AS id_guia
                                ,ma.id_usuario_creacion
                                ,gui.id_usuario_creacion
                            FROM 
                                materiales_apoyo AS ma
                                inner join guias_aprendizaje AS gui ON gui.id_guias_aprendizaje_pk = ma.guia_aprendizaje;";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function consultar(){
            if ($this->idMaterialesApoyoPk != '' ){
                $sentenciaSql = "SELECT * FROM materiales_apoyo WHERE id_materiales_apoyo_pk = $this->idMaterialesApoyoPk;";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        /*
        public function agregar(){
            $sentenciaSql = "call agregar_material('$this->nombre','$this->palabrasClave','$this->url',$this->idGuiaAprendizajeFk,$this->idUsuarioCreacion,$this->idUsuarioActualizacion);";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
        }

        public function modificar(){
            $sentenciaSql = "call modificar_material($this->idMaterialesApoyoPk,'$this->nombre',$this->idGuiaAprendizajeFk,'$this->palabrasClave','$this->url',$this->idUsuarioActualizacion);";
            $this->conn->Preparar($sentenciaSql);
            $this->conn->Ejecutar();     
        }

        public function eliminar(){
            $sentenciaSql = "call agregar_material($this->idGuiaPk);";        
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
        }

        public function consultarParaListar(){ 
            $sentenciaSql = "call listar_material();";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function consultar(){
            if ($this->idMaterialesApoyoPk != '' ){
                $sentenciaSql = "call consultar_material($this->idMaterialesApoyoPk);";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }
        */

        public function __destruct() {
            unset($this->idMaterialesApoyoPk);
            unset($this->nombre);
            unset($this->palabrasClave);
            unset($this->url);
            unset($this->idGuiaAprendizajeFk);
            unset($this->fechaCreacion);
            unset($this->fechaActualizacion);
            unset($this->idUsuarioCreacion);
            unset($this->idUsuarioActualizacion);
            unset($this->conn);
        }
    }
?>