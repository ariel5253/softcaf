<?php
    class ProgramasFormacion{ //Clase de ProgramasFormación.
        //Atributos.
        private $idProgramaFormacionPk;
        private $codigo;
        private $nombre;
        private $idRedesConocimientoFk;
        private $fechaCreacion;
        private $fechaModificacion;
        private $idUsuarioCreacion;
        private $idUsuarioModificacion;
        public $conn=null;
        //Los métodos get y set, para mostrar (get) o modificar (set) el valor de un atributo.
       //Set y get del atributo idProgramaFormacionPk.
        public function getIdProgramaFormacionPk(){
            return $this->idProgramaFormacionPk;
        }
        public function setIdProgramaFormacionPk($idProgramaFormacionPk){
            $this->idProgramaFormacionPk = $idProgramaFormacionPk;
        }
        //Set y get del atributo codigo.
        public function getCodigo(){
            return $this->codigo;
        }
        public function setCodigo($codigo){
            $this->codigo = $codigo;
        }
        //Set y get del atributo nombre.
        public function getNombre(){
            return $this->nombre;
        }
        public function setNombre($nombre){
            $this->nombre = $nombre;
        }
        //Set y get del atributo idRedesConocimientoFk.
        public function getIdRedesConocimientoFk(){
            return $this->idRedesConocimientoFk;
        }
        public function setIdRedesConocimientoFk($idRedesConocimientoFk){
            $this->idRedesConocimientoFk = $idRedesConocimientoFk;
        }
        //Set y get del atributo fechaCreación.
        public function getFechaCreacion(){ 
            return $this->fechaCreacion;
        }
        public function setFechaCreacion($fechaCreacion){ 
            $this->fechaCreacion =$fechaCreacion;
        }
        //Set y get del atributo fechaActualización.
        public function getFechaActualizacion(){ 
            return $this->fechaActualizacion;
        }
        public function setFechaActualizacion($fechaActualizacion){
             $this->fechaActualizacion =$fechaActualizacion;
            }
        //Set y get del atributo idUsuarioCreación.
        public function getIdUsuarioCreacion(){ 
            return $this->idUsuarioCreacion;
        }
        public function setIdUsuarioCreacion($idUsuarioCreacion){ 
            $this->idUsuarioCreacion =$idUsuarioCreacion;
        }
        //Set y get del atributo idUsuarioActualización.
        public function getIdUsuarioActualizacion(){ 
            return $this->idUsuarioActualizacion;
        }
        public function setIdUsuarioActualizacion($idUsuarioCreacion){ 
            $this->idUsuarioActualizacion =$idUsuarioActualizacion;
        }
    }
?>
