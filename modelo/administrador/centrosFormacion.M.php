<?php
    class CentrosFormacion //Clase de CentrosFormación.
    {
        //Atributos.
        private $idCentrosFormacionPk;
        private $nombre;
        private $idMunicipioFk;
        private $fechaCreacion;
        private $fechaActualizacion;
        private $idUsuarioCreacion;
        private $idUsuarioActualizacion;
        public $conn=null;
        //Los métodos get y set,para mostrar (get) o modificar (set) el valor de un atributo.
       //Set y get del atributo idCentrosFormaciónPk.
        public function getIdCentrosFormacionPk(){
            return $this->idCentrosFormacionPk;
        }
        public function setIdCentrosFormacionPk($idCentrosFormacionPk){
            $this->idCentrosFormacionPk=$idCentrosFormacionPk;
        }
        //Set y get del atributo nombre.
        public function setNombre($nombre){
            $this->nombre=$nombre;
        }
        public function getNombre(){
            return $this->nombre;
        }
        //Set y get del atributo IdMunicipioFk.
        public function setIdMunicipioFk($idMunicipioFk){
            $this->idMunicipioFk=$idMunicipioFk;
        }
        public function getIdMunicipioFk(){
            return $this ->idMunicipioFk;
        }
        //Set y get del atributo fechaCreación.
        public function getFechaCreacion(){ 
            return $this->fechaCreacion;
        }
        public function setFechaCreacion($fechaCreacion){ 
            $this->fechaCreacion =$fechaCreacion;
        }
        //Set y get del atributo fechaActualización.
        public function getFechaActualizacion(){ 
            return $this->fechaActualizacion;
        }
        public function setFechaActualizacion($fechaActualizacion){
             $this->fechaActualizacion =$fechaActualizacion;
            }
        //Set y get del atributo idUsuarioCreación.
        public function getIdUsuarioCreacion(){ 
            return $this->idUsuarioCreacion;
        }
        public function setIdUsuarioCreacion($idUsuarioCreacion){ 
            $this->idUsuarioCreacion = $idUsuarioCreacion;
        }
        //Set y get del atributo idUsuarioActualización.
        public function getIdUsuarioActualizacion(){ 
            return $this->idUsuarioActualizacion;
        }
        public function setIdUsuarioActualizacion($idUsuarioActualizacion){ 
            $this->idUsuarioActualizacion = $idUsuarioActualizacion;
        }

        //conexion
        public function __construct(){
            $this->conn = new Conexion(); 
        }

        public function agregar(){
            $sentenciaSql = "INSERT INTO centros_formacion(nombre
                                ,municipio
                                ,fecha_creacion
                                ,fecha_actualizacion
                                ,id_usuario_creacion
                                ,id_usuario_actualizacion)
                            VALUES ('$this->nombre'
                                ,$this->idMunicipioFk
                                ,curdate()
                                ,curdate()
                                ,$this->idUsuarioCreacion
                                ,$this->idUsuarioActualizacion);";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
        } 

        public function modificar(){
            $sentenciaSql = "UPDATE centros_formacion SET nombre = '$this->nombre'
                                ,municipio = $this->idMunicipioFk
                                ,fecha_actualizacion = curdate()
                                ,id_usuario_actualizacion = $this->idUsuarioActualizacion
                            WHERE id_centros_formacion_pk = $this->idCentrosFormacionPk;";
            $this->conn->Preparar($sentenciaSql);
            $this->conn->Ejecutar();     
        }

        public function consultarPorId(){
            if ($this->idCentrosFormacionPk != '' ){
                $sentenciaSql = "SELECT 
                                    cen.id_centros_formacion_pk AS id_centro
                                    ,cen.nombre AS nombre_centro
                                    ,cen.municipio AS id_municipio
                                    ,mun.nombre AS nombre_municipio
                                FROM 
                                    centros_formacion AS cen
                                    INNER JOIN municipios AS mun ON mun.id_municipio_pk = cen.municipio
                                WHERE id_centros_formacion_pk = $this->idCentrosFormacionPk;";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function consultarPorNombre(){
            if ($this->nombre != '') {
                $sentenciaSql = "SELECT * FROM centros_formacion WHERE nombre like '%$this->nombre%';";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function listarCentros(){
            $sentenciaSql = "SELECT 
                                cen.id_centros_formacion_pk AS id_centro
                                ,cen.nombre AS nombre_centro
                                ,mun.nombre AS nombre_municipio
                            from 
                                centros_formacion AS cen
                                INNER JOIN municipios AS mun ON mun.id_municipio_pk = cen.municipio;";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }
        
        public function __destruct() {
            unset($this->idCentrosFormacionPk);
            unset($this->nombre);
            unset($this->idMunicipioFk);
            unset($this->fechaCreacion);
            unset($this->fechaActualizacion);
            unset($this->idUsuarioCreacion);
            unset($this->idUsuarioActualizacion);
            unset($this->conn);
        }
        
    }        
?>