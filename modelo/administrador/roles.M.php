<?php
    class Roles{ //Clase de Roles.
        //Atributos.
        private $idRolPk;
        private $nombre;
        private $fechaCreacion;
        private $fechaActualizacion;
        private $idUsuarioCreacion;
        private $idUsuarioActualizacion;
        public $conn=null;
        //Los métodos get y set,(get) o modificar (set) el valor de un atributo.
       //Set y get del atributo idRol.
        public function getIdRolPk(){
            return $this->idRolPk;
        }
        public function setIdRolPk($idRolPk){
            $this->idRol = $idRolPk;
        }
        //Set y get del atributo nombre.
        public function getNombre(){
            return $this->nombre;
        }
        public function setNombre($nombre){
            $this->nombre = $nombre;
        }
        //Set y get del atributo fechaCreación.
        public function getFechaCreacion(){ 
            return $this->fechaCreacion;
        }
        public function setFechaCreacion($fechaCreacion){ 
            $this->fechaCreacion =$fechaCreacion;
        }
        //Set y get del atributo fechaActualización.
        public function getFechaActualizacion(){ 
            return $this->fechaActualizacion;
        }
        public function setFechaActualizacion($fechaActualizacion){
             $this->fechaActualizacion =$fechaActualizacion;
            }
        //Set y get del atributo idUsuarioCreación.
        public function getIdUsuarioCreacion(){ 
            return $this->idUsuarioCreacion;
        }
        public function setIdUsuarioCreacion($idUsuarioCreacion){ 
            $this->idUsuarioCreacion =$idUsuarioCreacion;
        }
        //Set y get del atributo idUsuarioActualización.
        public function getIdUsuarioActualizacion(){ 
            return $this->idUsuarioActualizacion;
        }
        public function setIdUsuarioActualizacion($idUsuarioActualizacion){ 
            $this->idUsuarioActualizacion =$idUsuarioActualizacion;
        }

        //conexion
        public function __construct(){
            $this->conn = new Conexion(); 
        }

        public function agregar(){
            $sentenciaSql = "INSERT INTO roles(nombre
                                ,fecha_creacion
                                ,fecha_actualizacion
                                ,id_usuario_creacion
                                ,id_usuario_actualizacion)
                            VALUES ('$this->nombre'
                                ,$this->idrolPk
                                ,curdate()
                                ,curdate()
                                ,$this->idUsuarioCreacion
                                ,$this->idUsuarioActualizacion);";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
        } 
        public function modificar(){
            $sentenciaSql = "UPDATE roles SET nombre='$this->nombre'
                                ,fecha_actualizacion=curdate()
                                ,id_usuario_actualizacion=$this->idUsuarioActualizacion
                            WHERE id_rol_pk = $this->idRolPk;";
            $this->conn->Preparar($sentenciaSql);
            $this->conn->Ejecutar();     
        }
        public function consultar(){
            if ($this->idRolPk != '' ){
                $sentenciaSql = "SELECT * FROM roles WHERE id_rol_pk = $this->idRolPk;";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function listarRol(){
            $sentenciaSql = "SELECT 
                                rol.id_rol_pk
                                ,rol.nombre
                            FROM 
                                roles AS rol;";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }
        
        public function __destruct() {
            unset($this->idRolPk);
            unset($this->nombre);
            unset($this->fechaCreacion);
            unset($this->fechaActualizacion);
            unset($this->idUsuarioCreacion);
            unset($this->idUsuarioActualizacion);
            unset($this->conn);
        }
    }
?>