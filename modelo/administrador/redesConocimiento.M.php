<?php
    class RedesConocimiento{ //Clase de RedesConocimiento.
        //Atributos.
        private $idRedesConocimientoPk;
        private $nombre;
        private $fechaCreacion;
        private $fechaActualizacion;
        private $idUsuarioCreacion;
        private $idUsuarioActualizacion;
        public  $conn=null;
        //Los métodos get y set,para mostrar(get) o modificar (set) el valor de un atributo.
       //Set y get del atributo idRedesConocimiento.
        public function setIdRedesConocimientoPk($idRedesConocimientoPk){
            $this->idRedesConocimientoPk=$idRedesConocimientoPk;
        }
        public function getIdRedesConocimientoPk(){
            return $this->idRedesConocimientoPk;
        }
       //Set y get del atributo nombre.
        public function getNombre(){
            return $this->nombre;
        }
        public function setNombre($nombre){
            $this->nombre = $nombre;
        }   
        //Set y get del atributo fechaCreación.
        public function getFechaCreacion(){ 
            return $this->fechaCreacion;
        }
        public function setFechaCreacion($fechaCreacion){ 
            $this->fechaCreacion =$fechaCreacion;
        }
        //Set y get del atributo fechaActualización.
        public function getFechaActualizacion(){ 
            return $this->fechaActualizacion;
        }
        public function setFechaActualizacion($fechaActualizacion){
             $this->fechaActualizacion =$fechaActualizacion;
            }
        //Set y get del atributo idUsuarioCreación.
        public function getIdUsuarioCreacion(){ 
            return $this->idUsuarioCreacion;
        }
        public function setIdUsuarioCreacion($idUsuarioCreacion){ 
            $this->idUsuarioCreacion =$idUsuarioCreacion;
        }
        //Set y get del atributo idUsuarioActualización.
        public function getIdUsuarioActualizacion(){ 
            return $this->idUsuarioActualizacion;
        }
        public function setIdUsuarioActualizacion($idUsuarioActualizacion){ 
            $this->idUsuarioActualizacion =$idUsuarioActualizacion;
        }

        //conexion
        public function __construct(){
            $this->conn = new Conexion(); 
        }

        public function agregar(){
            $sentenciaSql = "INSERT INTO redes_conocimiento(nombre 
                                ,fecha_creacion
                                ,fecha_actualizacion
                                ,id_usuario_creacion
                                ,id_usuario_actualizacion)
                            VALUES ('$this->nombre'
                                ,curdate()
                                ,curdate()
                                ,$this->idUsuarioCreacion
                                ,$this->idUsuarioActualizacion);";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
        } 

        public function modificar(){
            $sentenciaSql = "UPDATE redes_conocimiento SET nombre = '$this->nombre'
                                ,fecha_actualizacion = curdate()
                                ,id_usuario_actualizacion = $this->idUsuarioActualizacion
                            WHERE id_redes_conocimiento_pk = $this->idRedesConocimientoPk;";
            $this->conn->Preparar($sentenciaSql);
            $this->conn->Ejecutar();     
        }

        public function consultarPorId(){
            if ($this->idRedesConocimientoPk != '' ){
                $sentenciaSql = "SELECT 
                                    red.id_redes_conocimiento_pk AS id_red
                                    ,red.nombre AS nombre_red
                                FROM 
                                    redes_conocimiento AS red
                                WHERE id_redes_conocimiento_pk = $this->idRedesConocimientoPk;";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function consultarPorNombre(){
            if ($this->nombre != '' ){
                $sentenciaSql = "SELECT * FROM redes_conocimiento WHERE nombre like '%$this->nombre%';";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function listarRed(){
            $sentenciaSql = "SELECT 
                                    red.id_redes_conocimiento_pk AS id_red
                                    ,red.nombre AS nombre_red
                                FROM 
                                    redes_conocimiento AS red;";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }
        
        public function __destruct() {
            unset($this->idRedesConocimientoPk);
            unset($this->nombre);
            unset($this->fechaCreacion);
            unset($this->fechaActualizacion);
            unset($this->idUsuarioCreacion);
            unset($this->idUsuarioActualizacion);
            unset($this->conn);
        }
    }
?>
    
      
   

