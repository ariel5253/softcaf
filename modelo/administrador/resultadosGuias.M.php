<?php
    class ResultadosGuias{ //Clase de RedesConocimiento.
        //Atributos.
        private $idResultadosGuiasPk;
        private $idResultadosAprendizajeFk ;
        private $idGuiasAprendizajeFk ;
        private $fechaCreacion;
        private $fechaActualizacion;
        private $idUsuarioCreacion;
        private $idUsuarioActualizacion;
        public  $conn=null;
        //Los métodos get y set,para mostrar(get) o modificar (set) el valor de un atributo.
       //Set y get del atributo idRedesConocimiento.
        public function setIdResultadosGuiasPk($idResultadosGuiasPk){
            $this->idResultadosGuiasPk=$idResultadosGuiasPk;
        }
        public function getIdResultadosGuiasPk(){
            return $this->idResultadosGuiasPk;
        }
       //Set y get del atributo nombre.
        public function getIdResultadosAprendizajeFk(){
            return $this->idResultadosAprendizajeFk;
        }
        public function setIdResultadosAprendizajeFk($idResultadosAprendizajeFk){
            $this->idResultadosAprendizajeFk = $idResultadosAprendizajeFk;
        }   

        //Set y get del atributo nombre.
        public function getIdGuiasAprendizajeFk(){
            return $this->idGuiasAprendizajeFk;
        }
        public function setIdGuiasAprendizajeFk($idGuiasAprendizajeFk){
            $this->idGuiasAprendizajeFk = $idGuiasAprendizajeFk;
        }   
        
        //Set y get del atributo fechaCreación.
        public function getFechaCreacion(){ 
            return $this->fechaCreacion;
        }
        public function setFechaCreacion($fechaCreacion){ 
            $this->fechaCreacion =$fechaCreacion;
        }
        //Set y get del atributo fechaActualización.
        public function getFechaActualizacion(){ 
            return $this->fechaActualizacion;
        }
        public function setFechaActualizacion($fechaActualizacion){
             $this->fechaActualizacion =$fechaActualizacion;
            }
        //Set y get del atributo idUsuarioCreación.
        public function getIdUsuarioCreacion(){ 
            return $this->idUsuarioCreacion;
        }
        public function setIdUsuarioCreacion($idUsuarioCreacion){ 
            $this->idUsuarioCreacion =$idUsuarioCreacion;
        }
        //Set y get del atributo idUsuarioActualización.
        public function getIdUsuarioActualizacion(){ 
            return $this->idUsuarioActualizacion;
        }
        public function setIdUsuarioActualizacion($idUsuarioActualizacion){ 
            $this->idUsuarioActualizacion =$idUsuarioActualizacion;
        }

        //conexion
        public function __construct(){
            $this->conn = new Conexion(); 
        }

        public function agregar(){
            $sentenciaSql = "INSERT INTO resultados_guias(id_resultados_aprendizaje_fk  
                                ,id_guias_aprendizaje_fk 
                                ,fecha_creacion
                                ,fecha_actualizacion
                                ,id_usuario_creacion
                                ,id_usuario_actualizacion)
                            VALUES ($this->idResultadosAprendizajeFk
                                ,$this->idGuiasAprendizajeFk
                                ,curdate()
                                ,curdate()
                                ,$this->idUsuarioCreacion
                                ,$this->idUsuarioActualizacion);";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
        } 

        public function modificar(){
            $sentenciaSql = "UPDATE resultados_aprendizaje SET nombre = '$this->nombre'
                                ,id_competencia_fk = $this->idComptenciaFk
                                ,fecha_actualizacion = curdate()
                                ,id_usuario_actualizacion = $this->idUsuarioActualizacion
                            WHERE id_resultados_aprendizaje_pk = $this->idResultadoAprendizajePk;";
            $this->conn->Preparar($sentenciaSql);
            $this->conn->Ejecutar();     
        }

        public function consultarPorId(){
            if ($this->idResultadoAprendizajePk != '' ){
                $sentenciaSql = "SELECT 
                                    rap.id_resultados_aprendizaje_pk as id_rap
                                    ,rap.id_competencia_fk as id_com
                                    ,rap.nombre as nombre_rap
                                    ,concat(com.codigo,' ',com.nombre) as nombre_com
                                FROM 
                                    resultados_aprendizaje AS rap
                                    inner join competencias as com on com.id_competencia_pk = rap.id_competencia_fk
                                WHERE rap.id_resultados_aprendizaje_pk = $this->idResultadoAprendizajePk;";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function consultarPorCompetencia(){
            if ($this->idComptenciaFk != '' ){
                $sentenciaSql = "SELECT 
                                    rap.*
                                FROM 
                                    resultados_aprendizaje AS rap
                                    inner join competencias as com on com.id_competencia_pk = rap.id_competencia_fk
                                WHERE rap.id_competencia_fk = $this->idComptenciaFk;";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function listar(){
            $sentenciaSql = "SELECT 
                                rap.id_resultados_aprendizaje_pk as id_rap
                                ,rap.nombre as nombre_rap
                                ,concat(com.codigo,' ',com.nombre) as nombre_com
                            FROM 
                                resultados_aprendizaje AS rap
                                inner join competencias as com on com.id_competencia_pk = rap.id_competencia_fk;";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }
        
        public function __destruct() {
            unset($this->idResultadosGuiasPk);
            unset($this->idResultadosAprendizajeFk);
            unset($this->idGuiasAprendizajeFk);
            unset($this->fechaCreacion);
            unset($this->fechaActualizacion);
            unset($this->idUsuarioCreacion);
            unset($this->idUsuarioActualizacion);
            unset($this->conn);
        }
    }
?>