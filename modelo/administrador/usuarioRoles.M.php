<?php
        class UsuarioRoles{   //Clase de UsuarioRoles.
        private $idUsuariosRolesPk;
        private $idUsuarioFk;
        private $idRolFk;
        private $estado;
        private $fechaCreacion;
        private $fechaActualizacion;
        private $idUsuarioCreacion;
        private $idUsuarioActualizacion;
        public $conn=null;
        //Los métodos get y set,(get) o modificar (set) el valor de un atributo.
       //Set y get del atributo idUsuariosRolesPk.
        public function getIdUsuariosRolesPk(){
            return $this->idUsuariosRolesPk;
        }
        public function setIdUsuariosRolesPk($idUsuariosRolesPk){
            $this->idUsuariosRolesPk = $idUsuariosRolesPk;
        }
        //Set y get del atributo idUsuarioFk.
        public function getIdUsuarioFk(){
            return $this->idUsuarioFk;
        }
        public function setIdUsuarioFk($idUsuarioFk){
            $this->idUsuarioFk = $idUsuarioFk;
        }
        //Set y get del atributo idRolFk.
        public function getIdRolFk(){
            return $this->idRolFk;
        }
        public function setIdRolFk($idRolFk){
            $this->idRolFk = $idRolFk;
        }
        //Set y get del atributo estado.
        public function getEstado(){ 
            return $this->estado;
        }
        public function setEstado($estado){ 
            $this->estado=$estado;
        }
        //Set y get del atributo fechaCreación.
        public function getFechaCreacion(){ 
            return $this->fechaCreacion;
        }
        public function setFechaCreacion($fechaCreacion){ 
            $this->fechaCreacion =$fechaCreacion;
        }
        //Set y get del atributo fechaActualización.
        public function getFechaActualizacion(){ 
            return $this->fechaActualizacion;
        }
        public function setFechaActualizacion($fechaActualizacion){
             $this->fechaActualizacion =$fechaActualizacion;
            }
        //Set y get del atributo idUsuarioCreación.
        public function getIdUsuarioCreacion(){ 
            return $this->idUsuarioCreacion;
        }
        public function setIdUsuarioCreacion($idUsuarioCreacion){ 
            $this->idUsuarioCreacion =$idUsuarioCreacion;
        }
        //Set y get del atributo idUsuarioActualización.
        public function getIdUsuarioActualizacion(){ 
            return $this->idUsuarioActualizacion;
        }
        public function setIdUsuarioActualizacion($idUsuarioActualizacion){ 
            $this->idUsuarioActualizacion =$idUsuarioActualizacion;
        }

        //conexion
        public function __construct(){
            $this->conn = new Conexion();  
        }

        public function agregar(){
            $sentenciaSql = "INSERT INTO usuario_roles(usuarios 
                                ,roles 
                                ,estado 
                                ,fecha_creacion
                                ,fecha_actualizacion
                                ,id_usuario_creacion
                                ,id_usuario_actualizacion)
                            VALUES ($this->idUsuarioFk
                                ,$this->idRolFk
                                ,'$this->estado'
                                ,curdate()
                                ,curdate()
                                ,$this->idUsuarioCreacion
                                ,$this->idUsuarioActualizacion);";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
        } 

        public function modificar(){
            $sentenciaSql = "UPDATE usuario_roles SET usuarios  = $this->idUsuarioFk
                                ,roles = $this->idRolFk
                                ,estado = '$this->estado'
                                ,fecha_actualizacion = curdate()
                                ,id_usuario_actualizacion = $this->idUsuarioActualizacion
                            WHERE id_usuario_roles_pk = $this->idUsuariosRolesPk;";
            $this->conn->Preparar($sentenciaSql);
            $this->conn->Ejecutar();     
        }

        public function consultarPorId(){
            if ($this->idUsuariosRolesPk != '' ){
                $sentenciaSql = "SELECT 
                                    usr.id_usuario_roles_pk AS id_usuario_rol
                                    ,usr.usuarios AS id_usuario
                                    ,usr.roles AS id_rol
                                    ,usr.estado AS estado
                                    ,usu.usuario AS nombre_Usuario
                                    ,concat(per.nombres,' ',per.apellido) AS nombre_persona
                                FROM 
                                    usuario_roles AS usr
                                    INNER JOIN usuarios AS usu ON usu.id_usuario_pk = usr.usuarios
                                    INNER JOIN personas AS per ON per.id_persona_pk = usu.persona
                                WHERE usr.id_usuario_roles_pk = $this->idUsuariosRolesPk;";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function listarUsuarioRol(){
            $sentenciaSql = "SELECT 
                                usr.id_usuario_roles_pk AS id_usuario_rol
                                ,concat(per.nombres,' ',per.apellido) AS nombre_persona
                                ,usu.usuario AS usuario
                                ,rol.nombre AS rol
                                ,usr.estado AS estado
                            FROM 
                                usuario_roles AS usr
                                INNER JOIN usuarios AS usu ON usu.id_usuario_pk = usr.usuarios
                                INNER JOIN roles AS rol ON rol.id_rol_pk = usr.roles
                                INNER JOIN personas AS per ON per.id_persona_pk = usu.persona;";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }
        
        public function __destruct() {
            unset($this->idUsuariosRolesPk);
            unset($this->idUsuarioFk);
            unset($this->idRolFk);
            unset($this->estado);
            unset($this->fechaCreacion);
            unset($this->fechaActualizacion);
            unset($this->idUsuarioCreacion);
            unset($this->idUsuarioActualizacion);
            unset($this->conn);
        }
   }
  
?>