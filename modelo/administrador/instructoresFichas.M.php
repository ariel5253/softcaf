<?php
    class instructoresFicha{ //Clase de Fichas.
        //Atributos.
        private $idInstructoresFichaPk;
        private $idInstructorFk;
        private $idFichaFk;
        private $estado;
        private $fechaCreacion;
        private $fechaActualizacion;
        private $idUsuarioCreacion;
        private $idUsuarioActualizacion;
        public  $conn=null;
        //Los métodos get y set,para mostrar (get) o modificar (set) el valor de un atributo.
        //Set y get del atributo idFicha.
        public function getIdInstructoresFichaPk(){
            return $this->idInstructoresFichaPk;
        }
        public function setIdInstructoresFichaPk($idInstructoresFichaPk){
            $this->idInstructoresFichaPk = $idInstructoresFichaPk;
        }
        //Set y get del atributo codigo.
        public function getIdInstructorFk(){
            return $this->idInstructorFk;
        }
        public function setIdInstructorFk($idInstructorFk){
            $this->idInstructorFk = $idInstructorFk;
        }
        //Set y get del atributo idProgramaFormacionFk.
        public function getIdFichaFk(){
            return $this->idFichaFk;
        }
        public function setIdFichaFk($idFichaFk){
            $this->idFichaFk = $idFichaFk;
        }
        //Set y get del atributo idProgramaFormacionFk.
        public function getEstado(){
            return $this->estado;
        }
        public function setEstado($estado){
            $this->estado = $estado;
        }

        //Set y get del atributo fechaCreación.
        public function getFechaCreacion(){ 
            return $this->fechaCreacion;
        }
        public function setFechaCreacion($fechaCreacion){ 
            $this->fechaCreacion =$fechaCreacion;
        }
        //Set y get del atributo fechaActualización.
        public function getFechaActualizacion(){ 
            return $this->fechaActualizacion;
        }
        public function setFechaActualizacion($fechaActualizacion){
             $this->fechaActualizacion =$fechaActualizacion;
            }
        //Set y get del atributo idUsuarioCreación.
        public function getIdUsuarioCreacion(){ 
            return $this->idUsuarioCreacion;
        }
        public function setIdUsuarioCreacion($idUsuarioCreacion){ 
            $this->idUsuarioCreacion =$idUsuarioCreacion;
        }
        //Set y get del atributo idUsuarioActualización.
        public function getIdUsuarioActualizacion(){ 
            return $this->idUsuarioActualizacion;
        }
        public function setIdUsuarioActualizacion($idUsuarioActualizacion){ 
            $this->idUsuarioActualizacion =$idUsuarioActualizacion;
        }

        //conexion
        public function __construct(){
            $this->conn = new Conexion();
        }

        public function agregar(){
            $sentenciaSql = "INSERT INTO instructores_fichas(instructor 
                                ,ficha 
                                ,estado
                                ,fecha_creacion
                                ,fecha_actualizacion
                                ,id_usuario_creacion
                                ,id_usuario_actualizacion)
                            VALUES ($this->idInstructorFk
                                ,$this->idFichaFk
                                ,'$this->estado'
                                ,curdate()
                                ,curdate()
                                ,$this->idUsuarioCreacion
                                ,$this->idUsuarioActualizacion);";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
        } 

        public function modificar(){
            if ($this->idInstructoresFichaPk != '') {
                $sentenciaSql = "UPDATE instructores_fichas SET instructor = $this->idInstructorFk
                                ,ficha = $this->idFichaFk
                                ,estado = '$this->estado'
                                ,fecha_actualizacion = curdate()
                                ,id_usuario_actualizacion = $this->idUsuarioActualizacion
                            WHERE id_instructores_fichas_pk = $this->idInstructoresFichaPk;";
            }
            $this->conn->Preparar($sentenciaSql);
            $this->conn->Ejecutar();     
        }

        public function consultarPorId(){
            if ($this->idInstructoresFichaPk != '' ){
                $sentenciaSql = "SELECT 
                                    inf.id_instructores_fichas_pk AS id_insFic
                                    ,fic.id_ficha_pk AS id_ficha
                                    ,ins.id_instructor_pk AS id_instructor
                                    ,fic.codigo AS codigo_ficha
                                    ,concat(per.nombres,' ',per.apellido) AS nombre_persona
                                    ,per.numero_documento AS documento
                                    ,red.nombre AS nombre_red
                                    ,inf.estado AS estado
                                    ,prf.nombre AS nombre_programa
                                FROM 
                                    instructores_fichas AS inf
                                    INNER JOIN fichas AS fic ON fic.id_ficha_pk = inf.ficha
                                    INNER JOIN instructores AS ins ON ins.id_instructor_pk = inf.instructor
                                    INNER JOIN personas AS per ON per.id_persona_pk = ins.persona
                                    INNER JOIN programas_formacion AS prf ON prf.id_programas_formacion_pk = fic.programa
                                    INNER JOIN redes_conocimiento AS red ON red.id_redes_conocimiento_pk = prf.red_conocimiento
                                WHERE inf.id_instructores_fichas_pk = $this->idInstructoresFichaPk;";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function listarInstructorFicha(){
            $sentenciaSql = "SELECT 
                                inf.id_instructores_fichas_pk AS id_insFic
                                ,fic.codigo AS codigo_ficha
                                ,concat(per.nombres,' ',per.apellido) AS nombre_persona
                                ,per.numero_documento AS documento
                                ,inf.estado AS estado
                            FROM 
                                instructores_fichas AS inf
                                INNER JOIN fichas AS fic ON fic.id_ficha_pk = inf.ficha
                                INNER JOIN instructores AS ins ON ins.id_instructor_pk = inf.instructor
                                INNER JOIN personas AS per ON per.id_persona_pk = ins.persona;";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function __destruct() {
            unset($this->idInstructoresFichaPk);
            unset($this->idInstructorFk);
            unset($this->idFichaFk);
            unset($this->fechaCreacion);
            unset($this->fechaActualizacion);
            unset($this->idUsuarioCreacion);
            unset($this->idUsuarioActualizacion);
            unset($this->conn);
        }
    }
?>