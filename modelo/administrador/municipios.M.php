<?php
    class Municipios{  //Clase de Municipios.
        //Atributos.
        private $idMunicipioPk;
        private $nombre;
        private $idDepartamentoFk;
        private $fechaCreacion;
        private $fechaActualizacion;
        private $idUsuarioCreacion;
        private $idUsuarioActualizacion;
        public $conn=null;
        //Los métodos get y set,para mostrar(get) o modificar (set) el valor de un atributo.
       //Set y get del atributo idMunicipioPk.
        public function getIdMunicipioPk(){  
            return $this->idMunicipioPk;
        }
        public function setIdMunicipioPk($idMunicipioPk){
            $this->idMunicipioPk=$idMunicipioPk;
        }
        //Set y get del atributo nombre.
        public function  setNombre($nombre){
            $this->nombre=$nombre;
        }
        public function getNombre(){
            return  $this->nombre;
        }
        //Set y get del atributo idDepartamentoFk.
        public function setIdDepartamentoFk($idDepartamentoFk){
            $this->idDepartamentoFk=$idDepartamentoFk;
        }
        public function  getIdDepartamentoFk(){
            return $this ->idDepartamentoFk;
        }
        //Set y get del atributo fechaCreación.
        public function getFechaCreacion(){ 
            return $this->fechaCreacion;
        }
        public function setFechaCreacion($fechaCreacion){ 
            $this->fechaCreacion =$fechaCreacion;
        }
        //Set y get del atributo fechaActualización.
        public function getFechaActualizacion(){ 
            return $this->fechaActualizacion;
        }
        public function setFechaActualizacion($fechaActualizacion){
             $this->fechaActualizacion =$fechaActualizacion;
            }
        //Set y get del atributo idUsuarioCreación.
        public function getIdUsuarioCreacion(){ 
            return $this->idUsuarioCreacion;
        }
        public function setIdUsuarioCreacion($idUsuarioCreacion){ 
            $this->idUsuarioCreacion =$idUsuarioCreacion;
        }
        //Set y get del atributo idUsuarioActualización.
        public function getIdUsuarioActualizacion(){ 
            return $this->idUsuarioActualizacion;
        }
        public function setIdUsuarioActualizacion($idUsuarioActualizacion){ 
            $this->idUsuarioActualizacion =$idUsuarioActualizacion;
        }

        //conexion
        public function __construct(){
            $this->conn = new Conexion(); 
        }

        public function agregar(){
            $sentenciaSql = "INSERT INTO municipios(nombre
                                ,departamentos
                                ,fecha_creacion
                                ,fecha_actualizacion
                                ,id_usuario_creacion
                                ,id_usuario_actualizacion)
                            VALUES ('$this->nombre'
                                ,$this->idDepartamentoFk
                                ,curdate()
                                ,curdate()
                                ,$this->idUsuarioCreacion
                                ,$this->idUsuarioActualizacion);";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
        } 
        
        public function modificar(){
            $sentenciaSql = "UPDATE municipios SET nombre='$this->nombre'
                                ,departamentos=$this->idDepartamentoFk
                                ,fecha_actualizacion=curdate()
                                ,id_usuario_actualizacion=$this->idUsuarioActualizacion
                            WHERE id_municipio_pk = $this->idMunicipioPk;";
            $this->conn->Preparar($sentenciaSql);
            $this->conn->Ejecutar();     
        }

        public function consultar(){
            if ($this->idMunicipioPk != '' ){
                $sentenciaSql = "SELECT * FROM municipios WHERE id_municipio_pk = $this->idMunicipioPk;";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function consultarPorNombre(){
            if ($this->nombre != '') {
                $sentenciaSql = "SELECT * FROM municipios WHERE nombre like '%$this->nombre%';";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }
        
        public function __destruct() {
            unset($this->idMunicipioPk);
            unset($this->nombre);
            unset($this->idDepartamentoFk);
            unset($this->fechaCreacion);
            unset($this->fechaActualizacion);
            unset($this->idUsuarioCreacion);
            unset($this->idUsuarioActualizacion);
            unset($this->conn);
        }
    }
?>