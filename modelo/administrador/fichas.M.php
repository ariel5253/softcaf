<?php
    class Fichas{ //Clase de Fichas.
        //Atributos.
        private $idFichaPk;
        private $codigo;
        private $idProgramaFormacionFk;
        private $idCentroFormacionFk;
        private $fechaCreacion;
        private $fechaActualizacion;
        private $idUsuarioCreacion;
        private $idUsuarioActualizacion;
        public  $conn=null;
        //Los métodos get y set,para mostrar (get) o modificar (set) el valor de un atributo.
        //Set y get del atributo idFicha.
        public function getIdFichaPk(){
            return $this->idFichaPk;
        }
        public function setIdFichaPk($idFichaPk){
            $this->idFichaPk = $idFichaPk;
        }
        //Set y get del atributo codigo.
        public function getCodigo(){
            return $this->codigo;
        }
        public function setCodigo($codigo){
            $this->codigo = $codigo;
        }
        //Set y get del atributo idProgramaFormacionFk.
        public function getIdProgramaFormacionFk(){
            return $this->idProgramaFormacionFk;
        }
        public function setIdProgramaFormacionFk($idProgramaFormacionFk){
            $this->idProgramaFormacionFk = $idProgramaFormacionFk;
        }    
        //Set y get del atributo idCentroFormacionFk.
        public function getIdCentroFormacionFk(){
            return $this->idCentroFormacionFk;
        }
        public function setIdCentroFormacionFk($idCentroFormacionFk){
            $this->idCentroFormacionFk = $idCentroFormacionFk;
        } 
        //Set y get del atributo fechaCreación.
        public function getFechaCreacion(){ 
            return $this->fechaCreacion;
        }
        public function setFechaCreacion($fechaCreacion){ 
            $this->fechaCreacion =$fechaCreacion;
        }
        //Set y get del atributo fechaActualización.
        public function getFechaActualizacion(){ 
            return $this->fechaActualizacion;
        }
        public function setFechaActualizacion($fechaActualizacion){
             $this->fechaActualizacion =$fechaActualizacion;
            }
        //Set y get del atributo idUsuarioCreación.
        public function getIdUsuarioCreacion(){ 
            return $this->idUsuarioCreacion;
        }
        public function setIdUsuarioCreacion($idUsuarioCreacion){ 
            $this->idUsuarioCreacion =$idUsuarioCreacion;
        }
        //Set y get del atributo idUsuarioActualización.
        public function getIdUsuarioActualizacion(){ 
            return $this->idUsuarioActualizacion;
        }
        public function setIdUsuarioActualizacion($idUsuarioActualizacion){ 
            $this->idUsuarioActualizacion =$idUsuarioActualizacion;
        }

        //conexion
        public function __construct(){
            $this->conn = new Conexion();
        }

        public function agregar(){
            $sentenciaSql = "INSERT INTO fichas(codigo
                                ,programa
                                ,centro_formacion
                                ,fecha_creacion
                                ,fecha_actualizacion
                                ,id_usuario_creacion
                                ,id_usuario_actualizacion)
                            VALUES ($this->codigo
                                ,$this->idProgramaFormacionFk
                                ,$this->idCentroFormacionFk
                                ,curdate()
                                ,curdate()
                                ,$this->idUsuarioCreacion
                                ,$this->idUsuarioActualizacion);";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
        } 

        public function modificar(){
            if ($this->idFichaPk != '') {
                $sentenciaSql = "UPDATE fichas SET codigo = $this->codigo
                                ,programa = $this->idProgramaFormacionFk
                                ,centro_formacion = $this->idCentroFormacionFk
                                ,fecha_actualizacion = curdate()
                                ,id_usuario_actualizacion = $this->idUsuarioActualizacion
                            WHERE id_ficha_pk = $this->idFichaPk;";
            }
            $this->conn->Preparar($sentenciaSql);
            $this->conn->Ejecutar();     
        }

        public function consultarPorId(){
            if ($this->idFichaPk != '' ){
                $sentenciaSql = "SELECT 
                                    fic.id_ficha_pk AS id_ficha
                                    ,fic.codigo AS numero_ficha
                                    ,prf.nombre AS nombre_programa
                                    ,fic.programa AS id_programa
                                    ,cen.nombre AS nombre_centro
                                    ,fic.centro_formacion AS id_centro
                                FROM 
                                    fichas AS fic
                                    INNER JOIN programas_formacion AS prf ON prf.id_programas_formacion_pk = fic.programa
                                    INNER JOIN centros_formacion AS cen ON cen.id_centros_formacion_pk = fic.centro_formacion
                                WHERE id_ficha_pk = $this->idFichaPk;";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function listarFichas(){
            $sentenciaSql = "SELECT 
                                fic.id_ficha_pk AS id_ficha
                                ,fic.codigo AS numero_ficha
                                ,prf.nombre AS nombre_programa
                                ,cen.nombre AS nombre_centro
                            from 
                                fichas AS fic
                                INNER JOIN programas_formacion AS prf ON prf.id_programas_formacion_pk = fic.programa
                                INNER JOIN centros_formacion AS cen ON cen.id_centros_formacion_pk = fic.centro_formacion;";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function consultarPorCodigo(){
            if ($this->codigo != '' ){
                $sentenciaSql = "SELECT 
                                    fic.id_ficha_pk AS id_ficha
                                    ,fic.codigo AS numero_ficha
                                    ,red.nombre AS nombre_red
                                    ,prf.nombre AS nombre_programa
                                FROM 
                                    fichas AS fic
                                    INNER JOIN programas_formacion AS prf ON prf.id_programas_formacion_pk = fic.programa
                                    INNER JOIN redes_conocimiento AS red ON red.id_redes_conocimiento_pk = prf.red_conocimiento
                                WHERE fic.codigo = $this->codigo;";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        /*
        public function consultarParaListar(){
            $sentenciaSql = "call listar_ficha();";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }
        */

        public function __destruct() {
            unset($this->idFichaPk);
            unset($this->codigo);
            unset($this->idProgramaFormacionFk);
            unset($this->idCentroFormacionFk);
            unset($this->fechaCreacion);
            unset($this->fechaActualizacion);
            unset($this->idUsuarioCreacion);
            unset($this->idUsuarioActualizacion);
            unset($this->conn);
        }
    }
?>