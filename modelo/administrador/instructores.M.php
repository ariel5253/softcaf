<?php
    class Instructores{ //Clase de Instructores.
        //Atributos.
        private $idInstructorPk;
        private $idPersonaFk;
        private $idRedConocimientoFk;
        private $fechaCreacion;
        private $fechaActualizacion;
        private $idUsuarioCreacion;
        private $idUsuarioActualizacion;
        public $conn=null;
        //Los métodos get y set,para mostrar(get) o modificar (set) el valor de un atributo.
        //Set y get del atributo idInstructorPk.
        public function getIdInstructorPk(){
            return $this->idInstructorPk;
        }
        public function setIdInstructorPk($idInstructorPk){
            $this->idInstructorPk = $idInstructorPk;
        }
        //Set y get del atributo idPersonaFk.
        public function getIdPersonaFk(){
            return $this->idPersonaFk;
        }
        public function setIdPersonaFk($idPersonaFk){
            $this->idPersonaFk = $idPersonaFk;
        }    
        //Set y get del atributo idRedConocimientoFk.
        public function getIdRedConocimientoFk(){
            return $this->idRedConocimientoFk;
        }
        public function setIdRedConocimientoFk($idRedConocimientoFk){
            $this->idRedConocimientoFk = $idRedConocimientoFk;
        }
        //Set y get del atributo fechaCreación.
        public function getFechaCreacion(){ 
            return $this->fechaCreacion;
        }
        public function setFechaCreacion($fechaCreacion){ 
            $this->fechaCreacion =$fechaCreacion;
        }
        //Set y get del atributo fechaActualización.
        public function getFechaActualizacion(){ 
            return $this->fechaActualizacion;
        }
        public function setFechaActualizacion($fechaActualizacion){
             $this->fechaActualizacion =$fechaActualizacion;
            }
        //Set y get del atributo idUsuarioCreación.
        public function getIdUsuarioCreacion(){ 
            return $this->idUsuarioCreacion;
        }
        public function setIdUsuarioCreacion($idUsuarioCreacion){ 
            $this->idUsuarioCreacion =$idUsuarioCreacion;
        }
        //Set y get del atributo idUsuarioActualización.
        public function getIdUsuarioActualizacion(){ 
            return $this->idUsuarioActualizacion;
        }
        public function setIdUsuarioActualizacion($idUsuarioActualizacion){ 
            $this->idUsuarioActualizacion =$idUsuarioActualizacion;
        }

        //conexion
        public function __construct(){
            $this->conn = new Conexion(); 
        }

        public function agregar(){
            $sentenciaSql = "INSERT INTO instructores(persona
                                ,red_conocimiento
                                ,fecha_creacion
                                ,fecha_actualizacion
                                ,id_usuario_creacion
                                ,id_usuario_actualizacion)
                            VALUES ($this->idPersonaFk
                                ,$this->idRedConocimientoFk
                                ,curdate()
                                ,curdate()
                                ,$this->idUsuarioCreacion
                                ,$this->idUsuarioActualizacion);";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
        } 
        public function modificar(){
            $sentenciaSql = "UPDATE instructores SET persona = $this->idPersonaFk
                                ,red_conocimiento = $this->idRedConocimientoFk
                                ,fecha_actualizacion = curdate()
                                ,id_usuario_actualizacion = $this->idUsuarioActualizacion
                            WHERE id_instructor_pk = $this->idInstructorPk;";
            $this->conn->Preparar($sentenciaSql);
            $this->conn->Ejecutar();     
        }
        public function consultar(){
            if ($this->idInstructorPk != '' ){
                $sentenciaSql = "SELECT * FROM instructores WHERE id_instructor_pk = $this->idInstructorPk;";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function consultarPorIdPersona(){
            if ($this->idPersonaFk != '' ){
                $sentenciaSql = "SELECT 
                                    ins.id_instructor_pk AS id_instructor
                                FROM 
                                    personas AS per
                                    INNER JOIN instructores AS ins ON ins.persona = per.id_persona_pk
                                WHERE per.id_persona_pk = $this->idPersonaFk";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }
        
        public function __destruct() {
            unset($this->idInstructorPk);
            unset($this->idPersonaFk);
            unset($this->idRedConocimientoFk);
            unset($this->fechaCreacion);
            unset($this->fechaActualizacion);
            unset($this->idUsuarioCreacion);
            unset($this->idUsuarioActualizacion);
            unset($this->conn);
        }
    }
?>