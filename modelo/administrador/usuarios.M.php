<?php
    class Usuarios{  //Clase de Usuarios.
        //Atributos.
        private $idUsuarioPk;
        private $usuario;
        private $password;
        private $idPersonaFk;
        private $fechaCreacion;
        private $fechaActualizacion;
        private $idUsuarioCreacion;
        private $idUsuarioActualizacion;
        public  $conn=null;
        //Los métodos get y set,(get) o modificar (set) el valor de un atributo.
       //Set y get del atributo idUsuarioPk.
        public function getIdUsuarioPk(){
            return $this->idUsuarioPk;
        }
        public function setIdUsuarioPk($idUsuarioPk){
            $this->idUsuarioPk = $idUsuarioPk;
        }
        //Set y get del atributo Usuario.
        public function getUsuario(){
            return $this->usuario;
        }
        public function setUsuario($usuario){
            $this->usuario = $usuario;
        }  
        //Set y get del atributo password.
        public function getPassword(){
            return $this->password;
        }
        public function setPassword($password){
            $this->password = $password;
        }
        //Set y get del atributo idPersonaFk.
        public function getIdPersonaFk(){
            return $this->idPersonaFk;
        }
        public function setIdPersonaFk($idPersonaFk){
            $this->idPersonaFk = $idPersonaFk;
        }  
        //Set y get del atributo fechaCreación.
        public function getFechaCreacion(){ 
            return $this->fechaCreacion;
        }
        public function setFechaCreacion($fechaCreacion){ 
            $this->fechaCreacion =$fechaCreacion;
        }
        //Set y get del atributo fechaActualización.
        public function getFechaActualizacion(){ 
            return $this->fechaActualizacion;
        }
        public function setFechaActualizacion($fechaActualizacion){
             $this->fechaActualizacion =$fechaActualizacion;
            }
        //Set y get del atributo idUsuarioCreación.
        public function getIdUsuarioCreacion(){ 
            return $this->idUsuarioCreacion;
        }
        public function setIdUsuarioCreacion($idUsuarioCreacion){ 
            $this->idUsuarioCreacion =$idUsuarioCreacion;
        }
        //Set y get del atributo idUsuarioActualización.
        public function getIdUsuarioActualizacion(){ 
            return $this->idUsuarioActualizacion;
        }
        public function setIdUsuarioActualizacion($idUsuarioActualizacion){ 
            $this->idUsuarioActualizacion =$idUsuarioActualizacion;
        }

        //conexion
        public function __construct(){
            $this->conn = new Conexion(); 
        }

        public function agregar(){
            $sentenciaSql = "INSERT INTO usuarios(usuario 
                                ,password
                                ,persona
                                ,fecha_creacion
                                ,fecha_actualizacion
                                ,id_usuario_creacion
                                ,id_usuario_actualizacion)
                            VALUES ('$this->usuario'
                                ,'$this->password'
                                ,$this->idPersonaFk
                                ,curdate()
                                ,curdate()
                                ,$this->idUsuarioCreacion
                                ,$this->idUsuarioActualizacion);";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
        } 
        public function modificar(){
            $sentenciaSql = "UPDATE usuarios SET usuario = '$this->usuario'
                                ,password = '$this->password'
                                ,persona = $this->idPersonaFk
                                ,fecha_actualizacion = curdate()
                                ,id_usuario_actualizacion = $this->idUsuarioActualizacion
                            WHERE id_usuario_pk = $this->idUsuarioPk;";
            $this->conn->Preparar($sentenciaSql);
            $this->conn->Ejecutar();     
        }

        public function consultar(){
            if ($this->idUsuarioPk != '' ){
                $sentenciaSql = "SELECT * FROM usuarios WHERE id_usuario_pk = $this->idUsuarioPk;";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function consultarPorUsuario(){
            if ($this->usuario != '' ){
                $sentenciaSql = "SELECT 
                                    usu.id_usuario_pk AS id_usuario
                                    ,usu.usuario AS usuario
                                    ,usu.password AS password
                                    ,usu.persona AS id_persona
                                    ,concat(per.nombres,' ',per.apellido) AS nombre_persona
                                FROM 
                                    usuarios AS usu
                                    INNER JOIN personas AS per ON per.id_persona_pk = usu.persona
                                WHERE usuario like '%$this->usuario%';";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function consultarUltimoId(){
            $sentenciaSql = "SELECT
                                max(usu.id_usuario_pk) AS id_usuario
                            FROM
                                usuarios AS usu;";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }
        
        
        public function __destruct() {
            unset($this->idUsuarioPk);
            unset($this->usuario);
            unset($this->password);
            unset($this->idPersonaFk);
            unset($this->fechaCreacion);
            unset($this->fechaActualizacion);
            unset($this->idUsuarioCreacion);
            unset($this->idUsuarioActualizacion);
            unset($this->conn);
        }
   }
?>
