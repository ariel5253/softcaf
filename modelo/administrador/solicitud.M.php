<?php
    class Solicitud{ //Clase de Solicitudes.
        //Atributos.
        private $idSolicitudPk;
        private $instructorFk;
        private $fichaFk;
        private $descripcion;
        private $estado;
        private $fechaCreacion;
        private $fechaActualizacion;
        private $idUsuarioCreacion;
        private $idUsuarioActualizacion;
        public $conn=null;
        //Los métodos get y set, para mostrar (get) o modificar (set) el valor de un atributo.
       //Set y get del atributo idSolicitudPk.
        public function getIdSolicitudPk(){
            return $this->idSolicitudPk;
        }
        public function setIdSolicitudPk($idSolicitudPk){
            $this->idSolicitudPk = $idSolicitudPk;
        }
        //Set y get del atributo instructorFk.
        public function getInstructorFk(){
            return $this->instructorFk;
        }
        public function setInstructorFk($instructorFk){
            $this->instructorFk = $instructorFk;
        }
        //Set y get del atributo fichaFk.
        public function getFichaFk(){
            return $this->fichaFk;
        }
        public function setFichaFk($fichaFk){
            $this->fichaFk = $fichaFk;
        }
        //Set y get del atributo descripcion.
        public function getDescripcion(){
            return $this->descripcion;
        }
        public function setDescripcion($descripcion){
            $this->descripcion = $descripcion;
        }
        //Set y get del atributo estado.
        public function getEstado(){
            return $this->estado;
        }
        public function setEstado($estado){
            $this->estado = $estado;
        }
        //Set y get del atributo fechaCreación.
        public function getFechaCreacion(){ 
            return $this->fechaCreacion;
        }
        public function setFechaCreacion($fechaCreacion){ 
            $this->fechaCreacion =$fechaCreacion;
        }
        //Set y get del atributo fechaActualización.
        public function getFechaActualizacion(){ 
            return $this->fechaActualizacion;
        }
        public function setFechaActualizacion($fechaActualizacion){
             $this->fechaActualizacion =$fechaActualizacion;
            }
        //Set y get del atributo idUsuarioCreación.
        public function getIdUsuarioCreacion(){ 
            return $this->idUsuarioCreacion;
        }
        public function setIdUsuarioCreacion($idUsuarioCreacion){ 
            $this->idUsuarioCreacion =$idUsuarioCreacion;
        }
        //Set y get del atributo idUsuarioActualización.
        public function getIdUsuarioActualizacion(){ 
            return $this->idUsuarioActualizacion;
        }
        public function setIdUsuarioActualizacion($idUsuarioActualizacion){ 
            $this->idUsuarioActualizacion =$idUsuarioActualizacion;
        }

        //conexion
        public function __construct(){
            $this->conn = new Conexion();  
        }
        public function agregar(){
            $sentenciaSql = "INSERT INTO solicitudes(instructor
                                ,ficha
                                ,asunto
                                ,estado
                                ,fecha_creacion
                                ,fecha_actualizacion
                                ,id_usuario_creacion
                                ,id_usuario_actualizacion)
                            VALUES ($this->instructorFk
                                ,$this->fichaFk
                                ,'$this->descripcion'    
                                ,'$this->estado'
                                ,curdate()
                                ,curdate()
                                ,$this->idUsuarioCreacion
                                ,$this->idUsuarioActualizacion);";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
        } 
        public function modificar(){
            $sentenciaSql = "UPDATE solicitudes SET estado='$this->estado'
                                ,fecha_actualizacion=curdate()
                                ,id_usuario_actualizacion=$this->idUsuarioActualizacion
                            WHERE id_solicitud_pk = $this->idSolicitudPk;";
            $this->conn->Preparar($sentenciaSql);
            $this->conn->Ejecutar();     
        }
        public function consultar(){
            if ($this->idSolicitudPk != '' ){
                $sentenciaSql = "SELECT * FROM solicitudes WHERE id_solicitud_pk = $this->idSolicitudPk;";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function ListarSolicitud(){
            $sentenciaSql = "SELECT 
                                sol.*
                                ,fic.codigo
                                ,concat(per.nombres,' ',per.apellido) AS nombre_persona
                            FROM 
                                solicitudes AS sol
                                INNER JOIN instructores AS ins ON ins.id_instructor_pk = sol.instructor
                                INNER JOIN fichas AS fic ON fic.id_ficha_pk = sol.ficha
                                INNER JOIN personas AS per ON per.id_persona_pk = ins.persona;";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }
        
        
        public function __destruct() {
            unset($this->idSolicitudPk);
            unset($this->instructorFk);
            unset($this->fichaFk);
            unset($this->descripcion);
            unset($this->estado);
            unset($this->fechaCreacion);
            unset($this->fechaActualizacion);
            unset($this->idUsuarioCreacion);
            unset($this->idUsuarioActualizacion);
            unset($this->conn);
        }
    }
?>