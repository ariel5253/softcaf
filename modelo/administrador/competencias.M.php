<?php
    class Competencias{  //Clase de Competencias.

        //Atributos.
        private $idCompetenciaPk;
        private $codigo;
        private $nombre;
        private $idProgramaFormacionFk;
        private $fechaCreacion;
        private $fechaActualizacion;
        private $idUsuarioCreacion;
        private $idUsuarioActualizacion;
        public  $conn=null;

        //Los métodos get y set,para mostrar(get) o modificar (set) el valor de un atributo.

       //Set y get del atributo idCompetenciaPk.
        public function getIdCompetenciaPk(){  
            return $this->idCompetenciaPk;
        }
        public function setIdCompetenciaPk($idCompetenciaPk){
            $this->idCompetenciaPk=$idCompetenciaPk;
        }
        //Set y get del atributo codigo.
        public function  setCodigo($codigo){
            $this->codigo=$codigo;
        }
        public function getCodigo(){
            return $this->codigo;
        }
        //Set y get del atributo nombre.
        public function  setNombre($nombre){
            $this->nombre=$nombre;
        }
        public function getNombre(){
            return  $this->nombre;
        }
        //Set y get del atributo idProgramaFormacionFk.
        public function setIdProgramaFormacionFk($idProgramaFormacionFk){
            $this->idProgramaFormacionFk=$idProgramaFormacionFk;
        }
        public function  getIdProgramaFormacionFk(){
            return $this ->idProgramaFormacionFk;
        }
        //Set y get del atributo fechaCreación.
        public function getFechaCreacion(){ 
            return $this->fechaCreacion;
        }
        public function setFechaCreacion($fechaCreacion){ 
            $this->fechaCreacion =$fechaCreacion;
        }
        //Set y get del atributo fechaActualización.
        public function getFechaActualizacion(){ 
            return $this->fechaActualizacion;
        }
        public function setFechaActualizacion($fechaActualizacion){
             $this->fechaActualizacion =$fechaActualizacion;
            }
        //Set y get del atributo idUsuarioCreación.
        public function getIdUsuarioCreacion(){ 
            return $this->idUsuarioCreacion;
        }
        public function setIdUsuarioCreacion($idUsuarioCreacion){ 
            $this->idUsuarioCreacion =$idUsuarioCreacion;
        }
        //Set y get del atributo idUsuarioActualización.
        public function getIdUsuarioActualizacion(){ 
            return $this->idUsuarioActualizacion;
        }
        public function setIdUsuarioActualizacion($idUsuarioActualizacion){ 
            $this->idUsuarioActualizacion =$idUsuarioActualizacion;
        }

        //conexion
        public function __construct(){
            $this->conn = new Conexion();
        }

        public function agregar(){
            $sentenciaSql = "INSERT INTO competencias(codigo
                                ,nombre
                                ,programa
                                ,fecha_creacion
                                ,fecha_actualizacion
                                ,id_usuario_creacion
                                ,id_usuario_actualizacion)
                            VALUES ($this->codigo
                                ,'$this->nombre'
                                ,$this->idProgramaFormacionFk
                                ,curdate()
                                ,curdate()
                                ,$this->idUsuarioCreacion
                                ,$this->idUsuarioActualizacion);";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
        }

        public function modificar(){
            $sentenciaSql = "UPDATE competencias SET codigo = $this->codigo
                                ,nombre = '$this->nombre'
                                ,programa = $this->idProgramaFormacionFk
                                ,fecha_actualizacion = curdate()
                                ,id_usuario_actualizacion = $this->idUsuarioActualizacion
                            WHERE id_competencia_pk = $this->idCompetenciaPk;";
            $this->conn->Preparar($sentenciaSql);
            $this->conn->Ejecutar();     
        }

        public function consultarPorId(){
            if ($this->idCompetenciaPk != '' ){
                $sentenciaSql = "SELECT 
                                    com.id_competencia_pk AS id_competencia
                                    ,com.codigo AS codigo_competencia
                                    ,com.nombre AS nombre_competencia
                                    ,com.programa AS id_programa
                                    ,prf.nombre AS nombre_programa
                                from 
                                    competencias AS com
                                    INNER JOIN programas_formacion AS prf ON prf.id_programas_formacion_pk = com.programa
                                WHERE id_competencia_pk = $this->idCompetenciaPk;";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function listarCompetencia(){
            $sentenciaSql = "SELECT 
                                com.id_competencia_pk AS id_competencia
                                ,com.codigo AS codigo_competencia
                                ,com.nombre AS nombre_competencia
                                ,prf.nombre AS nombre_programa
                            from 
                                competencias AS com
                                INNER JOIN programas_formacion AS prf ON prf.id_programas_formacion_pk = com.programa;";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function consultarPorNombre(){
            if ($this->nombre != '' ){
                $sentenciaSql = "SELECT 
                                    com.id_competencia_pk AS id_competencia
                                    ,com.codigo AS codigo_competencia
                                    ,com.nombre AS nombre_competencia
                                from 
                                    competencias AS com
                                WHERE concat(com.codigo,' ',com.nombre) like '%$this->nombre%';";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        /*
        public function agregar(){
            $sentenciaSql = "call agregar_guia('$this->nombre','$this->palabrasClave',$this->fichaFk,$this->competenciaFk,'$this->url',1,1);";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
        }

        public function modificar(){
            $sentenciaSql = "call modificar_guia($this->idGuiaPk,'$this->nombre',$this->fichaFk,$this->competenciaFk,1);";
            $this->conn->Preparar($sentenciaSql);
            $this->conn->Ejecutar();     
        }

        public function eliminar(){
            $sentenciaSql = "call eliminar_guia($this->idGuiaPk);";        
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
        }

        public function consultarParaListar(){
            $sentenciaSql = "call listar_competencia();";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }
        */

        public function __destruct() {
            unset($this->idCompetenciaPk);
            unset($this->codigo);
            unset($this->nombre);
            unset($this->idProgramaFormacionFk);
            unset($this->fechaCreacion);
            unset($this->fechaActualizacion);
            unset($this->idUsuarioCreacion);
            unset($this->idUsuarioActualizacion);
            unset($this->conn);
        }
    }
?>