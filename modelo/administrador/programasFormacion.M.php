<?php
    class ProgramasFormacion{ //Clase de ProgramasFormación.
        //Atributos.
        private $idProgramaFormacionPk;
        private $codigo;
        private $nombre;
        private $idRedesConocimientoFk;
        private $fechaCreacion;
        private $fechaModificacion;
        private $idUsuarioCreacion;
        private $idUsuarioModificacion;
        public $conn=null;
        //Los métodos get y set, para mostrar (get) o modificar (set) el valor de un atributo.
       //Set y get del atributo idProgramaFormacionPk.
        public function getIdProgramaFormacionPk(){
            return $this->idProgramaFormacionPk;
        }
        public function setIdProgramaFormacionPk($idProgramaFormacionPk){
            $this->idProgramaFormacionPk = $idProgramaFormacionPk;
        }
        //Set y get del atributo codigo.
        public function getCodigo(){
            return $this->codigo;
        }
        public function setCodigo($codigo){
            $this->codigo = $codigo;
        }
        //Set y get del atributo nombre.
        public function getNombre(){
            return $this->nombre;
        }
        public function setNombre($nombre){
            $this->nombre = $nombre;
        }
        //Set y get del atributo idRedesConocimientoFk.
        public function getIdRedesConocimientoFk(){
            return $this->idRedesConocimientoFk;
        }
        public function setIdRedesConocimientoFk($idRedesConocimientoFk){
            $this->idRedesConocimientoFk = $idRedesConocimientoFk;
        }
        //Set y get del atributo fechaCreación.
        public function getFechaCreacion(){ 
            return $this->fechaCreacion;
        }
        public function setFechaCreacion($fechaCreacion){ 
            $this->fechaCreacion =$fechaCreacion;
        }
        //Set y get del atributo fechaActualización.
        public function getFechaActualizacion(){ 
            return $this->fechaActualizacion;
        }
        public function setFechaActualizacion($fechaActualizacion){
             $this->fechaActualizacion =$fechaActualizacion;
            }
        //Set y get del atributo idUsuarioCreación.
        public function getIdUsuarioCreacion(){ 
            return $this->idUsuarioCreacion;
        }
        public function setIdUsuarioCreacion($idUsuarioCreacion){ 
            $this->idUsuarioCreacion =$idUsuarioCreacion;
        }
        //Set y get del atributo idUsuarioActualización.
        public function getIdUsuarioActualizacion(){ 
            return $this->idUsuarioActualizacion;
        }
        public function setIdUsuarioActualizacion($idUsuarioActualizacion){ 
            $this->idUsuarioActualizacion =$idUsuarioActualizacion;
        }

        //conexion
        public function __construct(){
            $this->conn = new Conexion(); 
        }

        public function agregar(){
            $sentenciaSql = "INSERT INTO programas_formacion(codigo
                                ,nombre
                                ,red_conocimiento
                                ,fecha_creacion
                                ,fecha_actualizacion
                                ,id_usuario_creacion
                                ,id_usuario_actualizacion)
                            VALUES ($this->codigo
                                ,'$this->nombre'
                                ,$this->idRedesConocimientoFk
                                ,curdate()
                                ,curdate()
                                ,$this->idUsuarioCreacion
                                ,$this->idUsuarioActualizacion);";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
        } 
        
        public function modificar(){
            $sentenciaSql = "UPDATE programas_formacion SET codigo = $this->codigo
                                ,nombre = '$this->nombre'
                                ,red_conocimiento = $this->idRedesConocimientoFk
                                ,fecha_actualizacion = curdate()
                                ,id_usuario_actualizacion = $this->idUsuarioActualizacion
                            WHERE id_programas_formacion_pk = $this->idProgramaFormacionPk;";
            $this->conn->Preparar($sentenciaSql);
            $this->conn->Ejecutar();     
        }

        public function consultarPorId(){
            if ($this->idProgramaFormacionPk != '' ){
                $sentenciaSql = "SELECT 
                                    prf.id_programas_formacion_pk AS id_programa
                                    ,prf.codigo AS codigo_programa
                                    ,prf.nombre AS nombre_programa
                                    ,red.nombre AS nombre_red
                                    ,prf.red_conocimiento AS id_red
                                FROM 
                                    programas_formacion AS prf
                                    INNER JOIN redes_conocimiento AS red ON red.id_redes_conocimiento_pk = prf.red_conocimiento
                                WHERE id_programas_formacion_pk = $this->idProgramaFormacionPk;";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function consultarPorNombre(){
            if ($this->nombre != '' ){
                $sentenciaSql = "SELECT * FROM programas_formacion WHERE nombre like '%$this->nombre%';";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function consultarFichasPorPrograma(){
            if ($this->idProgramaFormacionPk != '' ){
                $sentenciaSql = "SELECT 
                                    fic.id_ficha_pk
                                    ,fic.codigo
                                FROM 
                                    fichas AS fic 
                                    INNER JOIN programas_formacion AS prf ON prf.id_programas_formacion_pk = fic.programa
                                WHERE prf.id_programas_formacion_pk = $this->idProgramaFormacionPk;";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        public function listarPrograma(){
            $sentenciaSql = "SELECT 
                                prf.id_programas_formacion_pk AS id_programa
                                ,prf.codigo AS codigo_programa
                                ,prf.nombre AS nombre_programa
                                ,red.nombre AS nombre_red
                            FROM 
                                programas_formacion AS prf
                                INNER JOIN redes_conocimiento AS red ON red.id_redes_conocimiento_pk = prf.red_conocimiento;";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }
        
        public function __destruct() {
            unset($this->idProgramaFormacionPk);
            unset($this->codigo);
            unset($this->nombre);
            unset($this->idRedesConocimientoFk);
            unset($this->fechaCreacion);
            unset($this->fechaActualizacion);
            unset($this->idUsuarioCreacion);
            unset($this->idUsuarioActualizacion);
            unset($this->conn);
        }
    }
?>
