<?php
 class InstructorFicha{ //Clase de GuiasAprendizaje.
        //Atributos.
        private $idInstructorFichaPk;
        private $idFichaFk;
        private $idInstructorfk;
        private $fechaCreacion;
        private $fechaActualizacion;
        private $idUsuarioCreacion;
        private $idUsuarioActualizacion;
        public  $conn=null;

        //Los métodos get y set,para mostrar (get) o modificar (set) el valor de un atributo.
       //Set y get del atributo idGuiasAprendizajePk.
        public function getIdInstructorFichaPk(){  
            return $this->idInstructorFichaPk;
        }
        public function setIdInstructorFichaPk($idInstructorFichaPk){
            $this->idInstructorFichaPk=$idInstructorFichaPk;
        }
        //Set y get del atributo idFichaFk.
        public function setIdFichaFk($idFichaFk){
            $this->idFichaFk=$idFichaFk;
        }
        public function  getIdFichaFk(){
            return $this ->idFichaFk;
        }
        //Set y get del atributo idCompetenciaFk.
        public function setIdInstructorfk($idInstructorfk){
            $this->idInstructorfk=$idInstructorfk;
        }
        public function  getIdInstructorfk(){
            return $this ->idInstructorfk;
        }
        //Set y get del atributo fechaCreación.
        public function getFechaCreacion(){ 
            return $this->fechaCreacion;
        }
        public function setFechaCreacion($fechaCreacion){ 
            $this->fechaCreacion =$fechaCreacion;
        }
        //Set y get del atributo fechaActualización.
        public function getFechaActualizacion(){ 
            return $this->fechaActualizacion;
        }
        public function setFechaActualizacion($fechaActualizacion){
             $this->fechaActualizacion =$fechaActualizacion;
            }
        //Set y get del atributo idUsuarioCreación.
        public function getIdUsuarioCreacion(){ 
            return $this->idUsuarioCreacion;
        }
        public function setIdUsuarioCreacion($idUsuarioCreacion){ 
            $this->idUsuarioCreacion =$idUsuarioCreacion;
        }
        //Set y get del atributo idUsuarioActualización.
        public function getIdUsuarioActualizacion(){ 
            return $this->idUsuarioActualizacion;
        }
        public function setIdUsuarioActualizacion($idUsuarioActualizacion){ 
            $this->idUsuarioActualizacion =$idUsuarioActualizacion;
        }
        //conexion
        public function __construct(){
            $this->conn = new Conexion();
        }

        public function consultarParaListar(){
            $sentenciaSql = "SELECT 
                                    infic.ficha As id_ficha
                                    ,ficha.codigo As codigo_ficha
                                FROM 
                                    instructores_fichas AS infic
                                    inner join fichas AS ficha ON ficha.id_ficha_pk = infic.ficha
                                    inner join instructores AS instr ON instr.id_instructor_pk = infic.instructor
                                WHERE infic.instructor = $this->idInstructorfk;";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }

        /*
        public function consultarParaListar(){
            $sentenciaSql = "call listar_ficha($this->idInstructorfk);";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }
        */

        public function __destruct() {
            unset($this->idInstructorFichaPk);
            unset($this->idFichaFk);
            unset($this->idInstructorfk);
            unset($this->fechaCreacion);
            unset($this->fechaActualizacion);
            unset($this->idUsuarioCreacion);
            unset($this->idUsuarioActualizacion);
            unset($this->conn);
        }
    }
?>