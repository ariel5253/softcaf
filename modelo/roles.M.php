<?php
    class Roles{ //Clase de Roles.
        //Atributos.
        private $idRolPk;
        private $nombre;
        private $fechaCreacion;
        private $fechaModificacion;
        private $idUsuarioCreacion;
        private $idUsuarioModificacion;
        public $conn=null;
        //Los métodos get y set,(get) o modificar (set) el valor de un atributo.
       //Set y get del atributo idRol.
        public function getIdRolPk(){
            return $this->idRolPk;
        }
        public function setIdRolPk($idRolPk){
            $this->idRol = $idRolPk;
        }
        //Set y get del atributo nombre.
        public function getNombre(){
            return $this->nombre;
        }
        public function setNombre($nombre){
            $this->nombre = $nombre;
        }
        //Set y get del atributo fechaCreación.
        public function getFechaCreacion(){ 
            return $this->fechaCreacion;
        }
        public function setFechaCreacion($fechaCreacion){ 
            $this->fechaCreacion =$fechaCreacion;
        }
        //Set y get del atributo fechaActualización.
        public function getFechaActualizacion(){ 
            return $this->fechaActualizacion;
        }
        public function setFechaActualizacion($fechaActualizacion){
             $this->fechaActualizacion =$fechaActualizacion;
            }
        //Set y get del atributo idUsuarioCreación.
        public function getIdUsuarioCreacion(){ 
            return $this->idUsuarioCreacion;
        }
        public function setIdUsuarioCreacion($idUsuarioCreacion){ 
            $this->idUsuarioCreacion =$idUsuarioCreacion;
        }
        //Set y get del atributo idUsuarioActualización.
        public function getIdUsuarioActualizacion(){ 
            return $this->idUsuarioActualizacion;
        }
        public function setIdUsuarioActualizacion($idUsuarioCreacion){ 
            $this->idUsuarioActualizacion =$idUsuarioActualizacion;
        }
    }
?>