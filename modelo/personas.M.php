<?php
    class Personas //Clase de Personas.
    {
        //Atributos.
        private $idPersonaPk;
        private $nombres;
        private $apellido;
        private $tipoSangre;
        private $tipoDocumento;
        private $numeroDocumento;
        private $edad;
        private $genero;
        private $telefono;
        private $correo;
        private $direccion;
        private $fechaCreacion;
        private $fechaActualizacion;
        private $idUsuarioCreacion;
        private $idUsuarioActualizacion;
        public $conn=null;
        //Los métodos get y set, para mostrar (get) o modificar (set) el valor de un atributo.
       //Set y get del atributo idPersonaPk.
        public function getIdPersonaPk(){
            return $this->idPersonaPk;
        }
        public function setIdPersonaPk($idPersonaPk){
            $this->idPersonaPk = $idPersonaPk;
        }
        //Set y get del atributo nombres.
        public function getNombres(){
            return $this->nombres;
        }
        public function setNombres($nombres){
            $this->nombres = $nombres;
        }
        //Set y get del atributo apellido.
        public function getApellido(){ 
            return $this->apellido;
        }
        public function setApellido($apellido){
            $this->apellido = $apellido;
        }
        //Set y get del atributo tipoSangre.
        public function getTipoSangre(){
            return $this->tipoSangre;
        }
        public function setTipoSangre($tipoSangre){
            $this->tipoSangre = $tipoSangre;
        }
        //Set y get del atributo tipoDocumento.
        public function getTipoDocumento(){
            return $this->tipoDocuemnto;
        }
        public function setTipoDocumento($tipoDocumento){
            $this->tipoDocumento= $tipoDocumento;
        }
        //Set y get del atributo numeroDocumento.
        public function getNumeroDocumento(){
            return $this->numeroDocumento;
        }
        public function setNumeroDocumento($numeroDocumento){
            $this->numeroDocumento = $numeroDocumento;
        }
        //Set y get del atributo edad.
        public function getEdad(){
            return $this->edad;
        }
        public function setEdad($edad){
            $this->edad = $edad;
        }
        //Set y get del atributo genero.
        public function getGenero(){
            return $this->genero;
        }
        public function setGenero($genero){
            $this->genero = $genero;
        }
        //Set y get del atributo telefono.
        public function getTelefono(){
            return $this->telefono;
        }
        public function setTelefono($telefono){
            $this->telefono = $telefono;
        }
        //Set y get del atributo correo.
        public function getCorreo(){
            return $this->correo;
        }
        public function setCorreo($correo){
            $this->correo = $correo;
        }
        //Set y get del atributo direccion.
        public function getDireccion(){
            return $this->direccion;
        }
        public function setDireccion($direccion){
            $this->direccion = $direccion;
        }
        //Set y get del atributo fechaCreación.
        public function getFechaCreacion(){ 
            return $this->fechaCreacion;
        }
        public function setFechaCreacion($fechaCreacion){ 
            $this->fechaCreacion =$fechaCreacion;
        }
        //Set y get del atributo fechaActualización.
        public function getFechaActualizacion(){ 
            return $this->fechaActualizacion;
        }
        public function setFechaActualizacion($fechaActualizacion){
             $this->fechaActualizacion =$fechaActualizacion;
            }
        //Set y get del atributo idUsuarioCreación.
        public function getIdUsuarioCreacion(){ 
            return $this->idUsuarioCreacion;
        }
        public function setIdUsuarioCreacion($idUsuarioCreacion){ 
            $this->idUsuarioCreacion =$idUsuarioCreacion;
        }
        //Set y get del atributo idUsuarioActualización.
        public function getIdUsuarioActualizacion(){ 
            return $this->idUsuarioActualizacion;
        }
        public function setIdUsuarioActualizacion($idUsuarioActualizacion){ 
            $this->idUsuarioActualizacion =$idUsuarioActualizacion;
        }
        public function agregar(){
            $sentenciaSql = "INSERT INTO personas(nombres
                                ,apellido
                                ,tipo_sangre
                                ,tipo_documento
                                ,numero_documento
                                ,edad
                                ,genero
                                ,telefono
                                ,correo
                                ,direccion
                                ,id_usuario_actualizacion)
                            VALUES ('$this->nombres'
                                ,'$this->apellido'
                                ,'$this->tipo_sangre'
                                ,'$this->tipo_documento'
                                ,'$this->numero_documento'
                                ,'$this->edad'
                                ,'$this->genero'
                                ,'$this->telefono'
                                ,'$this->correo'
                                ,'$this->direccion'
                                ,curdate()
                                ,curdate()
                                ,$this->idUsuarioCreacion
                                ,$this->idUsuarioActualizacion);";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
        } 
        public function modificar(){
            $sentenciaSql = "UPDATE personas SET nombres='$this->nombres'
                                ,apellido='$this->apellido'
                                ,tipo_sangre=$this->tipo_sangre
                                ,tipo_documento=$this->tipo_documento
                                ,numero_documento=$this->numero_documento
                                ,edad=$this->edad
                                ,genero=$this->genero
                                ,telefono=$this->telefono
                                ,correo=$this->correo
                                ,direccion=$this->direccion
                                ,fecha_actualizacion=curdate()
                                ,id_usuario_actualizacion=$this->idUsuarioActualizacion
                            WHERE id_persona_pk = $this->idPersonaPk;";
            $this->conn->Preparar($sentenciaSql);
            $this->conn->Ejecutar();     
        }
        public function consultar(){
            if ($this->idPersonaPk != '' ){
                $sentenciaSql = "SELECT * FROM personas WHERE id_persona_pk = $this->idPersonaPk;";
            }
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true; 
        }
        //conexion
        public function __construct(){
            $this->conn = new Conexion(); 
        }
        
        public function __destruct() {
            unset($this->idPersonaPk);
            unset($this->nombres);
            unset($this->apellidos);
            unset($this->tipoSangre);
            unset($this->tipoDocumento);
            unset($this->numeroDocumento);
            unset($this->edad);
            unset($this->genero);
            unset($this->telefono);
            unset($this->correo);
            unset($this->direccion);
            unset($this->fechaCreacion);
            unset($this->fechaActualizacion);
            unset($this->idUsuarioCreacion);
            unset($this->idUsuarioActualizacion);
            unset($this->conn);
        }
    }
?>
