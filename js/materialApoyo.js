var primeraFila;
$(function () {
    primeraFila=$("#fila");
    
    $("#btnAgregar").click(function() {

        if ($("#txtNombreMaterial").val() != "" && $("#txtPalabrasMaterial").val() != "") {
            Agregar();
        }else{
            alert("Por favor llene todos los campos");
        }
        
    });

    $("#btnModificar").click(function() {

        if ($("#txtNombreMaterial").val() != "" && $("#txtPalabrasMaterial").val() != "") {
            Modificar();
        }else{
            alert("Por favor llene todos los campos");
        }
        
    });
});

function Modificar(){
    $("#hidAccion").val("MODIFICAR");
    var formData = new FormData($("#frmMaterialApoyo")[0]);
    $.ajax({
            data: formData, //datos que se van a enviar al ajax
            url: '../../controlador/materialApoyo.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            contentType: false,
            processData: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if(respuesta['accion']=='MODIFICAR'){
                    alert(respuesta['respuesta']);
                    limpiar(); 
                    location.reload();
                }
            }
    });
}

function Consultar(id){
    var parametros={
        "hidAccion":"CONSULTAR",
        "hidIdMaterial":id
    }

    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/materialApoyo.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if(respuesta['accion']=='CONSULTAR'){

                    $('#txtNombreMaterial').val(respuesta.datos.nombre);
                    $('#txtPalabrasMaterial').val(respuesta.datos.palabras_clave);
                    $('#hidIdGuia').val(respuesta.datos.guia_aprendizaje);
                    $('#hidIdMaterial').val(respuesta.datos.id_materiales_apoyo_pk);
                    $('#hidUrl').val(respuesta.datos.url);
                    $('#hidNombre').val(respuesta.datos.nombre);

                }
            }
    }); 
}

function Agregar(){
    $("#hidAccion").val("ADICIONAR");
    var formData = new FormData($("#frmMaterialApoyo")[0]);
    $.ajax({
            data: formData, //datos que se van a enviar al ajax
            url: '../../controlador/materialApoyo.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            contentType: false,
            processData: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if(respuesta['accion']=='ADICIONAR'){
                    alert(respuesta['respuesta']); 
                    location.reload();
                }
            }
    });
}

function Listar(){
     
    $(".otraFila").remove(); 
    $("#dataTable2 tbody").append(primeraFila);
    var parametros={
        "hidAccion":"LISTAR"
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/materialApoyo.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if (respuesta['accion']=='LISTAR') {
                    $.each(respuesta.lista, function (i, lista) {
                        if ($("#hidIdGuia").val()==lista[5]) {
                            $("#aguia").html(lista[1]);
                            $("#bguia").html(lista[3]);
                            $("#cguia").html(lista[2]);
                            $("#aArchivo").attr("href","../"+lista[4]+"");
                            $("#btnEditar").attr("onclick","Consultar("+lista[0]+")");
                            $("#dataTable2 tbody").append($("#fila").clone(true).attr("class","otraFila"));
                        }
                    });  
                    $(".primeraFila").remove();
                }
                var t = $('#dataTable2').DataTable( {
                    "lengthMenu": [[3, 10, 25, 50, -1], [3, 10, 25, 50, "All"]],
                    "columnDefs": [ {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    } ],
                    "order": [[ 1, 'asc' ]],
                    "language": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sSearch": "Buscar:",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                          "sFirst": "Primero",
                          "sLast": "Último",
                          "sNext": "Siguiente",
                          "sPrevious": "Anterior"
                        },
                        "oAria": {
                          "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        },
                        "buttons": {
                          "copy": "Copiar",
                          "colvis": "Visibilidad"
                        }
                    }
                } );
             
                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    } );
                } ).draw();
            }
    });
} 