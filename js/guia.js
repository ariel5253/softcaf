var primeraFila;
$(function () {
    primeraFila=$("#fila");
    primeraFilao=$("#filao");


    $("#cmbFicha").click(function() {
        ListarCom();
    });
    $("#cmbComp").click(function() {
        ListarRap();
    });

    $("#btnAgregar").click(function() {

        if ($("#txtNombre").val() != "" && $("#txtPalabras").val() != "" && $("#cmbFicha").val() != "") {
            Agregar();
        }else{
            alert("Por favor llene todos los campos"); 
        }
        
    });

    $("#btnModificar").click(function() {

        if ($("#txtNombre").val() != "" && $("#txtPalabras").val() != "" && $("#cmbFicha").val() != "") {
            Modificar();
        }else{
            alert("Por favor llene todos los campos");
        }
        
    });
});

function Modificar(){
    $("#hidAccion").val("MODIFICAR");
    var formData = new FormData($("#frmGuia")[0]);
    $.ajax({
            data: formData, //datos que se van a enviar al ajax
            url: '../../controlador/guia.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            contentType: false,
            processData: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if(respuesta['accion']=='MODIFICAR'){
                    alert(respuesta['respuesta']);
                    location.reload();
                }
            }
    });
}

function Consultar(id){ 
    var parametros={
        "hidAccion":"CONSULTAR",
        "hidIdGuia":id
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/guia.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if(respuesta['accion']=='CONSULTAR'){

                    $("#cmbFicha").val(respuesta.datos.ficha);
                    ListarCom();
                    Consul(respuesta.datos.id_guias_aprendizaje_pk);

                }
            }
    });
}

function Consul(id){
    var parametros={
        "hidAccion":"CONSULTAR",
        "hidIdGuia":id
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/guia.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if(respuesta['accion']=='CONSULTAR'){

                    $("#hidIdGuia").val(respuesta.datos.id_guias_aprendizaje_pk);
                    $("#txtNombre").val(respuesta.datos.nombre);
                    $("#txtPalabras").val(respuesta.datos.palabrasClave);
                    $("#cmbFicha").val(respuesta.datos.ficha);
                    $("#cmbComp .otraCom[value="+respuesta.datos.competencia+"]").attr("selected", true);
                    $("#hidNombre").val(respuesta.datos.nombre);
                    $("#hidUrl").val(respuesta.datos.url);
                    $("#cmbComp").val(respuesta.datos.competencia);
            
                }
            }
    });
}

function Agregar(){
    $("#hidAccion").val("ADICIONAR");
    var formData = new FormData($("#frmGuia")[0]);
    $.ajax({
            data: formData, //datos que se van a enviar al ajax
            url: '../../controlador/guia.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            contentType: false,
            processData: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if(respuesta['accion']=='ADICIONAR'){
                    alert(respuesta['respuesta']); 
                }
            }
    });
}

function Listar(){

    $(".otraFila").remove(); 
    $("#dataTable1 tbody").append(primeraFila);
    var parametros={
        "hidAccion":"LISTAR"
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/guia.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if (respuesta['accion']=='LISTAR') {
                    $.each(respuesta.listagui, function (i, lista) {
                        if ($("#hidIdSesion").val()===lista[8]) {
                            $("#aguia").html(lista[1]);
                            $("#bguia").html(lista[2]);
                            $("#cguia").html(lista[3]);
                            $("#dguia").html(lista[4]);
                            $("#aArchivo").attr("href","../"+lista[5]+"");
                            $("#IdAgregarMaterial").attr("href","index.php?pg=materialApoyo.V&bd="+lista[0]+"");
                            $("#btnEditar").attr("onclick","Consultar("+lista[0]+")");
                            $("#dataTable1 tbody").append($("#fila").clone(true).attr("class","otraFila"));
                        }
                    });  
                    $(".primeraFila").remove();
                }
                var t = $('#dataTable1').DataTable( {
                    "lengthMenu": [[3, 10, 25, 50, -1], [3, 10, 25, 50, "All"]],
                    "columnDefs": [ {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    } ],
                    "order": [[ 1, 'asc' ]],
                    "language": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sSearch": "Buscar:",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                          "sFirst": "Primero",
                          "sLast": "Último",
                          "sNext": "Siguiente",
                          "sPrevious": "Anterior"
                        },
                        "oAria": {
                          "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        },
                        "buttons": {
                          "copy": "Copiar",
                          "colvis": "Visibilidad"
                        }
                    }
                } );
             
                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    } );
                } ).draw();
            }
    });
}

function ListarCom(){
    $("#cmbComp .otraCom").remove();
    var parametros={
        "hidAccion":"LISTARCOMPETENCIA",
        "cmbFicha":$("#cmbFicha").val()
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/guia.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if (respuesta['accion']=='LISTAR') {
                    $.each(respuesta.listaCom, function (i, lista) {
                        $('#cmbComp').append(
                            $('<option>',{
                                value: lista[0],
                                text: lista[2]
                            }).attr("class","otraCom")
                        );  
                    });  
                }
            }
    });
}

function ListarFic(){
    var parametros={
        "hidAccion":"LISTARFICHA",
        "hidPersona":$("#hidPersona").val()
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/guia.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if (respuesta['accion']=='LISTAR') {
                    $.each(respuesta.listaFic, function (i, lista) {
                        $('#cmbFicha').append(
                            $('<option>',{
                                value: lista[0],
                                text: lista[1]
                            })
                        );  
                    });  
                }
            }
    });
}

function ListarRap(){
    $("#cmbRap .otraRap").remove();
    var parametros={
        "hidAccion":"LISTARRAP",
        "cmbComp":$("#cmbComp").val()
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/guia.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if (respuesta['accion']=='LISTAR') {
                    $.each(respuesta.listaRap, function (i, lista) {
                        $('#cmbRap').append(
                            $('<option>',{
                                value: lista[0],
                                text: lista[1]
                            }).attr("class","otraRap")
                        );  
                    });  
                }
            }
    });
}