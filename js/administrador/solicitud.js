$(function() {
    //se carga el autocompleta del contratista
    $("#txtPrograma").autocomplete({ 
        source:'../../busqueda/programasFormacion.php',
        select:function(event, ui){
            $("#hidPrograma").val(ui.item.id_programa); 
            ListarFic();
        } 
    }); 
});

var primeraFilaSol;
var primeraFilaAdm;

$(function () {
    primeraFilaSol=$("#filaSol");
    primeraFilaAdm=$("#filaAdm");
    
    $("#btnAgregar").click(function() {

        if ($("#txtAsunto").val() != "" && $("#cmbFicha").val() != "") {
            Agregar();
        }else{
            alert("Por favor llene todos los campos");
        }
        
    });

    $("#btnModificar").click(function() {

        if ($("#txtNombre").val() != "") {
            if ($("#hidRedConocimiento").val() != "") {
                Modificar();
            }else{
                alert("Por favor consulte primero");
            }
        }else{
            alert("Por favor llene todos los campos");
        }
        
    });
})

function Agregar(){
    $("#hidAccion").val("ADICIONAR");
    $.ajax({
        data: $("#frmSolicitud").serialize(), //datos que se van a enviar al ajax
        url: '../../controlador/administrador/solicitud.C.php', //archivo php que recibe los datos 
        type: 'post', //método para enviar los datos
        dataType: 'json',//Recibe el array desde php
        cache: false,
        success:  function (respuesta) { //procesa y devuelve la respuesta
            console.log(respuesta); 
            alert(respuesta['respuesta']);
            location.reload();
        }
    });
}

function Modificar(id,ins,fic){
    var parametros = {
        "hidAccion":"MODIFICAR",
        "hidSolicitud":id,
        "hidInstructor":ins,
        "hidFicha":fic,
        "hidIdSesion": $("#hidIdSesion").val(),
        "hidEstadoInsFic": "ACTIVO",
        "hidEstado": $("#hidEstado").val()
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/administrador/solicitud.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if(respuesta['accion']=='MODIFICAR'){
                    alert(respuesta['respuesta']);
                    location.reload();
                }
            }
    });
}

function ListarParaUsuario(){

    $(".otraFila").remove(); 
    $("#dataTable13 tbody").append(primeraFilaSol);
    var parametros={
        "hidAccion":"LISTAR"
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/administrador/solicitud.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if (respuesta['accion']=='LISTAR') {
                    $.each(respuesta.listaSol, function (i, lista) {
                        if ($("#hidIdSesion").val()===lista[7]) {
                            $("#asol").html(lista[3]);
                            $("#bsol").html(lista[9]);
                            $("#btnEstado").val(lista[4]);
                            $("#dataTable13 tbody").append($("#filaSol").clone(true).attr("class","otraFila"));
                        }
                    });  
                    $(".primeraFilaSol").remove();
                }
                var t = $('#dataTable13').DataTable( {
                    "lengthMenu": [[3, 10, 25, 50, -1], [3, 10, 25, 50, "All"]],
                    "columnDefs": [ {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    } ],
                    "order": [[ 1, 'asc' ]],
                    "language": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sSearch": "Buscar:",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                          "sFirst": "Primero",
                          "sLast": "Último",
                          "sNext": "Siguiente",
                          "sPrevious": "Anterior"
                        },
                        "oAria": {
                          "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        },
                        "buttons": {
                          "copy": "Copiar",
                          "colvis": "Visibilidad"
                        }
                    }
                } );
             
                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    } );
                } ).draw();

            }
    });
}

function ListarParaAdministrador(){

    $(".otraFila").remove(); 
    $("#dataTable14 tbody").append(primeraFilaAdm);
    var parametros={
        "hidAccion":"LISTAR"
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/administrador/solicitud.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if (respuesta['accion']=='LISTAR') {
                    $.each(respuesta.listaSol, function (i, lista) {
                        $("#aadm").html(lista[10]);
                        $("#badm").html(lista[3]);
                        $("#cadm").html(lista[9]);
                        $("#btnEstado").val(lista[4]);
                        $("#btnEstado").attr("onclick","Modificar("+lista[0]+","+lista[1]+","+lista[2]+")");
                        if (lista[4] == "Aprobado") {
                            document.getElementById("btnEstado").style.backgroundColor= "blue";
                        }else{
                            document.getElementById("btnEstado").style.backgroundColor= "red";
                        }
                        $("#dataTable14 tbody").append($("#filaAdm").clone(true).attr("class","otraFila"));
                    });  
                    $(".primeraFilaAdm").remove();
                }
                var t = $('#dataTable14').DataTable( {
                    "lengthMenu": [[3, 10, 25, 50, -1], [3, 10, 25, 50, "All"]],
                    "columnDefs": [ {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    } ],
                    "order": [[ 1, 'asc' ]],
                    "language": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sSearch": "Buscar:",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                          "sFirst": "Primero",
                          "sLast": "Último",
                          "sNext": "Siguiente",
                          "sPrevious": "Anterior"
                        },
                        "oAria": {
                          "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        },
                        "buttons": {
                          "copy": "Copiar",
                          "colvis": "Visibilidad"
                        }
                    }
                } );
             
                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    } );
                } ).draw();

            }
    });
}

function ListarFic(){
    $("#cmbFicha .otraFilaop").remove();
    var parametros={
        "hidAccion":"LISTARFICHA", 
        "hidPrograma":$("#hidPrograma").val()
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/administrador/solicitud.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if (respuesta['accion']=='LISTAR') {
                    $.each(respuesta.listaFic, function (i, lista) {
                        $('#cmbFicha').append(
                            $('<option>',{
                                value: lista[0],
                                text: lista[1]
                            }).attr("class","otraFilaop")
                        );  
                    });  
                }
            }
    });
}

function ConsultarInstructor(){
    var parametros={
        "hidAccion":"CONSULTARINSTRUCTOR",
        "hidPersona":$("#hidPersona").val()
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/administrador/solicitud.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if(respuesta['accion']=='CONSULTAR'){

                    $("#hidInstructor").val(respuesta.datos.id_instructor_pk);
            
                }
            }
    });
}