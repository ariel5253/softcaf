$(function() {
    //se carga el autocompleta del contratista
    $("#txtRedConocimiento").autocomplete({ 
        source:'../../busqueda/redesConocimiento.php',
        select:function(event, ui){
            $("#hidRedConocimiento").val(ui.item.id_red); 
        } 
    }); 
});

var primeraFilaPer;

$(function () {
    primeraFilaPer=$("#filaPer"); 
     
    $("#btnAgregar").click(function() {

        if ($("#txtNombres").val() != "" && $("#txtapellidos").val() != "" && $("#txtDocumento").val() != "" && $("#txtEdad").val() != "" && $("#txtTelefono").val() != "" && $("#txtDireccion").val() != ""&& $("#txtCorreo").val() != "") {
            Agregar();
        }else{
            alert("Por favor llene todos los campos");
        }
        
    });

    $("#btnModificar").click(function() {

        if ($("#txtNombres").val() != "" && $("#txtapellidos").val() != "" && $("#txtDocumento").val() != "" && $("#txtEdad").val() != "" && $("#txtTelefono").val() != "" && $("#txtDireccion").val() != ""&& $("#txtCorreo").val() != "") {
            Modificar();
        }else{
            alert("Por favor llene todos los campos");
        }
        
    });
})

function Agregar(){
    $("#hidAccion").val("ADICIONAR");
    $.ajax({
        data: $("#frmPersona").serialize(), //datos que se van a enviar al ajax
        url: '../../controlador/administrador/personas.C.php', //archivo php que recibe los datos 
        type: 'post', //método para enviar los datos
        dataType: 'json',//Recibe el array desde php
        cache: false,
        success:  function (respuesta) { //procesa y devuelve la respuesta
            console.log(respuesta); 
            alert(respuesta['respuesta']);
            location.reload();
        }
    });
}

function Modificar(){
    $("#hidAccion").val("MODIFICAR");
    $.ajax({
            data: $("#frmPersona").serialize(), //datos que se van a enviar al ajax
            url: '../../controlador/administrador/personas.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if(respuesta['accion']=='MODIFICAR'){
                    alert(respuesta['respuesta']);
                    location.reload();
                }
            }
    });
}

function Consultar(id){
    var parametros={
        "hidAccion":"CONSULTAR",
        "hidPersona":id
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/administrador/personas.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if(respuesta['accion']=='CONSULTAR'){

                    $("#hidPersona").val(respuesta.datos.id_persona_pk);
                    $("#txtNombres").val(respuesta.datos.nombres);
                    $("#txtapellidos").val(respuesta.datos.apellido);
                    $("#cmbTipoDocumento").val(respuesta.datos.tipo_documento);
                    $("#txtDocumento").val(respuesta.datos.numero_documento );
                    $("#cmbTipoSangre").val(respuesta.datos.tipo_sangre);
                    $("#cmbGenero").val(respuesta.datos.genero);
                    $("#txtEdad").val(respuesta.datos.edad);
                    $("#txtTelefono").val(respuesta.datos.telefono);
                    $("#txtDireccion").val(respuesta.datos.direccion);
                    $("#txtCorreo").val(respuesta.datos.correo );
                    $("#hidCorreo").val(respuesta.datos.correo );
                    $("#cmbRol").val(respuesta.datos.id_rol );
                    if ($("#cmbRol").val() == 2) {
                        $("#txtRedConocimiento").prop( "disabled", false );
                        ConsultarRed(respuesta.datos.id_persona_pk);
                    }else{
                        $("#txtRedConocimiento").val(null);
                        $("#hidRedConocimiento").val(null);
                        $("#txtRedConocimiento").prop( "disabled", true );
                    }
                }
            }
    });
}

function ConsultarRed(id){
    var parametros={
        "hidAccion":"CONSULTARRED",
        "hidPersona":id
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/administrador/personas.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if(respuesta['accion']=='CONSULTAR'){

                    $("#txtRedConocimiento").val(respuesta.datos.nombre_red);
                    $("#hidRedConocimiento").val(respuesta.datos.id_red);
            
                }
            }
    });
}

function Listar(){

    $(".otraFila").remove(); 
    $("#dataTable10 tbody").append(primeraFilaPer);
    var parametros={
        "hidAccion":"LISTAR"
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/administrador/personas.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if (respuesta['accion']=='LISTAR') {
                    $.each(respuesta.listaPer, function (i, lista) {
                        $("#aper").html(lista[1]);
                        $("#bper").html(lista[2]);
                        $("#cper").html(lista[3]);
                        $("#dper").html(lista[4]);
                        $("#eper").html(lista[5]);
                        $("#btnEditar").attr("onclick","Consultar("+lista[0]+")");
                        $("#dataTable10 tbody").append($("#filaPer").clone(true).attr("class","otraFila"));
                    });  
                    $(".primeraFilaPer").remove();
                }
                //Diseño de la tabla
                var t = $('#dataTable10').DataTable( {
                    "lengthMenu": [[3, 10, 25, 50, -1], [3, 10, 25, 50, "All"]],
                    "columnDefs": [ {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    } ],
                    "order": [[ 1, 'asc' ]],
                    "language": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sSearch": "Buscar:",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                          "sFirst": "Primero",
                          "sLast": "Último",
                          "sNext": "Siguiente",
                          "sPrevious": "Anterior"
                        },
                        "oAria": {
                          "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        },
                        "buttons": {
                          "copy": "Copiar",
                          "colvis": "Visibilidad"
                        }
                    }
                } );
             
                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    } );
                } ).draw();
            }
    });
}

function ListarRol(){
    var parametros={
        "hidAccion":"LISTARROL",
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/administrador/personas.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if (respuesta['accion']=='LISTAR') {
                    $.each(respuesta.listaRol, function (i, lista) {
                        $('#cmbRol').append(
                            $('<option>',{
                                value: lista[0],
                                text: lista[1]
                            })
                        );  
                    });  
                }
            }
    });
}