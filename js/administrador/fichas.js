$(function() {
    //se carga el autocompleta del contratista
    $("#txtPrograma").autocomplete({ 
        source:'../../busqueda/programasFormacion.php',
        select:function(event, ui){
            $("#hidPrograma").val(ui.item.id_programa); 
        } 
    }); 
});

$(function() {
    //se carga el autocompleta del contratista
    $("#txtCentro").autocomplete({ 
        source:'../../busqueda/centrosFormacion.php',
        select:function(event, ui){
            $("#hidCentroFormacion").val(ui.item.id_centro); 
        } 
    }); 
});

var primeraFilaFic;

$(function () {
    primeraFilaFic=$("#filafic");
    
    $("#btnAgregar").click(function() {

        if ($("#txtNumeroFicha").val() != "" && $("#txtPrograma").val() != "" && $("#txtCentro").val() != "") {
            if ($("#hidPrograma").val() != "" && $("#hidCentroFormacion").val() != "") {
                Agregar();
            }else{
                alert("Por favor selecione programa y centro");
            }
        }else{
            alert("Por favor llene todos los campos");
        }
        
    });

    $("#btnModificar").click(function() {

        if ($("#txtNumeroFicha").val() != "" && $("#txtPrograma").val() != "" && $("#txtCentro").val() != "") {
            if ($("#hidPrograma").val() != "" && $("#hidCentroFormacion").val() != "") {
                Modificar();
            }else{
                alert("Por favor selecione programa y centro");
            }
        }else{
            alert("Por favor llene todos los campos");
        }
        
    });
})

function Agregar(){
    $("#hidAccion").val("ADICIONAR");
    $.ajax({
        data: $("#frmFichas").serialize(), //datos que se van a enviar al ajax
        url: '../../controlador/administrador/fichas.C.php', //archivo php que recibe los datos 
        type: 'post', //método para enviar los datos
        dataType: 'json',//Recibe el array desde php
        cache: false,
        success:  function (respuesta) { //procesa y devuelve la respuesta
            console.log(respuesta); 
            alert(respuesta['respuesta']);
            location.reload();
        }
    });
}

function Modificar(){
    $("#hidAccion").val("MODIFICAR");
    $.ajax({
            data: $("#frmFichas").serialize(), //datos que se van a enviar al ajax
            url: '../../controlador/administrador/fichas.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if(respuesta['accion']=='MODIFICAR'){
                    alert(respuesta['respuesta']);
                    location.reload();
                }
            }
    });
}

function Consultar(id){
    var parametros={
        "hidAccion":"CONSULTAR",
        "hidFicha":id
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/administrador/fichas.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if(respuesta['accion']=='CONSULTAR'){

                    $("#hidFicha").val(respuesta.datos.id_ficha);
                    $("#txtNumeroFicha").val(respuesta.datos.numero_ficha);
                    $("#txtPrograma").val(respuesta.datos.nombre_programa);
                    $("#hidPrograma").val(respuesta.datos.id_programa);
                    $("#txtCentro").val(respuesta.datos.nombre_centro);
                    $("#hidCentroFormacion").val(respuesta.datos.id_centro);
            
                }
            }
    });
}

function Listar(){

    $(".otraFila").remove(); 
    $("#dataTable6 tbody").append(primeraFilaFic);
    var parametros={
        "hidAccion":"LISTAR"
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/administrador/fichas.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if (respuesta['accion']=='LISTAR') {
                    $.each(respuesta.listaFic, function (i, lista) {
                        $("#afic").html(lista[1]);
                        $("#bfic").html(lista[2]);
                        $("#cfic").html(lista[3]);
                        $("#btnEditar").attr("onclick","Consultar("+lista[0]+")");
                        $("#dataTable6 tbody").append($("#filafic").clone(true).attr("class","otraFila"));
                    });  
                    $(".primeraFilaFic").remove();
                }
                //Diseño de la tabla
                var t = $('#dataTable6').DataTable( {
                    "scrollY": 200,
                    "scrollX": true,
                    "columnDefs": [ {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    } ],
                    "order": [[ 1, 'asc' ]],
                    "language": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sSearch": "Buscar:",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                          "sFirst": "Primero",
                          "sLast": "Último",
                          "sNext": "Siguiente",
                          "sPrevious": "Anterior"
                        },
                        "oAria": {
                          "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        },
                        "buttons": {
                          "copy": "Copiar",
                          "colvis": "Visibilidad"
                        }
                    }
                } );
             
                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    } );
                } ).draw();
            }
    });
}