$(function() {
    //se carga el autocompleta del contratista
    $("#txtCompetencia").autocomplete({ 
        source:'../../busqueda/competencia.php',
        select:function(event, ui){
            $("#hidCompetencia").val(ui.item.id_competencia); 
        } 
    }); 
});

var primeraFila;

$(function () {
    primeraFila=$("#fila");
    
    $("#btnAgregar").click(function() {

        if ($("#txtNombre").val() != "" && $("#txtCompetencia").val() != "") {
            Agregar();
        }else{
            alert("Por favor llene todos los campos");
        }
        
    });

    $("#btnModificar").click(function() {

        if ($("#txtNombre").val() != "" && $("#txtMunicipio").val() != "") {
            if ($("#hidMunicipio").val() != "") {
                Modificar();
            }else{
                alert("Por favor selecione el municipio");
            }
        }else{
            alert("Por favor llene todos los campos");
        }
        
    });
})

function Agregar(){
    $("#hidAccion").val("ADICIONAR");
    $.ajax({
        data: $("#frmRap").serialize(), //datos que se van a enviar al ajax
        url: '../../controlador/administrador/resultadoAprendizaje.C.php', //archivo php que recibe los datos 
        type: 'post', //método para enviar los datos
        dataType: 'json',//Recibe el array desde php
        cache: false,
        success:  function (respuesta) { //procesa y devuelve la respuesta
            console.log(respuesta); 
            alert(respuesta['respuesta']);
            location.reload();
        }
    });
}

function Modificar(){
    $("#hidAccion").val("MODIFICAR");
    $.ajax({
            data: $("#frmRap").serialize(), //datos que se van a enviar al ajax
            url: '../../controlador/administrador/resultadoAprendizaje.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if(respuesta['accion']=='MODIFICAR'){
                    alert(respuesta['respuesta']);
                    location.reload();
                }
            }
    });
}

function Consultar(id){
    var parametros={
        "hidAccion":"CONSULTAR",
        "hidResultadoA":id
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/administrador/resultadoAprendizaje.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if(respuesta['accion']=='CONSULTAR'){

                    $("#txtNombre").val(respuesta.datos.nombre_rap);
                    $("#txtCompetencia").val(respuesta.datos.nombre_com);
                    $("#hidCompetencia").val(respuesta.datos.id_com);
                    $("#hidResultadoA").val(respuesta.datos.id_rap);
            
                }
            }
    });
}

function Listar(){

    $(".otraFila").remove(); 
    $("#tablaComp tbody").append(primeraFila);
    var parametros={
        "hidAccion":"LISTAR"
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/administrador/resultadoAprendizaje.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if (respuesta['accion']=='LISTAR') {
                    $.each(respuesta.listaCom, function (i, lista) {
                        $("#aRap").html(lista[1]);
                        $("#bRap").html(lista[2]);
                        $("#btnEditar").attr("onclick","Consultar("+lista[0]+")");
                        $("#tablaComp tbody").append($("#fila").clone(true).attr("class","otrafila"));
                    });  
                    $(".primeraFila").remove();
                }
                //Diseño de la tabla
                var t = $('#tablaComp').DataTable( {
                    "lengthMenu": [[3, 10, 25, 50, -1], [3, 10, 25, 50, "All"]],
                    "columnDefs": [ {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    } ],
                    "order": [[ 1, 'asc' ]],
                    "language": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sSearch": "Buscar:",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                          "sFirst": "Primero",
                          "sLast": "Último",
                          "sNext": "Siguiente",
                          "sPrevious": "Anterior"
                        },
                        "oAria": {
                          "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        },
                        "buttons": {
                          "copy": "Copiar",
                          "colvis": "Visibilidad"
                        }
                    }
                } );
             
                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    } );
                } ).draw();
            }
    });
}