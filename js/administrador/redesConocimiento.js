var primeraFilaRed;

$(function () {
    primeraFilaRed=$("#filaRed");
    
    $("#btnAgregar").click(function() {

        if ($("#txtNombre").val() != "") {
            Agregar();
        }else{
            alert("Por favor llene todos los campos");
        }
        
    });

    $("#btnModificar").click(function() {

        if ($("#txtNombre").val() != "") {
            if ($("#hidRedConocimiento").val() != "") {
                Modificar();
            }else{
                alert("Por favor consulte primero");
            }
        }else{
            alert("Por favor llene todos los campos");
        }
        
    });
})

function Agregar(){
    $("#hidAccion").val("ADICIONAR");
    $.ajax({
        data: $("#frmRed").serialize(), //datos que se van a enviar al ajax
        url: '../../controlador/administrador/redesConocimiento.C.php', //archivo php que recibe los datos 
        type: 'post', //método para enviar los datos
        dataType: 'json',//Recibe el array desde php
        cache: false,
        success:  function (respuesta) { //procesa y devuelve la respuesta
            console.log(respuesta); 
            alert(respuesta['respuesta']);
            location.reload();
        }
    });
}

function Modificar(){
    $("#hidAccion").val("MODIFICAR");
    $.ajax({
            data: $("#frmRed").serialize(), //datos que se van a enviar al ajax
            url: '../../controlador/administrador/redesConocimiento.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if(respuesta['accion']=='MODIFICAR'){
                    alert(respuesta['respuesta']);
                    location.reload();
                }
            }
    });
}

function Consultar(id){
    var parametros={
        "hidAccion":"CONSULTAR",
        "hidRedConocimiento":id
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/administrador/redesConocimiento.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if(respuesta['accion']=='CONSULTAR'){

                    $("#hidRedConocimiento").val(respuesta.datos.id_red);
                    $("#txtNombre").val(respuesta.datos.nombre_red);
            
                }
            }
    });
}

function Listar(){

    $(".otraFila").remove(); 
    $("#dataTable8 tbody").append(primeraFilaRed);
    var parametros={
        "hidAccion":"LISTAR"
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/administrador/redesConocimiento.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if (respuesta['accion']=='LISTAR') {
                    $.each(respuesta.listaRed, function (i, lista) {
                        $("#ared").html(lista[1]);
                        $("#btnEditar").attr("onclick","Consultar("+lista[0]+")");
                        $("#dataTable8 tbody").append($("#filaRed").clone(true).attr("class","otraFila"));
                    });  
                    $(".primeraFilaRed").remove();
                }
                var t = $('#dataTable8').DataTable( {
                    "scrollY": 200,
                    "scrollX": true,
                    "columnDefs": [ {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    } ],
                    "order": [[ 1, 'asc' ]],
                    "language": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sSearch": "Buscar:",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                          "sFirst": "Primero",
                          "sLast": "Último",
                          "sNext": "Siguiente",
                          "sPrevious": "Anterior"
                        },
                        "oAria": {
                          "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        },
                        "buttons": {
                          "copy": "Copiar",
                          "colvis": "Visibilidad"
                        }
                    }
                } );
             
                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    } );
                } ).draw();

            }
    });
}