$(function() {
    //se carga el autocompleta del contratista
    $("#txtInstructor").autocomplete({ 
        source:'../../busqueda/instructor.php',
        select:function(event, ui){
            $("#hidInstructor").val(ui.item.id_instructor); 
            $("#txtDocumento").val(ui.item.documento); 
        } 
    }); 
});

$(function() {
    //se carga el autocompleta del contratista
    $("#txtDocumento").autocomplete({ 
        source:'../../busqueda/instructorPorDocumento.php',
        select:function(event, ui){
            $("#hidInstructor").val(ui.item.id_instructor); 
            $("#txtInstructor").val(ui.item.nombre_persona); 
        } 
    }); 
});

$(function() {
    //se carga el autocompleta del contratista
    $("#txtFicha").autocomplete({ 
        source:'../../busqueda/ficha.php',
        select:function(event, ui){
            $("#hidFicha").val(ui.item.id_ficha); 
            $("#txtRed").val(ui.item.nombre_red); 
            $("#txtPrograma").val(ui.item.nombre_programa); 
        } 
    }); 
});


var primeraFilaFic;

$(function () {
    primeraFilaFic=$("#filaFic");
     
    $("#btnAgregar").click(function() {

        if ($("#hidInstructor").val() != "" && $("#hidFicha").val() != "" && $("#cmbEstado").val() != "") {
            Agregar();
        }else{
            alert("Por favor llene todos los campos");
        }
        
    });

    $("#btnModificar").click(function() {

        if ($("#hidInstructor").val() != "" && $("#hidFicha").val() != "" && $("#cmbEstado").val() != "") {
            Modificar();
        }else{
            alert("Por favor llene todos los campos");
        }
        
    });
})


function Agregar(){
    $("#hidAccion").val("ADICIONAR");
    $.ajax({
        data: $("#frmInsFicha").serialize(), //datos que se van a enviar al ajax
        url: '../../controlador/administrador/instructoresFicha.C.php', //archivo php que recibe los datos 
        type: 'post', //método para enviar los datos
        dataType: 'json',//Recibe el array desde php
        cache: false,
        success:  function (respuesta) { //procesa y devuelve la respuesta
            console.log(respuesta); 
            alert(respuesta['respuesta']);
            location.reload();
        }
    });
} 

function Modificar(){
    $("#hidAccion").val("MODIFICAR");
    $.ajax({
            data: $("#frmInsFicha").serialize(), //datos que se van a enviar al ajax
            url: '../../controlador/administrador/instructoresFicha.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if(respuesta['accion']=='MODIFICAR'){
                    alert(respuesta['respuesta']);
                    location.reload();
                }
            }
    });
} 

function Consultar(id){
    var parametros={
        "hidAccion":"CONSULTAR",
        "hidInstructorFicha":id
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/administrador/instructoresFicha.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if(respuesta['accion']=='CONSULTAR'){

                    $("#hidInstructorFicha").val(respuesta.datos.id_insFic);
                    $("#hidInstructor").val(respuesta.datos.id_instructor);
                    $("#hidFicha").val(respuesta.datos.id_ficha);
                    $("#txtInstructor").val(respuesta.datos.nombre_persona);
                    $("#txtDocumento").val(respuesta.datos.documento);
                    $("#txtFicha").val(respuesta.datos.codigo_ficha);
                    $("#txtRed").val(respuesta.datos.nombre_red);
                    $("#cmbEstado").val(respuesta.datos.estado);
                    $("#txtPrograma").val(respuesta.datos.nombre_programa);
            
                }
            }
    });
}

function Listar(){

    $(".otraFila").remove(); 
    $("#dataTable12 tbody").append(primeraFilaFic);
    var parametros={
        "hidAccion":"LISTAR"
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/administrador/instructoresFicha.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if (respuesta['accion']=='LISTAR') {
                    $.each(respuesta.listaInsFic, function (i, lista) {
                        $("#afic").html(lista[2]);
                        $("#bfic").html(lista[3]);
                        $("#cfic").html(lista[1]);
                        $("#dfic").html(lista[4]);
                        $("#btnEditarInsFic").attr("onclick","Consultar("+lista[0]+")");
                        $("#dataTable12 tbody").append($("#filaFic").clone(true).attr("class","otraFila"));
                    });  
                    $(".primeraFilaFic").remove();
                }
                //Diseño de la tabla
                var t = $('#dataTable12').DataTable( {
                    "lengthMenu": [[3, 10, 25, 50, -1], [3, 10, 25, 50, "All"]],
                    "columnDefs": [ {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    } ],
                    "order": [[ 1, 'asc' ]],
                    "language": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sSearch": "Buscar:",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                          "sFirst": "Primero",
                          "sLast": "Último",
                          "sNext": "Siguiente",
                          "sPrevious": "Anterior"
                        },
                        "oAria": {
                          "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        },
                        "buttons": {
                          "copy": "Copiar",
                          "colvis": "Visibilidad"
                        }
                    }
                } );
             
                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    } );
                } ).draw();
            }
    });
}
