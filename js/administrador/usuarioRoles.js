$(function() {
    //se carga el autocompleta del contratista
    $("#txtUsurio").autocomplete({ 
        source:'../../busqueda/usuarios.php',
        select:function(event, ui){
            $("#hidUsuario").val(ui.item.id_usuario); 
            $("#txtPersona").val(ui.item.nombre_persona); 
        } 
    }); 
}); 

var primeraFilaUsu;

$(function () {
    primeraFilaUsu=$("#filaUsu");
     
    $("#btnAgregar").click(function() {

        if ($("#txtUsurio").val() != "" && $("#cmbRol").val() != "" && $("#cmbEstado").val() != "") {
            Agregar();
        }else{
            alert("Por favor llene todos los campos");
        }
        
    });

    $("#btnModificar").click(function() {

        if ($("#txtUsurio").val() != "" && $("#cmbRol").val() != "" && $("#cmbEstado").val() != "") {
            Modificar();
        }else{
            alert("Por favor llene todos los campos");
        }
        
    });
})

function Agregar(){
    $("#hidAccion").val("ADICIONAR");
    $.ajax({
        data: $("#frmUsuarioRol").serialize(), //datos que se van a enviar al ajax
        url: '../../controlador/administrador/usuarioRoles.C.php', //archivo php que recibe los datos 
        type: 'post', //método para enviar los datos
        dataType: 'json',//Recibe el array desde php
        cache: false,
        success:  function (respuesta) { //procesa y devuelve la respuesta
            console.log(respuesta); 
            alert(respuesta['respuesta']);
            location.reload();
        }
    });
}

function Modificar(){
    $("#hidAccion").val("MODIFICAR");
    $.ajax({
            data: $("#frmUsuarioRol").serialize(), //datos que se van a enviar al ajax
            url: '../../controlador/administrador/usuarioRoles.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if(respuesta['accion']=='MODIFICAR'){
                    alert(respuesta['respuesta']);
                    location.reload();
                }
            }
    });
}

function Consultar(id){
    var parametros={
        "hidAccion":"CONSULTAR",
        "hidUsuarioRol":id
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/administrador/usuarioRoles.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if(respuesta['accion']=='CONSULTAR'){

                    $("#hidUsuarioRol").val(respuesta.datos.id_usuario_rol);
                    $("#txtUsurio").val(respuesta.datos.nombre_Usuario);
                    $("#txtPersona").val(respuesta.datos.nombre_persona);
                    $("#cmbRol").val(respuesta.datos.id_rol);
                    $("#hidUsuario").val(respuesta.datos.id_usuario);
                    $("#cmbEstado").val(respuesta.datos.estado);
            
                }
            }
    });
}

function Listar(){

    $(".otraFila").remove(); 
    $("#dataTable11 tbody").append(primeraFilaUsu);
    var parametros={
        "hidAccion":"LISTAR"
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/administrador/usuarioRoles.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if (respuesta['accion']=='LISTAR') {
                    $.each(respuesta.listaUsuRol, function (i, lista) {
                        $("#ausu").html(lista[1]);
                        $("#busu").html(lista[2]);
                        $("#cusu").html(lista[3]);
                        $("#dusu").html(lista[4]);
                        $("#btnEditarUsuarioRol").attr("onclick","Consultar("+lista[0]+")");
                        $("#dataTable11 tbody").append($("#filaUsu").clone(true).attr("class","otraFila"));
                    });  
                    $(".primeraFilaUsu").remove();
                }
                //Diseño de la tabla
                var t = $('#dataTable11').DataTable( {
                    "lengthMenu": [[3, 10, 25, 50, -1], [3, 10, 25, 50, "All"]],
                    "columnDefs": [ {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    } ],
                    "order": [[ 1, 'asc' ]],
                    "language": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sSearch": "Buscar:",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                          "sFirst": "Primero",
                          "sLast": "Último",
                          "sNext": "Siguiente",
                          "sPrevious": "Anterior"
                        },
                        "oAria": {
                          "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        },
                        "buttons": {
                          "copy": "Copiar",
                          "colvis": "Visibilidad"
                        }
                    }
                } );
             
                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    } );
                } ).draw();
            }
    });
}

function ListarRol(){
    var parametros={
        "hidAccion":"LISTARROL",
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/administrador/personas.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if (respuesta['accion']=='LISTAR') {
                    $.each(respuesta.listaRol, function (i, lista) {
                        $('#cmbRol').append(
                            $('<option>',{
                                value: lista[0],
                                text: lista[1]
                            })
                        );  
                    });  
                }
            }
    });
}