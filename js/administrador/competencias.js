$(function() {
    //se carga el autocompleta del contratista
    $("#txtPrograma").autocomplete({ 
        source:'../../busqueda/programasFormacion.php',
        select:function(event, ui){
            $("#hidProgramaFormacion").val(ui.item.id_programa); 
        } 
    }); 
});

var primeraFilacom;

$(function () {
    primeraFilacom=$("#filacom");
    
    $("#btnAgregar").click(function() {

        if ($("#txtCodigo").val() != "" && $("#txtNombre").val() != "" && $("#txtPrograma").val() != "") {
            if ($("#hidProgramaFormacion").val() != "") {
                Agregar();
            }else{
                alert("Por favor selecione el municipio");
            }
        }else{
            alert("Por favor llene todos los campos");
        }
        
    });

    $("#btnModificar").click(function() {

        if ($("#txtCodigo").val() != "" && $("#txtNombre").val() != "" && $("#txtPrograma").val() != "") {
            if ($("#hidProgramaFormacion").val() != "") {
                Modificar();
            }else{
                alert("Por favor selecione el municipio");
            }
        }else{
            alert("Por favor llene todos los campos");
        }
        
    });
})

function Agregar(){
    $("#hidAccion").val("ADICIONAR");
    $.ajax({
        data: $("#frmCompetencia").serialize(), //datos que se van a enviar al ajax
        url: '../../controlador/administrador/competencias.C.php', //archivo php que recibe los datos 
        type: 'post', //método para enviar los datos
        dataType: 'json',//Recibe el array desde php
        cache: false,
        success:  function (respuesta) { //procesa y devuelve la respuesta
            console.log(respuesta); 
            alert(respuesta['respuesta']);
            location.reload();
        }
    });
}

function Modificar(){
    $("#hidAccion").val("MODIFICAR");
    $.ajax({
            data: $("#frmCompetencia").serialize(), //datos que se van a enviar al ajax
            url: '../../controlador/administrador/competencias.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if(respuesta['accion']=='MODIFICAR'){
                    alert(respuesta['respuesta']);
                    location.reload();
                }
            }
    });
}

function Consultar(id){
    var parametros={
        "hidAccion":"CONSULTAR",
        "hidCompetencia":id
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/administrador/competencias.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if(respuesta['accion']=='CONSULTAR'){

                    $("#hidCompetencia").val(respuesta.datos.id_competencia);
                    $("#txtCodigo").val(respuesta.datos.codigo_competencia);
                    $("#txtNombre").val(respuesta.datos.nombre_competencia);
                    $("#hidProgramaFormacion").val(respuesta.datos.id_programa);
                    $("#txtPrograma").val(respuesta.datos.nombre_programa);
            
                }
            }
    });
}

function Listar(){

    $(".otraFila").remove(); 
    $("#dataTable5 tbody").append(primeraFilacom);
    var parametros={
        "hidAccion":"LISTAR"
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/administrador/competencias.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if (respuesta['accion']=='LISTAR') {
                    $.each(respuesta.listaCom, function (i, lista) {
                        $("#acom").html(lista[1]);
                        $("#bcom").html(lista[2]);
                        $("#ccom").html(lista[3]);
                        $("#btnEditar").attr("onclick","Consultar("+lista[0]+")");
                        $("#dataTable5 tbody").append($("#filacom").clone(true).attr("class","otraFila"));
                    });  
                    $(".primeraFilacom").remove();
                }
                //Diseño de la tabla
                var t = $('#dataTable5').DataTable( {
                    "scrollY": 200,
                    "scrollX": true,
                    "columnDefs": [ {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    } ],
                    "order": [[ 1, 'asc' ]],
                    "language": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sSearch": "Buscar:",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                          "sFirst": "Primero",
                          "sLast": "Último",
                          "sNext": "Siguiente",
                          "sPrevious": "Anterior"
                        },
                        "oAria": {
                          "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        },
                        "buttons": {
                          "copy": "Copiar",
                          "colvis": "Visibilidad"
                        }
                    }
                } );
             
                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    } );
                } ).draw();
            }
    });
}