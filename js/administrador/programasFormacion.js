$(function() {
    //se carga el autocompleta del contratista
    $("#txtRed").autocomplete({ 
        source:'../../busqueda/redesConocimiento.php',
        select:function(event, ui){
            $("#hidRedConocimiento").val(ui.item.id_red); 
        } 
    }); 
});

var primeraFilapro;

$(function () {
    primeraFilapro=$("#filapro");
    
    $("#btnAgregar").click(function() {

        if ($("#txtCodigo").val() != "" && $("#txtNombre").val() != "" && $("#txtRed").val() != "") {
            if ($("#hidRedConocimiento").val() != "") {
                Agregar();
            }else{
                alert("Por favor selecione la red de conocimiento");
            }
        }else{
            alert("Por favor llene todos los campos");
        }
        
    });

    $("#btnModificar").click(function() {

        if ($("#txtCodigo").val() != "" && $("#txtNombre").val() != "" && $("#txtRed").val() != "") {
            if ($("#hidProgramaFormacion").val() != "" && $("#hidRedConocimiento").val() != "") {
                Modificar();
            }else{
                alert("Por favor selecione Programa y la Red de conocimiento");
            }
        }else{
            alert("Por favor llene todos los campos");
        }
        
    });
})

function Agregar(){
    $("#hidAccion").val("ADICIONAR");
    $.ajax({
        data: $("#frmProgramaFormacion").serialize(), //datos que se van a enviar al ajax
        url: '../../controlador/administrador/programasFormacion.C.php', //archivo php que recibe los datos 
        type: 'post', //método para enviar los datos
        dataType: 'json',//Recibe el array desde php
        cache: false,
        success:  function (respuesta) { //procesa y devuelve la respuesta
            console.log(respuesta); 
            alert(respuesta['respuesta']);
            location.reload();
        }
    });
}

function Modificar(){
    $("#hidAccion").val("MODIFICAR");
    $.ajax({
            data: $("#frmProgramaFormacion").serialize(), //datos que se van a enviar al ajax
            url: '../../controlador/administrador/programasFormacion.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if(respuesta['accion']=='MODIFICAR'){
                    alert(respuesta['respuesta']);
                    location.reload();
                }
            }
    });
}

function Consultar(id){
    var parametros={
        "hidAccion":"CONSULTAR",
        "hidProgramaFormacion":id
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/administrador/programasFormacion.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if(respuesta['accion']=='CONSULTAR'){

                    $("#hidProgramaFormacion").val(respuesta.datos.id_programa);
                    $("#txtCodigo").val(respuesta.datos.codigo_programa);
                    $("#txtNombre").val(respuesta.datos.nombre_programa);
                    $("#txtRed").val(respuesta.datos.nombre_red);
                    $("#hidRedConocimiento").val(respuesta.datos.id_red);
            
                }
            }
    });
}

function Listar(){

    $(".otraFila").remove(); 
    $("#dataTable7 tbody").append(primeraFilapro);
    var parametros={
        "hidAccion":"LISTAR"
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/administrador/programasFormacion.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if (respuesta['accion']=='LISTAR') {
                    $.each(respuesta.listaPro, function (i, lista) {
                        $("#apro").html(lista[1]);
                        $("#bpro").html(lista[2]);
                        $("#cpro").html(lista[3]);
                        $("#btnEditar").attr("onclick","Consultar("+lista[0]+")");
                        $("#dataTable7 tbody").append($("#filapro").clone(true).attr("class","otraFila"));
                    });  
                    $(".primeraFilapro").remove();
                }
                //Diseño de la tabla
                var t = $('#dataTable7').DataTable( {
                    "scrollY": 200,
                    "scrollX": true,
                    "columnDefs": [ {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    } ],
                    "order": [[ 1, 'asc' ]],
                    "language": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sSearch": "Buscar:",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                          "sFirst": "Primero",
                          "sLast": "Último",
                          "sNext": "Siguiente",
                          "sPrevious": "Anterior"
                        },
                        "oAria": {
                          "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        },
                        "buttons": {
                          "copy": "Copiar",
                          "colvis": "Visibilidad"
                        }
                    }
                } );
             
                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    } );
                } ).draw(); 
            }
    });
}