function login(){
    $("#accion").val("CONSULTAR");
    $.ajax({
        data: $("#frmLogin").serialize(), //datos que se van a enviar al ajax
        url: '../controlador/login.C.php', //archivo php que recibe los datos 
        type: 'post', //método para enviar los datos
        dataType: 'json',//Recibe el array desde php
        cache: false,
        success:  function (respuesta) { //procesa y devuelve la respuesta
            console.log(respuesta);
            alert(respuesta['respuesta']);
            if(respuesta['accion']=='CONSULTAR' && respuesta['numeroRegistros']==1){
                if (respuesta['rol']=='Administrador') { 
                    window.location="../vista/administrador/";
                } else {
                    window.location="../vista/usuario/";
                }
            }else{
                if (respuesta['accion']=='CONSULTAR') {
                    location.reload();
                }
            }
        },
        error: function (respuesta) {
            console.log(respuesta); 
            alert(respuesta['respuesta']);
        }
    });
}