var primeraFilao;
var primeraFilamo;
$(function () {
    primeraFilao=$("#filao"); 
    primeraFilamo=$("#filamo");
});

function Consultar(id){
    var parametros={
        "hidAccion":"CONSULTAR", 
        "hidIdGuia":id
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/guia.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if(respuesta['accion']=='CONSULTAR'){

                    $("#hidIdGuia").val(respuesta.datos.id_guias_aprendizaje_pk);
                    ListarMaterial();
            
                }
            }
    });
} 

function Listar(){
    $(".otraFilao").remove(); 
    $("#dataTable2 tbody").append(primeraFilao);
    var parametros={
        "hidAccion":"LISTAR"
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax 
            url: '../../controlador/guia.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if (respuesta['accion']=='LISTAR') {
                    $.each(respuesta.listagui, function (i, lista) {
                        if ($("#hidIdSesion").val() != lista[8]) {
                            $("#aguiao").html(lista[1]);
                            $("#bguiao").html(lista[2]);
                            $("#cguiao").html(lista[3]);
                            $("#dguiao").html(lista[4]);
                            $("#aArchivoo").attr("href","../"+lista[5]+"");
                            $("#btnVer").attr("onclick","Consultar("+lista[0]+")");
                            $("#dataTable2 tbody").append($("#filao").clone(true).attr("class","otraFilao"));
                        }
                    });  
                    $(".primeraFilao").remove();
                }
                var t = $('#dataTable2').DataTable( {
                    "scrollY": 200,
                    "scrollX": true,
                    "columnDefs": [ {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    } ],
                    "order": [[ 1, 'asc' ]],
                    "language": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sSearch": "Buscar:",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                          "sFirst": "Primero",
                          "sLast": "Último",
                          "sNext": "Siguiente",
                          "sPrevious": "Anterior"
                        },
                        "oAria": {
                          "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        },
                        "buttons": {
                          "copy": "Copiar",
                          "colvis": "Visibilidad"
                        }
                    }
                } );
             
                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    } );
                } ).draw();
            }
    });
}

function ListarMaterial(){
     
    $(".otraFilamo").remove(); 
    $("#dataTable3 tbody").append(primeraFilamo);
    var parametros={
        "hidAccion":"LISTAR"
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/materialApoyo.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if (respuesta['accion']=='LISTAR') {
                    $.each(respuesta.lista, function (i, lista) {
                        if ($("#hidIdGuia").val()==lista[5]) {
                            $("#aguiamo").html(lista[1]);
                            $("#bguiamo").html(lista[3]);
                            $("#cguiamo").html(lista[2]);
                            $("#aArchivomo").attr("href","../"+lista[4]+"");
                            $("#dataTable3 tbody").append($("#filamo").clone(true).attr("class","otraFilamo"));
                        }
                    });  
                    $(".primeraFilamo").remove();
                }
                //Diseño de la tabla
                var t = $('#dataTable3').DataTable( {
                    "lengthMenu": [[3, 10, 25, 50, -1], [3, 10, 25, 50, "All"]],
                    "columnDefs": [ {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    } ],
                    "order": [[ 1, 'asc' ]],
                    "language": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sSearch": "Buscar:",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                          "sFirst": "Primero",
                          "sLast": "Último",
                          "sNext": "Siguiente",
                          "sPrevious": "Anterior"
                        },
                        "oAria": {
                          "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        },
                        "buttons": {
                          "copy": "Copiar",
                          "colvis": "Visibilidad"
                        }
                    }
                } );
             
                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    } );
                } ).draw();
            }
    });
}

function ConsultarInstructorRed(){
    var parametros={
        "hidAccion":"CONSULTARREDINSTRUCTOR", 
        "hidPersona":$("#hidPersona").val()
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax
            url: '../../controlador/guia.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if(respuesta['accion']=='CONSULTAR'){

                    $("#hidRedC").val(respuesta.datos.id_red);
                    $("#hidIns").val(respuesta.datos.id_instructor);
            
                }
            }
    });
}

function ListaGuiaAp(){
    ConsultarInstructorRed();
    $(".otraFilao").remove(); 
    $("#dataTable2 tbody").append(primeraFilao);
    var parametros={
        "hidAccion":"LISTAR"
    }
    $.ajax({
            data: parametros, //datos que se van a enviar al ajax 
            url: '../../controlador/guia.C.php', //archivo php que recibe los datos 
            type: 'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
            cache: false,
            success:  function (respuesta) { //procesa y devuelve la respuesta
                console.log(respuesta); 
                if (respuesta['accion']=='LISTAR') {
                    $.each(respuesta.listagui, function (i, lista) {
                        if ($("#hidRedC").val() == lista[9]) {
                            $("#aguiao").html(lista[1]);
                            $("#bguiao").html(lista[2]);
                            $("#cguiao").html(lista[3]);
                            $("#dguiao").html(lista[4]);
                            $("#aArchivoo").attr("href","../"+lista[5]+"");
                            $("#btnVer").attr("onclick","Consultar("+lista[0]+")");
                            $("#dataTable2 tbody").append($("#filao").clone(true).attr("class","otraFilao"));
                        }
                    });  
                    $(".primeraFilao").remove();
                }
                var t = $('#dataTable2').DataTable( {
                    "scrollY": 200,
                    "scrollX": true,
                    "columnDefs": [ {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    } ],
                    "order": [[ 1, 'asc' ]],
                    "language": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sSearch": "Buscar:",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                          "sFirst": "Primero",
                          "sLast": "Último",
                          "sNext": "Siguiente",
                          "sPrevious": "Anterior"
                        },
                        "oAria": {
                          "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        },
                        "buttons": {
                          "copy": "Copiar",
                          "colvis": "Visibilidad"
                        }
                    }
                } );
             
                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    } );
                } ).draw();
            }
    });
}