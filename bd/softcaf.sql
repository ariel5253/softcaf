-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-06-2021 a las 07:36:52
-- Versión del servidor: 8.0.24
-- Versión de PHP: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `softcaft`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `agregar_guia` (IN `Nom` VARCHAR(100), IN `Palabra` VARCHAR(100), IN `Fic` INT, IN `Com` INT, IN `Url` VARCHAR(200), IN `Id_usuario_creacion` INT, IN `Id_usuario_modificacion` INT)  BEGIN
	INSERT INTO guias_aprendizaje(nombre
						,palabrasClave
						,ficha
						,competencia
                        ,url
						,fecha_creacion
						,fecha_actualizacion
						,id_usuario_creacion
						,id_usuario_actualizacion)
    VALUES (Nom
			,Palabra
            ,Fic
            ,Com
            ,Url
            ,curdate()
            ,curdate()
            ,Id_usuario_creacion
            ,Id_usuario_modificacion
            );
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `agregar_material` (IN `Nom` VARCHAR(100), IN `Palabra` VARCHAR(100), IN `Url` VARCHAR(200), IN `Gui` INT, IN `Id_usuario_creacion` INT, IN `Id_usuario_modificacion` INT)  BEGIN
	INSERT INTO materiales_apoyo(nombre
						,guia_aprendizaje
						,palabras_clave
                        ,url
						,fecha_creacion
						,fecha_actualizacion
						,id_usuario_creacion
						,id_usuario_actualizacion)
    VALUES (Nom
			,Gui
			,Palabra
            ,Url
            ,curdate()
            ,curdate()
            ,Id_usuario_creacion
            ,Id_usuario_modificacion
            );
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `consultar_guia` (IN `Id_guia` INT)  BEGIN
		if Id_guia <> '' then
			select * from guias_aprendizaje where id_guias_aprendizaje_pk = Id_guia;
        end if;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `consultar_material` (IN `Id_material` INT)  BEGIN
		if Id_material <> '' then
			select * from materiales_apoyo where id_materiales_apoyo_pk = Id_material;
        end if;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `consultar_ultimo_id_guia` ()  BEGIN
	SELECT MAX(id_guias_aprendizaje_pk)+1 AS id FROM guias_aprendizaje;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `consultar_usuario` (IN `Usu` VARCHAR(50), IN `Pas` VARCHAR(50))  BEGIN
	select
		usu.id_usuario_pk AS id_usuario
		,usu.usuario AS nombre_usuario
		,concat(per.nombres,+" ",per.apellido) AS nombre_persona
		,rol.nombre AS rol_usuario
        ,ins.id_instructor_pk AS id_instructor
	from
		personas AS per
		inner join usuarios AS usu ON usu.persona = per.id_persona_pk
		inner join usuario_roles AS usr ON usr.usuarios = usu.id_usuario_pk
		inner join roles AS rol ON rol.id_rol_pk = usr.roles
        inner join instructores AS ins ON ins.persona = per.id_persona_pk
	where usu.usuario like Usu && usu.password like Pas;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listar_competencia` (IN `Fic` INT)  BEGIN
			SELECT
		competencias.*
	FROM
	fichas AS fi
	INNER JOIN programas_formacion ON programas_formacion.id_programas_formacion_pk = fi.programa
	INNER JOIN competencias ON competencias.programa = programas_formacion.id_programas_formacion_pk
	WHERE fi.id_ficha_pk = Fic;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listar_ficha` (IN `Id_ins` INT)  BEGIN
	select 
		infic.ficha As id_ficha
		,ficha.codigo As codigo_ficha
	from 
		instructores_fichas AS infic
		inner join fichas AS ficha ON ficha.id_ficha_pk = infic.ficha
		inner join instructores AS instr ON instr.id_instructor_pk = infic.instructor
	where infic.instructor = Id_ins;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listar_guia` ()  BEGIN
		select 
			gu.id_guias_aprendizaje_pk
            ,gu.nombre AS nombre_guia
            ,gu.palabrasClave AS palabras_guia
            ,fi.codigo AS codigo_ficha
            ,co.nombre AS nombre_competencia
            ,gu.url AS url_guia
            ,gu.ficha AS id_ficha
            ,gu.competencia AS id_competencia
            ,gu.id_usuario_creacion
        from 
        guias_aprendizaje AS gu
        inner join fichas AS fi ON fi.id_ficha_pk = gu.ficha
        inner join competencias AS co ON co.id_competencia_pk = gu.competencia;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listar_material` ()  BEGIN
		select 
			ma.id_materiales_apoyo_pk
            ,ma.nombre AS nombre_material
            ,gui.nombre AS nombre_guia
            ,ma.palabras_clave AS palabras_material
            ,ma.url AS url_material
            ,ma.guia_aprendizaje AS id_guia
            ,ma.id_usuario_creacion
            ,gui.id_usuario_creacion
        from 
        materiales_apoyo AS ma
        inner join guias_aprendizaje AS gui ON gui.id_guias_aprendizaje_pk = ma.guia_aprendizaje;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `modificar_guia` (IN `Id_gui` INT, IN `Nom` VARCHAR(200), IN `Pal` VARCHAR(200), IN `Fic` INT, IN `Com` INT, IN `Url` VARCHAR(200), IN `Id_usuario_actualizacion` INT)  BEGIN
	UPDATE guias_aprendizaje SET nombre=Nom
								,palabrasClave=Pal
								,ficha=Fic
								,competencia=Com
								,url=Url
								,fecha_actualizacion=curdate()
								,id_usuario_actualizacion=Id_usuario_actualizacion
    WHERE id_guias_aprendizaje_pk = Id_gui;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `modificar_material` (IN `Id_mat` INT, IN `Nom` VARCHAR(200), IN `Gui` INT, IN `Pal` VARCHAR(200), IN `Url` VARCHAR(200), IN `Id_usuario_actualizacion` INT)  BEGIN
	UPDATE materiales_apoyo SET nombre=Nom
								,guia_aprendizaje=Gui
								,palabras_clave=Pal
								,url=Url
								,fecha_actualizacion=curdate()
								,id_usuario_actualizacion=Id_usuario_actualizacion
    WHERE id_materiales_apoyo_pk = Id_mat;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `centros_formacion`
--

CREATE TABLE `centros_formacion` (
  `id_centros_formacion_pk` int NOT NULL,
  `nombre` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `municipio` int NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_actualizacion` datetime NOT NULL,
  `id_usuario_creacion` int NOT NULL,
  `id_usuario_actualizacion` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `centros_formacion`
--

INSERT INTO `centros_formacion` (`id_centros_formacion_pk`, `nombre`, `municipio`, `fecha_creacion`, `fecha_actualizacion`, `id_usuario_creacion`, `id_usuario_actualizacion`) VALUES
(1, 'CENTRO AGROEMPRESARIAL Y DESARROLLO PECUARIO DEL HUILA', 88, '2021-05-13 15:45:13', '2021-05-13 15:45:13', 1, 1),
(2, 'CENTRO DE DESARROLLO AGROEMPRESARIAL Y TURÍSTICO DEL HUILA', 109, '2021-05-13 15:49:06', '2021-05-13 15:49:06', 2, 2),
(3, 'CENTRO DE LA INDUSTRIA, LA EMPRESA Y LOS SERVICIOS​', 19, '2021-05-13 15:51:18', '2021-05-13 15:51:18', 3, 3),
(4, 'CENTRO DE GESTIÓN Y DESARROLLO SOSTENIBLE SURCOLOMBIANO', 55, '2021-05-13 15:53:15', '2021-05-13 15:53:15', 4, 4),
(5, 'CENTRO DE DESARROLLO AGROEMPRESARIAL Y TURÍSTICO DEL HUILA', 2, '2021-05-13 15:54:23', '2021-06-12 00:00:00', 5, 2),
(10, 'CENTRO DE DESARROLLO AGROEMPRESARIAL Y TURÍSTICO DEL HUILA', 55, '2021-06-12 00:00:00', '2021-06-12 00:00:00', 2, 4),
(11, 'CENTRO DE DESARROLLO AGROEMPRESARIAL Y TURÍSTICO DEL HUILA', 19, '2021-06-12 00:00:00', '2021-06-12 00:00:00', 2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `competencias`
--

CREATE TABLE `competencias` (
  `id_competencia_pk` int NOT NULL,
  `codigo` int NOT NULL,
  `nombre` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `programa` int NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_actualizacion` datetime NOT NULL,
  `id_usuario_creacion` int NOT NULL,
  `id_usuario_actualizacion` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `competencias`
--

INSERT INTO `competencias` (`id_competencia_pk`, `codigo`, `nombre`, `programa`, `fecha_creacion`, `fecha_actualizacion`, `id_usuario_creacion`, `id_usuario_actualizacion`) VALUES
(1, 240201530, 'RESULTADO DE APRENDIZAJE DE LA INDUCCIÒN', 1, '2021-05-13 16:15:35', '2021-05-13 16:15:35', 1, 1),
(2, 234215362, 'ESTRUCTURAR PROCESOS ADMINISTRATIVOS EN ENTIDADES RECREO DEPORTIVAS DE ACUERDO A LA LEGISLACION VIGENTE. ', 1, '2021-05-13 16:23:55', '2021-05-13 16:23:55', 2, 2),
(3, 245673621, 'PROCESAR,DILIGENCIAR Y OPERAR PROCESOS DE IMPORTACIÒN Y EXPORTACIÒN DE ACUERDO A LA NORMATIVIDAD.', 1, '2021-05-13 16:30:59', '2021-05-13 16:30:59', 3, 3),
(4, 243876908, 'PROCESAR,DILIGENCIAR Y OPERAR PROCESOS DE IMPORTACIÒN Y EXPORTACIÒN DE ACUERDO A LA NORMATIVIDAD.', 2, '2021-05-13 16:35:10', '2021-05-13 16:35:10', 4, 4),
(5, 123456, 'ADMINISTRAR LOS RECURSOS DEL ÀREA DE ACUERDO CON POLÌTICAS DE LA ORGANIZACIÒN', 1, '2021-05-13 16:46:49', '2021-06-12 00:00:00', 5, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamentos`
--

CREATE TABLE `departamentos` (
  `id_departamento_pk` int NOT NULL,
  `nombre` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_actualizacion` datetime NOT NULL,
  `id_usuario_creacion` int NOT NULL,
  `id_usuario_actualizacion` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `departamentos`
--

INSERT INTO `departamentos` (`id_departamento_pk`, `nombre`, `fecha_creacion`, `fecha_actualizacion`, `id_usuario_creacion`, `id_usuario_actualizacion`) VALUES
(1, 'AMAZONAS', '2021-05-11 23:43:11', '2021-05-11 23:43:11', 1, 1),
(2, 'ANTIOQUIA', '2021-05-11 23:44:31', '2021-05-11 23:44:31', 2, 2),
(3, 'ARAUCA', '2021-05-11 23:44:51', '2021-05-11 23:44:51', 3, 3),
(4, 'ATLANTICO', '2021-05-11 23:45:07', '2021-05-11 23:45:07', 4, 4),
(5, 'BOLIVAR', '2021-05-11 23:45:25', '2021-05-11 23:45:25', 5, 5),
(6, 'BOYACA', '2021-05-11 23:45:43', '2021-05-11 23:45:43', 6, 6),
(7, 'CALDAS', '2021-05-11 23:48:24', '2021-05-11 23:48:24', 7, 7),
(8, 'CAQUETA', '2021-05-11 23:48:36', '2021-05-11 23:48:36', 8, 8),
(9, 'CASANARE', '2021-05-11 23:48:48', '2021-05-11 23:48:48', 9, 9),
(10, 'CAUCA', '2021-05-11 23:49:02', '2021-05-11 23:49:02', 10, 10),
(11, 'CESAR', '2021-05-11 23:49:17', '2021-05-11 23:49:17', 11, 11),
(12, 'CHOCO', '2021-05-11 23:49:28', '2021-05-11 23:49:28', 12, 12),
(13, 'CORDOBA', '2021-05-11 23:49:41', '2021-05-11 23:49:41', 13, 13),
(14, 'CUNDINAMARCA', '2021-05-11 23:49:55', '2021-05-11 23:49:55', 15, 15),
(15, 'GUAINIA', '2021-05-11 23:50:13', '2021-05-11 23:50:13', 15, 15),
(16, 'GUAVIARE', '2021-05-11 23:50:30', '2021-05-11 23:50:30', 16, 16),
(17, 'HUILA', '2021-05-11 23:50:52', '2021-05-11 23:50:52', 17, 17),
(18, 'LA GUAJIRA', '2021-05-11 23:51:07', '2021-05-11 23:51:07', 18, 18),
(19, 'MAGDALENA', '2021-05-11 23:51:27', '2021-05-11 23:51:27', 19, 19),
(20, 'META', '2021-05-11 23:51:47', '2021-05-11 23:51:47', 20, 20),
(21, 'NARIÑO', '2021-05-11 23:52:05', '2021-05-11 23:52:05', 21, 21),
(22, 'NORTE DE SANTANDER', '2021-05-11 23:52:18', '2021-05-11 23:52:18', 22, 22),
(23, 'PUTUMAYO', '2021-05-11 23:52:37', '2021-05-11 23:52:37', 23, 23),
(24, 'QUINDIO', '2021-05-11 23:52:51', '2021-05-11 23:52:51', 24, 24),
(25, 'RISARALDA', '2021-05-11 23:53:11', '2021-05-11 23:53:11', 25, 25),
(26, 'SAN ANDRES Y PROVIDENCIA', '2021-05-11 23:53:22', '2021-05-11 23:53:22', 26, 26),
(27, 'SANTANDER', '2021-05-11 23:53:44', '2021-05-11 23:53:44', 27, 27),
(28, 'SUCRE', '2021-05-11 23:54:01', '2021-05-11 23:54:01', 28, 28),
(29, 'TOLIMA', '2021-05-11 23:54:15', '2021-05-11 23:54:15', 29, 29),
(30, 'VALLE DEL CAUCA', '2021-05-11 23:54:35', '2021-05-11 23:54:35', 30, 30),
(31, 'VAUPES', '2021-05-11 23:54:56', '2021-05-11 23:54:56', 31, 31),
(32, 'VICHADA', '2021-05-11 23:55:15', '2021-05-11 23:55:15', 32, 32);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fichas`
--

CREATE TABLE `fichas` (
  `id_ficha_pk` int NOT NULL,
  `codigo` int NOT NULL,
  `programa` int NOT NULL,
  `centro_formacion` int NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_actualizacion` datetime NOT NULL,
  `id_usuario_creacion` int NOT NULL,
  `id_usuario_actualizacion` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `fichas`
--

INSERT INTO `fichas` (`id_ficha_pk`, `codigo`, `programa`, `centro_formacion`, `fecha_creacion`, `fecha_actualizacion`, `id_usuario_creacion`, `id_usuario_actualizacion`) VALUES
(1, 2052637, 1, 3, '2021-05-13 16:48:59', '2021-05-13 16:48:59', 1, 1),
(2, 2435263, 2, 2, '2021-05-13 16:49:40', '2021-05-13 16:49:40', 2, 2),
(3, 2453684, 1, 3, '2021-05-13 16:50:14', '2021-05-13 16:50:14', 3, 3),
(4, 2453674, 2, 5, '2021-05-13 16:52:33', '2021-05-13 16:52:33', 4, 4),
(5, 2564785, 2, 4, '2021-05-13 16:53:43', '2021-05-13 16:53:43', 5, 5),
(6, 2052611, 2, 4, '2021-06-12 00:00:00', '2021-06-12 00:00:00', 2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `guias_aprendizaje`
--

CREATE TABLE `guias_aprendizaje` (
  `id_guias_aprendizaje_pk` int NOT NULL,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `palabrasClave` varchar(1000) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `ficha` int NOT NULL,
  `competencia` int NOT NULL,
  `url` varchar(200) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_actualizacion` datetime NOT NULL,
  `id_usuario_creacion` int NOT NULL,
  `id_usuario_actualizacion` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `guias_aprendizaje`
--

INSERT INTO `guias_aprendizaje` (`id_guias_aprendizaje_pk`, `nombre`, `palabrasClave`, `ficha`, `competencia`, `url`, `fecha_creacion`, `fecha_actualizacion`, `id_usuario_creacion`, `id_usuario_actualizacion`) VALUES
(231, 'Guia_36', 'js,xan', 3, 4, '../archivos/guias/Guia_36_231.pdf', '2021-06-04 00:00:00', '2021-06-04 00:00:00', 1, 1),
(232, 'Guia_26', 'Php,js', 3, 4, '../archivos/guias/Guia_26_232.pdf', '2021-06-04 00:00:00', '2021-06-04 00:00:00', 1, 1),
(233, 'Guia_5', 'js', 3, 4, '../archivos/guias/Guia_5_233.pdf', '2021-06-04 00:00:00', '2021-06-04 00:00:00', 1, 1),
(235, 'Gui33-22', 'jpd', 1, 1, '../archivos/guias/Gui33-22_235.pdf', '2021-06-04 00:00:00', '2021-06-04 00:00:00', 2, 2),
(236, 'Guia100', 'java', 2, 5, '../archivos/guias/Guia100_236.pdf', '2021-06-07 00:00:00', '2021-06-07 00:00:00', 3, 3),
(237, 'Guia_120', 'js-js', 2, 5, '../archivos/guias/Guia_120_237.pdf', '2021-06-07 00:00:00', '2021-06-10 00:00:00', 3, 3),
(238, 'Guia_200', 'java', 1, 1, '../archivos/guias/Guia_200_238.pdf', '2021-06-07 00:00:00', '2021-06-07 00:00:00', 3, 3),
(239, 'guia_500', 'java', 1, 1, '../archivos/guias/guia_500_239.pdf', '2021-06-09 00:00:00', '2021-06-09 00:00:00', 3, 3),
(240, 'guia_3212', 'java', 1, 1, '../archivos/guias/guia_3212_240.pdf', '2021-06-15 00:00:00', '2021-06-15 00:00:00', 3, 3),
(241, 'Guia_500', 'java', 1, 1, '../archivos/guias/Guia_500_241.pdf', '2021-06-15 00:00:00', '2021-06-15 00:00:00', 15, 15),
(242, 'Guia_321', 'java', 1, 1, '../archivos/guias/Guia_321_242.pdf', '2021-06-16 00:00:00', '2021-06-16 00:00:00', 28, 28),
(243, 'Guia322', 'php', 1, 2, '../archivos/guias/Guia322_243.pdf', '2021-06-16 00:00:00', '2021-06-16 00:00:00', 28, 28),
(244, 'Guia_400', 'js', 1, 2, '../archivos/guias/Guia_400_244.pdf', '2021-06-16 00:00:00', '2021-06-16 00:00:00', 28, 28);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instructores`
--

CREATE TABLE `instructores` (
  `id_instructor_pk` int NOT NULL,
  `persona` int NOT NULL,
  `red_conocimiento` int NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_actualizacion` datetime NOT NULL,
  `id_usuario_creacion` int NOT NULL,
  `id_usuario_actualizacion` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `instructores`
--

INSERT INTO `instructores` (`id_instructor_pk`, `persona`, `red_conocimiento`, `fecha_creacion`, `fecha_actualizacion`, `id_usuario_creacion`, `id_usuario_actualizacion`) VALUES
(14, 53, 5, '2021-06-16 00:00:00', '2021-06-16 00:00:00', 13, 28),
(15, 54, 19, '2021-06-16 00:00:00', '2021-06-16 00:00:00', 28, 28);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instructores_fichas`
--

CREATE TABLE `instructores_fichas` (
  `id_instructores_fichas_pk` int NOT NULL,
  `instructor` int NOT NULL,
  `ficha` int NOT NULL,
  `estado` enum('ACTIVO','INACTIVO') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_actualizacion` datetime NOT NULL,
  `id_usuario_creacion` int NOT NULL,
  `id_usuario_actualizacion` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `instructores_fichas`
--

INSERT INTO `instructores_fichas` (`id_instructores_fichas_pk`, `instructor`, `ficha`, `estado`, `fecha_creacion`, `fecha_actualizacion`, `id_usuario_creacion`, `id_usuario_actualizacion`) VALUES
(14, 14, 1, 'ACTIVO', '2021-06-16 00:00:00', '2021-06-16 00:00:00', 13, 13),
(15, 14, 4, 'ACTIVO', '2021-06-16 00:00:00', '2021-06-16 00:00:00', 28, 28);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materiales_apoyo`
--

CREATE TABLE `materiales_apoyo` (
  `id_materiales_apoyo_pk` int NOT NULL,
  `nombre` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `guia_aprendizaje` int NOT NULL,
  `palabras_clave` varchar(1000) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `url` varchar(200) COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_actualizacion` datetime NOT NULL,
  `id_usuario_creacion` int NOT NULL,
  `id_usuario_actualizacion` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `materiales_apoyo`
--

INSERT INTO `materiales_apoyo` (`id_materiales_apoyo_pk`, `nombre`, `guia_aprendizaje`, `palabras_clave`, `url`, `fecha_creacion`, `fecha_actualizacion`, `id_usuario_creacion`, `id_usuario_actualizacion`) VALUES
(137, 'Material_1', 232, 'js', '../archivos/materialApoyo/Material_1_232.pdf', '2021-06-04 00:00:00', '2021-06-04 00:00:00', 1, 1),
(138, 'Material_2', 231, 'php', '../archivos/materialApoyo/Material_2_231.pdf', '2021-06-04 00:00:00', '2021-06-04 00:00:00', 1, 1),
(139, 'Materil_5', 233, 'kol', '../archivos/materialApoyo/Materil_5_233.pdf', '2021-06-04 00:00:00', '2021-06-04 00:00:00', 1, 1),
(141, 'Materiasl_33-23', 235, 'js,php,java', '../archivos/materialApoyo/Materiasl_33-23_235.pdf', '2021-06-04 00:00:00', '2021-06-04 00:00:00', 2, 2),
(142, 'Material_100', 236, 'js', '../archivos/materialApoyo/Material_100_236.pdf', '2021-06-07 00:00:00', '2021-06-07 00:00:00', 3, 3),
(143, 'Material_120', 237, 'java-js', '../archivos/materialApoyo/Material_120_237.pdf', '2021-06-07 00:00:00', '2021-06-07 00:00:00', 3, 3),
(144, 'Material_200', 238, 'java-js', '../archivos/materialApoyo/Material_200_238.pdf', '2021-06-07 00:00:00', '2021-06-08 00:00:00', 3, 3),
(149, 'material3212', 240, 'js', '../archivos/materialApoyo/material3212_240.pdf', '2021-06-15 00:00:00', '2021-06-15 00:00:00', 3, 3),
(150, 'material_33', 241, 'js', '../archivos/materialApoyo/material_33_241.pdf', '2021-06-15 00:00:00', '2021-06-15 00:00:00', 15, 15),
(151, 'Material_242', 242, 'js', '../archivos/materialApoyo/Material_242_242.pdf', '2021-06-16 00:00:00', '2021-06-16 00:00:00', 28, 28),
(152, 'materi77', 243, 'js', '../archivos/materialApoyo/materi77_243.pdf', '2021-06-16 00:00:00', '2021-06-16 00:00:00', 28, 28);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipios`
--

CREATE TABLE `municipios` (
  `id_municipio_pk` int NOT NULL,
  `nombre` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `departamento` int NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_actualizacion` datetime NOT NULL,
  `id_usuario_creacion` int NOT NULL,
  `id_usuario_actualizacion` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `municipios`
--

INSERT INTO `municipios` (`id_municipio_pk`, `nombre`, `departamento`, `fecha_creacion`, `fecha_actualizacion`, `id_usuario_creacion`, `id_usuario_actualizacion`) VALUES
(1, 'BOGOTA', 14, '2021-05-11 23:56:22', '2021-05-11 23:56:22', 1, 1),
(2, 'MEDELLIN', 2, '2021-05-11 23:57:43', '2021-05-11 23:57:43', 2, 2),
(3, 'CALI', 30, '2021-05-11 23:58:02', '2021-05-11 23:58:02', 3, 3),
(4, 'BARRANQUILLA', 4, '2021-05-11 23:58:22', '2021-05-11 23:58:22', 4, 4),
(5, 'CARTAGENA', 5, '2021-05-11 23:58:49', '2021-05-11 23:58:49', 5, 5),
(6, 'CUCUTA', 22, '2021-05-11 23:59:15', '2021-05-11 23:59:15', 6, 6),
(7, 'SOACHA', 14, '2021-05-11 23:59:39', '2021-05-11 23:59:39', 7, 7),
(8, 'SOLEDAD', 4, '2021-05-12 00:00:09', '2021-05-12 00:00:09', 8, 8),
(9, 'BUCARAMANGA', 27, '2021-05-12 00:00:41', '2021-05-12 00:00:41', 9, 9),
(10, 'BELLO', 2, '2021-05-12 00:01:06', '2021-05-12 00:01:07', 10, 10),
(11, 'VILLAVICENCIO', 20, '2021-05-12 00:01:27', '2021-05-12 00:01:27', 11, 11),
(12, 'IBAGUE', 29, '2021-05-12 00:02:06', '2021-05-12 00:02:06', 12, 12),
(13, 'SANTA MARTA', 19, '2021-05-12 00:02:35', '2021-05-12 00:02:35', 13, 13),
(14, 'VALLEDUPAR', 11, '2021-05-12 00:02:58', '2021-05-12 00:02:58', 14, 14),
(15, 'MONTERIA', 13, '2021-05-12 00:03:24', '2021-05-12 00:03:24', 15, 15),
(16, 'PEREIRA', 25, '2021-05-12 00:03:41', '2021-05-12 00:03:41', 16, 16),
(17, 'MANIZALES', 7, '2021-05-12 00:04:04', '2021-05-12 00:04:04', 17, 17),
(18, 'PASTO', 21, '2021-05-12 00:04:24', '2021-05-12 00:04:24', 18, 18),
(19, 'NEIVA', 17, '2021-05-12 00:04:45', '2021-05-12 00:04:45', 19, 19),
(20, 'PALMIRA', 30, '2021-05-12 00:05:07', '2021-05-12 00:05:07', 20, 20),
(21, 'POPAYAN', 10, '2021-05-12 00:05:33', '2021-05-12 00:05:33', 21, 21),
(22, 'BUENAVENTURA', 30, '2021-05-12 00:05:48', '2021-05-12 00:05:48', 22, 22),
(23, 'FLORIDABLANCA', 27, '2021-05-12 00:06:16', '2021-05-12 00:06:16', 23, 23),
(24, 'ARMENIA', 24, '2021-05-12 00:06:44', '2021-05-12 00:06:44', 24, 24),
(25, 'SINCELEJO', 28, '2021-05-12 00:07:10', '2021-05-12 00:07:10', 25, 25),
(26, 'ITAGUI', 2, '2021-05-12 00:07:37', '2021-05-12 00:07:37', 26, 26),
(27, 'TUMACO', 21, '2021-05-12 00:08:09', '2021-05-12 00:08:09', 27, 27),
(28, 'ENVIGADO', 2, '2021-05-12 00:08:37', '2021-05-12 00:08:37', 28, 28),
(29, 'DOSQUEBRADAS', 25, '2021-05-12 00:08:59', '2021-05-12 00:08:59', 29, 29),
(30, 'TULUA', 30, '2021-05-12 00:09:28', '2021-05-12 00:09:28', 30, 30),
(31, 'BARRANCABERMEJA', 27, '2021-05-12 00:09:48', '2021-05-12 00:09:48', 31, 31),
(32, 'RIOHACHA', 18, '2021-05-12 00:10:14', '2021-05-12 00:10:14', 32, 32),
(33, 'URIBIA', 18, '2021-05-12 00:16:45', '2021-05-12 00:16:45', 33, 33),
(34, 'MAICAO', 18, '2021-05-12 00:17:00', '2021-05-12 00:17:00', 34, 34),
(35, 'PIEDECUESTA', 27, '2021-05-12 00:17:23', '2021-05-12 00:17:23', 35, 35),
(36, 'TUNJA', 6, '2021-05-12 00:17:55', '2021-05-12 00:17:55', 36, 36),
(37, 'YOPAL', 9, '2021-05-12 00:18:13', '2021-05-12 00:18:13', 37, 37),
(38, 'FLORENCIA', 8, '2021-05-12 00:18:49', '2021-05-12 00:18:49', 38, 38),
(39, 'GIRON', 27, '2021-05-12 00:19:05', '2021-05-12 00:19:05', 39, 39),
(40, 'JAMUNDI', 30, '2021-05-12 00:19:33', '2021-05-12 00:19:33', 40, 40),
(41, 'FACATATIVA', 14, '2021-05-12 00:19:54', '2021-05-12 00:19:54', 41, 41),
(42, 'FASAGASUGA', 14, '2021-05-12 00:20:22', '2021-05-12 00:20:22', 42, 42),
(43, 'MOSQUERA', 14, '2021-05-12 00:20:46', '2021-05-12 00:20:46', 43, 43),
(44, 'CHIA', 14, '2021-05-12 00:21:05', '2021-05-12 00:21:05', 44, 44),
(45, 'ZIPAQUIRA', 14, '2021-05-12 00:21:19', '2021-05-12 00:21:19', 45, 45),
(46, 'RIONEGRO', 2, '2021-05-12 00:21:43', '2021-05-12 00:21:43', 46, 46),
(47, 'MAGANGUE', 5, '2021-05-12 00:22:02', '2021-05-12 00:22:02', 47, 47),
(48, 'MALAMBO', 4, '2021-05-12 00:22:21', '2021-05-12 00:22:21', 48, 48),
(49, 'CARTAGO', 30, '2021-05-12 00:22:49', '2021-05-12 00:22:49', 49, 49),
(50, 'SOGAMOSO', 6, '2021-05-12 00:23:28', '2021-05-12 00:23:28', 50, 50),
(51, 'QUIBDO', 12, '2021-05-12 00:23:51', '2021-05-12 00:23:51', 51, 51),
(52, 'TURBO', 2, '2021-05-12 00:24:14', '2021-05-12 00:24:14', 52, 52),
(53, 'OCAÑA', 22, '2021-05-12 00:24:54', '2021-05-12 00:24:54', 53, 53),
(54, 'BUGA', 30, '2021-05-12 00:25:40', '2021-05-12 00:25:40', 54, 54),
(55, 'PITALITO', 17, '2021-05-12 00:26:05', '2021-05-12 00:26:05', 55, 55),
(56, 'APARTADO', 2, '2021-05-12 00:26:25', '2021-05-12 00:26:25', 56, 56),
(57, 'MADRID', 14, '2021-05-12 00:26:44', '2021-05-12 00:26:44', 57, 57),
(58, 'DUITAMA', 6, '2021-05-12 00:27:08', '2021-05-12 00:27:08', 58, 58),
(59, 'CIENAGA', 19, '2021-05-12 00:27:28', '2021-05-12 00:27:28', 59, 59),
(60, 'AGUACHICA', 11, '2021-05-12 00:27:47', '2021-05-12 00:27:47', 60, 60),
(61, 'IPIALES', 21, '2021-05-12 00:28:15', '2021-05-12 00:28:15', 61, 61),
(62, 'LORICA', 13, '2021-05-12 00:28:42', '2021-05-12 00:28:42', 62, 62),
(63, 'TURBACO', 5, '2021-05-12 00:29:03', '2021-05-12 00:29:03', 63, 63),
(64, 'SANTANDER DE QUILICHAO', 10, '2021-05-12 00:29:24', '2021-05-12 00:29:24', 64, 64),
(65, 'VILLA DEL ROSARIO', 22, '2021-05-12 00:30:11', '2021-05-12 00:30:11', 65, 65),
(66, 'SAHAGUN', 13, '2021-05-12 00:30:34', '2021-05-12 00:30:34', 66, 66),
(67, 'YUMBO', 30, '2021-05-12 00:31:00', '2021-05-12 00:31:00', 67, 67),
(68, 'GIRARDOT', 14, '2021-05-12 00:31:19', '2021-05-12 00:31:19', 68, 68),
(69, 'CERETE', 13, '2021-05-12 00:31:44', '2021-05-12 00:31:44', 69, 69),
(70, 'FUNZA', 14, '2021-05-12 00:32:14', '2021-05-12 00:32:14', 70, 70),
(71, 'SABANALARGA', 4, '2021-05-12 00:32:38', '2021-05-12 00:32:38', 71, 71),
(72, 'LOS PATIOS', 22, '2021-05-12 00:33:00', '2021-05-12 00:33:00', 72, 72),
(73, 'ARAUCA', 3, '2021-05-12 00:33:24', '2021-05-12 00:33:24', 73, 73),
(74, 'CAUCASIA', 2, '2021-05-12 00:33:40', '2021-05-12 00:33:40', 74, 74),
(75, 'TIERRALTA', 13, '2021-05-12 00:34:02', '2021-05-12 00:34:02', 75, 75),
(76, 'CANDELARIA', 30, '2021-05-12 00:34:28', '2021-05-12 00:34:28', 76, 76),
(77, 'MANAURE', 18, '2021-05-12 00:34:54', '2021-05-12 00:34:54', 77, 77),
(78, 'CAJICA', 14, '2021-05-12 00:35:13', '2021-05-12 00:35:13', 78, 78),
(79, 'ACACIAS', 20, '2021-05-12 00:35:29', '2021-05-12 00:35:29', 79, 79),
(80, 'SABANETA', 2, '2021-05-12 00:35:53', '2021-05-12 00:35:53', 80, 80),
(81, 'MONTELIBANO', 13, '2021-05-12 00:36:13', '2021-05-12 00:36:13', 81, 81),
(82, 'CALDAS', 2, '2021-05-12 00:36:48', '2021-05-12 00:36:48', 82, 82),
(83, 'COPACABANA', 2, '2021-05-12 00:37:13', '2021-05-12 00:37:13', 83, 83),
(84, 'SANTA ROSA DE CABAL', 25, '2021-05-12 00:37:37', '2021-05-12 00:37:37', 84, 84),
(85, 'CUMARIBO', 32, '2021-05-12 00:38:08', '2021-05-12 00:38:08', 85, 85),
(86, 'LA ESTRELLA', 2, '2021-05-12 00:38:35', '2021-05-12 00:38:35', 86, 86),
(87, 'CALARCA', 24, '2021-05-12 00:38:58', '2021-05-12 00:38:58', 87, 87),
(88, 'GARZON', 17, '2021-05-12 00:39:33', '2021-05-12 00:39:33', 88, 88),
(89, 'LA DORADA', 7, '2021-05-12 00:39:57', '2021-05-12 00:39:57', 89, 89),
(90, 'ZONA BANANERA', 19, '2021-05-12 00:40:15', '2021-05-12 00:40:15', 90, 90),
(91, 'ARJONA', 5, '2021-05-12 00:40:43', '2021-05-12 00:40:43', 91, 91),
(92, 'EL CARMEN DE BOLIVAR', 5, '2021-05-12 00:41:02', '2021-05-12 00:41:02', 92, 92),
(93, 'ESPINAL', 29, '2021-05-12 00:41:30', '2021-05-12 00:41:30', 93, 93),
(94, 'COROZAL', 28, '2021-05-12 00:41:52', '2021-05-12 00:41:52', 94, 94),
(95, 'GRANADA', 20, '2021-05-12 00:42:15', '2021-05-12 00:42:15', 95, 95),
(96, 'FUNDACION', 19, '2021-05-12 00:42:46', '2021-05-12 00:42:46', 96, 96),
(97, 'EL BANCO', 19, '2021-05-12 00:43:03', '2021-05-12 00:43:03', 97, 97),
(98, 'LA CEJA', 2, '2021-05-12 00:43:27', '2021-05-12 00:43:27', 98, 98),
(99, 'MARINILLA', 2, '2021-05-12 00:43:45', '2021-05-12 00:43:45', 99, 99),
(100, 'VILLAMARIA', 7, '2021-05-12 00:44:04', '2021-05-12 00:44:04', 100, 100),
(101, 'PUERTO ASIS', 23, '2021-05-12 00:44:40', '2021-05-12 00:44:40', 101, 101),
(102, 'BARANOA', 4, '2021-05-12 00:45:09', '2021-05-12 00:45:09', 102, 102),
(103, 'GALAPA', 4, '2021-05-12 00:45:28', '2021-05-12 00:45:28', 103, 103),
(104, 'PLANETA RICA', 13, '2021-05-12 00:45:52', '2021-05-12 00:45:52', 104, 104),
(105, 'AGUSTIN CODAZZI', 11, '2021-05-12 00:46:12', '2021-05-12 00:46:12', 105, 105),
(106, 'PLATO', 19, '2021-05-12 00:46:41', '2021-05-12 00:46:41', 106, 106),
(107, 'SARAVENA', 3, '2021-05-12 00:47:24', '2021-05-12 00:47:24', 107, 107),
(108, 'EL CARMEN DE VIBORAL', 2, '2021-05-12 00:47:46', '2021-05-12 00:47:46', 108, 108),
(109, 'LA PLATA', 17, '2021-05-12 00:48:07', '2021-05-12 00:48:07', 109, 109),
(110, 'SAN MARCOS', 28, '2021-05-12 00:48:28', '2021-05-12 00:48:28', 110, 110),
(111, 'CIENAGA DE ORO', 13, '2021-05-12 00:48:54', '2021-05-12 00:48:54', 111, 111),
(112, 'CHIGORODO', 2, '2021-05-12 00:49:54', '2021-05-12 00:49:54', 112, 112),
(113, 'SAN GIL', 27, '2021-05-12 00:50:27', '2021-05-12 00:50:27', 113, 113),
(114, 'MOCOA', 23, '2021-05-12 00:50:52', '2021-05-12 00:50:52', 114, 114),
(115, 'TIBU', 22, '2021-05-12 00:51:17', '2021-05-12 00:51:17', 115, 115),
(116, 'GUARNE', 2, '2021-05-12 00:51:36', '2021-05-12 00:51:36', 116, 116),
(117, 'FLORIDA', 30, '2021-05-12 00:51:55', '2021-05-12 00:51:55', 117, 117),
(118, 'CHIQUINQUIRA', 6, '2021-05-12 00:52:18', '2021-05-12 00:52:18', 118, 118),
(119, 'SAN ANDRES', 26, '2021-05-12 00:52:41', '2021-05-12 00:52:41', 119, 119),
(120, 'EL CERRITO', 30, '2021-05-12 00:53:04', '2021-05-12 00:53:04', 120, 120),
(121, 'BARBACOAS', 21, '2021-05-12 01:00:09', '2021-05-12 01:00:09', 121, 121),
(122, 'ARAUQUITA', 3, '2021-05-12 01:00:37', '2021-05-12 01:00:37', 122, 122),
(123, 'SAN JOSE DEL GUAVIARE', 16, '2021-05-12 01:00:59', '2021-05-12 01:00:59', 123, 123),
(124, 'RIOSUCIO', 12, '2021-05-12 01:01:24', '2021-05-12 01:01:24', 124, 124),
(125, 'TUCHIN', 13, '2021-05-12 01:01:43', '2021-05-12 01:01:43', 125, 125),
(126, 'GIRARDOTA', 2, '2021-05-12 01:02:14', '2021-05-12 01:02:14', 126, 126),
(127, 'BARBOSA', 2, '2021-05-12 01:02:34', '2021-05-12 01:02:34', 127, 127),
(128, 'PAMPLONA', 22, '2021-05-12 01:02:54', '2021-05-12 01:02:54', 128, 128),
(129, 'EL BAGRE', 2, '2021-05-12 01:03:18', '2021-05-12 01:03:18', 129, 129),
(130, 'EL TAMBO', 10, '2021-05-12 01:03:46', '2021-05-12 01:03:46', 130, 130),
(131, 'PUERTO COLOMBIA', 4, '2021-05-12 01:04:12', '2021-05-12 01:04:12', 131, 131),
(132, 'SAN PELAYO', 13, '2021-05-12 01:05:43', '2021-05-12 01:05:43', 132, 132),
(133, 'SAN VICENTE DEL CAGUAN', 8, '2021-05-12 01:06:08', '2021-05-12 01:06:08', 133, 133),
(134, 'CHINCHINA', 7, '2021-05-12 01:06:31', '2021-05-12 01:06:31', 134, 134),
(135, 'RIOSUCIO', 7, '2021-05-12 01:06:54', '2021-05-12 01:06:54', 135, 135),
(136, 'CAREPA', 2, '2021-05-12 01:07:22', '2021-05-12 01:07:22', 136, 136),
(137, 'SAN ONOFRE', 28, '2021-05-12 01:07:44', '2021-05-12 01:07:44', 137, 137),
(138, 'LA JAGUA DE IBIRICO', 11, '2021-05-12 01:08:06', '2021-05-12 01:08:06', 138, 138),
(139, 'CHAPARRAL', 29, '2021-05-12 01:08:37', '2021-05-12 01:08:37', 139, 139);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE `personas` (
  `id_persona_pk` int NOT NULL,
  `nombres` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `apellido` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `tipo_sangre` enum('O+','A+','A-','B+','B-','AB-','AB+') CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `tipo_documento` enum('CE','NIP','NIC','TI','CC') CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `numero_documento` int NOT NULL,
  `edad` int NOT NULL,
  `genero` enum('Femenino','Masculino') CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `telefono` varchar(11) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `correo` varchar(60) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `direccion` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_actualizacion` datetime NOT NULL,
  `id_usuario_creacion` int NOT NULL,
  `id_usuario_actualizacion` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`id_persona_pk`, `nombres`, `apellido`, `tipo_sangre`, `tipo_documento`, `numero_documento`, `edad`, `genero`, `telefono`, `correo`, `direccion`, `fecha_creacion`, `fecha_actualizacion`, `id_usuario_creacion`, `id_usuario_actualizacion`) VALUES
(31, 'jose', 'fortuna', 'O+', 'CC', 123456789, 19, 'Masculino', '31213463', 'joseFortuna@misena.edu.co', 'calle 100#10-108', '2021-06-15 00:00:00', '2021-06-15 00:00:00', 6, 6),
(53, 'Jhonn Faiber', 'Sanchez Cupitre', 'O+', 'CC', 1193081391, 19, 'Masculino', '3186064125', 'cupitre16@gmail.com', 'calle 100#10-108', '2021-06-16 00:00:00', '2021-06-16 00:00:00', 13, 28),
(54, 'Dora', 'Sanchez', 'O+', 'CC', 123456, 48, 'Femenino', '3124542421', 'dora12@gmail.com', 'calle 100#10-108', '2021-06-16 00:00:00', '2021-06-16 00:00:00', 28, 28);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `programas_formacion`
--

CREATE TABLE `programas_formacion` (
  `id_programas_formacion_pk` int NOT NULL,
  `codigo` int NOT NULL,
  `nombre` varchar(80) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `red_conocimiento` int NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_actualizacion` datetime NOT NULL,
  `id_usuario_creacion` int NOT NULL,
  `id_usuario_actualizacion` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `programas_formacion`
--

INSERT INTO `programas_formacion` (`id_programas_formacion_pk`, `codigo`, `nombre`, `red_conocimiento`, `fecha_creacion`, `fecha_actualizacion`, `id_usuario_creacion`, `id_usuario_actualizacion`) VALUES
(1, 22810600, 'TECNÒLOGO EN ANÀLISIS Y DESARROLLO DE SISTEMAS DE INFORMACIÒN', 2, '2021-05-13 15:55:58', '2021-06-13 00:00:00', 1, 2),
(2, 1490949, 'ELEMENTOS PARA LA ADMINISTRACIÒN DEPORTIVA', 19, '2021-05-13 16:05:45', '2021-05-13 16:05:45', 2, 2),
(3, 2329354, 'TÈCNICO EN COMERCIO INTERNACIONAL', 10, '2021-05-13 16:09:27', '2021-05-13 16:09:27', 3, 3),
(4, 949974, 'CONTEXTUALIZACIÒN DEL TURISMO COMUNITARIO', 18, '2021-05-13 16:10:41', '2021-05-13 16:10:41', 4, 4),
(5, 2322881, 'TECNÒLOGO EN MANTENIMIENTO ELECTROMECÀNICO DE EQUIPO PESADO', 24, '2021-05-13 16:11:51', '2021-05-13 16:11:51', 5, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `redes_conocimiento`
--

CREATE TABLE `redes_conocimiento` (
  `id_redes_conocimiento_pk` int NOT NULL,
  `nombre` varchar(60) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_actualizacion` datetime NOT NULL,
  `id_usuario_creacion` int NOT NULL,
  `id_usuario_actualizacion` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `redes_conocimiento`
--

INSERT INTO `redes_conocimiento` (`id_redes_conocimiento_pk`, `nombre`, `fecha_creacion`, `fecha_actualizacion`, `id_usuario_creacion`, `id_usuario_actualizacion`) VALUES
(1, 'RED DE ACTIVIDAD FISICA,RECREACION Y DEPORTE', '2021-05-12 01:13:31', '2021-05-12 01:13:31', 1, 1),
(2, 'RED DE ACUÍCOLA Y PESCA', '2021-05-12 01:14:16', '2021-05-12 01:14:16', 2, 2),
(3, 'RED DE AEROESPACIAL', '2021-05-12 01:15:39', '2021-05-12 01:15:39', 3, 3),
(4, 'RED DE AGRÍCOLA', '2021-05-12 01:16:51', '2021-05-12 01:16:51', 4, 4),
(5, 'RED DE AMBIENTAL', '2021-05-12 01:17:12', '2021-05-12 01:17:12', 5, 5),
(6, 'RED DE ARTES GRÁFICAS', '2021-05-12 01:17:31', '2021-05-12 01:17:31', 6, 6),
(7, 'RED DE ARTESANÍAS', '2021-05-12 01:17:56', '2021-05-12 01:17:56', 7, 7),
(8, 'RED DE AUTOMOTOR', '2021-05-12 01:18:24', '2021-05-12 01:18:24', 8, 8),
(9, 'RED DE BIOTECNOLOGÍA', '2021-05-12 01:18:46', '2021-05-12 01:18:46', 9, 9),
(10, 'RED DE COMERCIO Y VENTAS', '2021-05-12 01:19:06', '2021-05-12 01:19:06', 10, 10),
(11, 'RED DE CONSTRUCCIÓN', '2021-05-12 01:19:32', '2021-05-12 01:19:32', 11, 11),
(12, 'RED DE CUERO, CALZADO Y MARROQUINERÍA', '2021-05-12 01:19:55', '2021-05-12 01:19:55', 12, 12),
(13, 'RED DE CULTURA', '2021-05-12 01:20:17', '2021-05-12 01:20:17', 13, 13),
(14, 'RED DE ELECTRÓNICA Y AUTOMATIZACIÓN', '2021-05-12 01:20:42', '2021-05-12 01:20:42', 14, 14),
(15, 'RED DE ENERGÍA ELÉCTRICA', '2021-05-12 01:21:01', '2021-05-12 01:21:01', 15, 15),
(16, 'RED DE GESTIÓN ADMINISTRATIVA Y FINANCIERA', '2021-05-12 01:21:20', '2021-05-12 01:21:20', 16, 16),
(17, 'RED DE HIDROCARBUROS', '2021-05-12 01:21:40', '2021-05-12 01:21:40', 17, 17),
(18, 'RED DE HOTELERÍA Y TURISMO', '2021-05-12 01:21:56', '2021-05-12 01:21:56', 18, 18),
(19, 'RED DE INFORMÁTICA, DISEÑO Y DESARROLLO DE SOFTWARE', '2021-05-12 01:22:13', '2021-05-12 01:22:13', 19, 19),
(20, 'RED DE INFRAESTRUCTURA', '2021-05-12 01:22:36', '2021-05-12 01:22:36', 20, 20),
(21, 'RED DE LOGÍSTICA, GESTIÓN DE LA PRODUCCIÓN', '2021-05-12 01:22:54', '2021-05-12 01:22:54', 21, 21),
(22, 'RED DE MATERIALES PARA LA INDUSTRIA', '2021-05-12 01:23:14', '2021-05-12 01:23:14', 22, 22),
(23, 'RED DE MECÁNICA INDUSTRIAL', '2021-05-12 01:23:32', '2021-05-12 01:23:32', 23, 23),
(24, 'RED DE MINERÍA', '2021-05-12 01:23:50', '2021-05-12 01:23:50', 24, 24),
(25, 'RED DE PECUARIA', '2021-05-12 01:24:08', '2021-05-12 01:24:08', 25, 25),
(26, 'RED DE QUÍMICA APLICADA', '2021-05-12 01:24:27', '2021-05-12 01:24:27', 26, 26),
(27, 'RED DE SALUD', '2021-05-12 01:24:44', '2021-05-12 01:24:44', 27, 27),
(28, 'RED DE SERVICIOS PERSONALES', '2021-05-12 01:24:59', '2021-05-12 01:24:59', 28, 28),
(29, 'RED DE TELECOMUNICACIONES', '2021-05-12 01:25:17', '2021-05-12 01:25:17', 29, 29),
(30, 'RED DE TEXTIL, CONFECCIÓN, DISEÑO Y MODA', '2021-05-12 01:25:42', '2021-05-12 01:25:42', 30, 30),
(31, 'RED DE TRANSPORTE', '2021-05-12 01:26:01', '2021-05-12 01:26:01', 31, 31);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id_rol_pk` int NOT NULL,
  `nombre` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_actualizacion` datetime NOT NULL,
  `id_usuario_creacion` int NOT NULL,
  `id_usuario_actualizacion` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id_rol_pk`, `nombre`, `fecha_creacion`, `fecha_actualizacion`, `id_usuario_creacion`, `id_usuario_actualizacion`) VALUES
(1, 'Administrador', '2021-05-13 17:05:34', '2021-05-13 17:05:34', 1, 1),
(2, 'Instructor', '2021-05-13 17:05:47', '2021-05-13 17:05:47', 2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitudes`
--

CREATE TABLE `solicitudes` (
  `id_solicitud_pk` int NOT NULL,
  `persona_fk` int NOT NULL,
  `asunto` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `descripcion` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `estado` enum('En Proceso','Aprovado') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_actualizacion` datetime NOT NULL,
  `id_usuario_creacion` int NOT NULL,
  `id_usuario_actualizacion` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario_pk` int NOT NULL,
  `usuario` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `password` varchar(60) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `persona` int NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_actualizacion` datetime NOT NULL,
  `id_usuario_creacion` int NOT NULL,
  `id_usuario_actualizacion` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario_pk`, `usuario`, `password`, `persona`, `fecha_creacion`, `fecha_actualizacion`, `id_usuario_creacion`, `id_usuario_actualizacion`) VALUES
(13, 'joseFortuna@misena.edu.co', '$2y$10$NEg.FX4hJmtqwgkjEAVeWu1TPx/l3/y.iex606F4.mmAK3i56qb8e', 31, '2021-06-15 00:00:00', '2021-06-15 00:00:00', 6, 6),
(28, 'cupitre16@gmail.com', '$2y$10$VtzSNEUfwKT.cx91TB8kwOCF3WQO4JLvUrSD0hHHfWkZFArblTLvC', 53, '2021-06-16 00:00:00', '2021-06-16 00:00:00', 13, 28),
(29, 'dora12@gmail.com', '$2y$10$2uIC81KRT83LsSdUN3YONOOMb9QHwVW3AtqsHDQx5lWbzuoQF2RzK', 54, '2021-06-16 00:00:00', '2021-06-16 00:00:00', 28, 28);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_roles`
--

CREATE TABLE `usuario_roles` (
  `id_usuario_roles_pk` int NOT NULL,
  `usuarios` int NOT NULL,
  `roles` int NOT NULL,
  `estado` enum('ACTIVO','INACTIVO') CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_actualizacion` datetime NOT NULL,
  `id_usuario_creacion` int NOT NULL,
  `id_usuario_actualizacion` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `usuario_roles`
--

INSERT INTO `usuario_roles` (`id_usuario_roles_pk`, `usuarios`, `roles`, `estado`, `fecha_creacion`, `fecha_actualizacion`, `id_usuario_creacion`, `id_usuario_actualizacion`) VALUES
(27, 13, 1, 'ACTIVO', '2021-06-16 13:44:40', '2021-06-16 13:44:40', 13, 13),
(29, 28, 2, 'ACTIVO', '2021-06-16 00:00:00', '2021-06-16 00:00:00', 13, 13),
(30, 29, 2, 'ACTIVO', '2021-06-16 00:00:00', '2021-06-16 00:00:00', 28, 28);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `centros_formacion`
--
ALTER TABLE `centros_formacion`
  ADD PRIMARY KEY (`id_centros_formacion_pk`),
  ADD KEY `centros_formacion_municipios_fk` (`municipio`);

--
-- Indices de la tabla `competencias`
--
ALTER TABLE `competencias`
  ADD PRIMARY KEY (`id_competencia_pk`),
  ADD KEY `competencias_programas_formacion_fk` (`programa`);

--
-- Indices de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  ADD PRIMARY KEY (`id_departamento_pk`);

--
-- Indices de la tabla `fichas`
--
ALTER TABLE `fichas`
  ADD PRIMARY KEY (`id_ficha_pk`),
  ADD KEY `fichas_programas_formacion_fk` (`programa`),
  ADD KEY `fichas_centros_formacion_fk` (`centro_formacion`);

--
-- Indices de la tabla `guias_aprendizaje`
--
ALTER TABLE `guias_aprendizaje`
  ADD PRIMARY KEY (`id_guias_aprendizaje_pk`),
  ADD KEY `guias_aprendizaje_fichas_fk` (`ficha`),
  ADD KEY `guias_aprendizaje_competencias_fk` (`competencia`);

--
-- Indices de la tabla `instructores`
--
ALTER TABLE `instructores`
  ADD PRIMARY KEY (`id_instructor_pk`),
  ADD KEY `instructores_personas_fk` (`persona`),
  ADD KEY `instructores_redes_conocimiento_fk` (`red_conocimiento`);

--
-- Indices de la tabla `instructores_fichas`
--
ALTER TABLE `instructores_fichas`
  ADD PRIMARY KEY (`id_instructores_fichas_pk`),
  ADD KEY `instructores_fichas_instructor_fk` (`instructor`),
  ADD KEY `instructores_fichas_ficha_fk` (`ficha`);

--
-- Indices de la tabla `materiales_apoyo`
--
ALTER TABLE `materiales_apoyo`
  ADD PRIMARY KEY (`id_materiales_apoyo_pk`),
  ADD KEY `materiales_apoyo_guias_aprendizaje_fk` (`guia_aprendizaje`);

--
-- Indices de la tabla `municipios`
--
ALTER TABLE `municipios`
  ADD PRIMARY KEY (`id_municipio_pk`),
  ADD KEY `municipios_departamentos_fk` (`departamento`);

--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`id_persona_pk`),
  ADD UNIQUE KEY `numero_documento` (`numero_documento`),
  ADD UNIQUE KEY `correo` (`correo`);

--
-- Indices de la tabla `programas_formacion`
--
ALTER TABLE `programas_formacion`
  ADD PRIMARY KEY (`id_programas_formacion_pk`),
  ADD KEY `programas_formacion_redes_conocimiento_fk` (`red_conocimiento`);

--
-- Indices de la tabla `redes_conocimiento`
--
ALTER TABLE `redes_conocimiento`
  ADD PRIMARY KEY (`id_redes_conocimiento_pk`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id_rol_pk`);

--
-- Indices de la tabla `solicitudes`
--
ALTER TABLE `solicitudes`
  ADD PRIMARY KEY (`id_solicitud_pk`),
  ADD KEY `solicitudes_instructores_fk` (`persona_fk`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario_pk`),
  ADD KEY `usuarios_personas_fk` (`persona`);

--
-- Indices de la tabla `usuario_roles`
--
ALTER TABLE `usuario_roles`
  ADD PRIMARY KEY (`id_usuario_roles_pk`),
  ADD KEY `usuario_roles_usuarios_fk` (`usuarios`),
  ADD KEY `usuario_roles_roles_fk` (`roles`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `centros_formacion`
--
ALTER TABLE `centros_formacion`
  MODIFY `id_centros_formacion_pk` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `competencias`
--
ALTER TABLE `competencias`
  MODIFY `id_competencia_pk` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  MODIFY `id_departamento_pk` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `fichas`
--
ALTER TABLE `fichas`
  MODIFY `id_ficha_pk` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `guias_aprendizaje`
--
ALTER TABLE `guias_aprendizaje`
  MODIFY `id_guias_aprendizaje_pk` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=245;

--
-- AUTO_INCREMENT de la tabla `instructores`
--
ALTER TABLE `instructores`
  MODIFY `id_instructor_pk` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `instructores_fichas`
--
ALTER TABLE `instructores_fichas`
  MODIFY `id_instructores_fichas_pk` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `materiales_apoyo`
--
ALTER TABLE `materiales_apoyo`
  MODIFY `id_materiales_apoyo_pk` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=153;

--
-- AUTO_INCREMENT de la tabla `municipios`
--
ALTER TABLE `municipios`
  MODIFY `id_municipio_pk` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=140;

--
-- AUTO_INCREMENT de la tabla `personas`
--
ALTER TABLE `personas`
  MODIFY `id_persona_pk` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT de la tabla `programas_formacion`
--
ALTER TABLE `programas_formacion`
  MODIFY `id_programas_formacion_pk` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `redes_conocimiento`
--
ALTER TABLE `redes_conocimiento`
  MODIFY `id_redes_conocimiento_pk` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id_rol_pk` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `solicitudes`
--
ALTER TABLE `solicitudes`
  MODIFY `id_solicitud_pk` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario_pk` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de la tabla `usuario_roles`
--
ALTER TABLE `usuario_roles`
  MODIFY `id_usuario_roles_pk` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `centros_formacion`
--
ALTER TABLE `centros_formacion`
  ADD CONSTRAINT `centros_formacion_municipios_fk` FOREIGN KEY (`municipio`) REFERENCES `municipios` (`id_municipio_pk`);

--
-- Filtros para la tabla `competencias`
--
ALTER TABLE `competencias`
  ADD CONSTRAINT `competencias_programas_formacion_fk` FOREIGN KEY (`programa`) REFERENCES `programas_formacion` (`id_programas_formacion_pk`);

--
-- Filtros para la tabla `fichas`
--
ALTER TABLE `fichas`
  ADD CONSTRAINT `fichas_centros_formacion_fk` FOREIGN KEY (`centro_formacion`) REFERENCES `centros_formacion` (`id_centros_formacion_pk`),
  ADD CONSTRAINT `fichas_programas_formacion_fk` FOREIGN KEY (`programa`) REFERENCES `programas_formacion` (`id_programas_formacion_pk`);

--
-- Filtros para la tabla `guias_aprendizaje`
--
ALTER TABLE `guias_aprendizaje`
  ADD CONSTRAINT `guias_aprendizaje_competencias_fk` FOREIGN KEY (`competencia`) REFERENCES `competencias` (`id_competencia_pk`),
  ADD CONSTRAINT `guias_aprendizaje_fichas_fk` FOREIGN KEY (`ficha`) REFERENCES `fichas` (`id_ficha_pk`);

--
-- Filtros para la tabla `instructores`
--
ALTER TABLE `instructores`
  ADD CONSTRAINT `instructores_personas_fk` FOREIGN KEY (`persona`) REFERENCES `personas` (`id_persona_pk`),
  ADD CONSTRAINT `instructores_redes_conocimiento_fk` FOREIGN KEY (`red_conocimiento`) REFERENCES `redes_conocimiento` (`id_redes_conocimiento_pk`);

--
-- Filtros para la tabla `instructores_fichas`
--
ALTER TABLE `instructores_fichas`
  ADD CONSTRAINT `instructores_fichas_ficha_fk` FOREIGN KEY (`ficha`) REFERENCES `fichas` (`id_ficha_pk`),
  ADD CONSTRAINT `instructores_fichas_instructor_fk` FOREIGN KEY (`instructor`) REFERENCES `instructores` (`id_instructor_pk`);

--
-- Filtros para la tabla `materiales_apoyo`
--
ALTER TABLE `materiales_apoyo`
  ADD CONSTRAINT `materiales_apoyo_guias_aprendizaje_fk` FOREIGN KEY (`guia_aprendizaje`) REFERENCES `guias_aprendizaje` (`id_guias_aprendizaje_pk`);

--
-- Filtros para la tabla `municipios`
--
ALTER TABLE `municipios`
  ADD CONSTRAINT `municipios_departamentos_fk` FOREIGN KEY (`departamento`) REFERENCES `departamentos` (`id_departamento_pk`);

--
-- Filtros para la tabla `programas_formacion`
--
ALTER TABLE `programas_formacion`
  ADD CONSTRAINT `programas_formacion_redes_conocimiento_fk` FOREIGN KEY (`red_conocimiento`) REFERENCES `redes_conocimiento` (`id_redes_conocimiento_pk`);

--
-- Filtros para la tabla `solicitudes`
--
ALTER TABLE `solicitudes`
  ADD CONSTRAINT `solicitudes_instructores_fk` FOREIGN KEY (`persona_fk`) REFERENCES `personas` (`id_persona_pk`);

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_personas_fk` FOREIGN KEY (`persona`) REFERENCES `personas` (`id_persona_pk`);

--
-- Filtros para la tabla `usuario_roles`
--
ALTER TABLE `usuario_roles`
  ADD CONSTRAINT `usuario_roles_roles_fk` FOREIGN KEY (`roles`) REFERENCES `roles` (`id_rol_pk`),
  ADD CONSTRAINT `usuario_roles_usuarios_fk` FOREIGN KEY (`usuarios`) REFERENCES `usuarios` (`id_usuario_pk`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
