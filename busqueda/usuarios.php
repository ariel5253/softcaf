<?php
    require '../entorno/conexion.php';
    require '../modelo/administrador/usuarios.M.php';

    $arr = array();
    $contador = 0;
    $usuario = new Usuarios();
    $usuario->setUsuario($_REQUEST['term']);
    $usuario->consultarPorUsuario();
    $numeroRegistros = $usuario->conn->obtenerNumeroRegistros();
    while($row = $usuario->conn->obtenerObjeto()){
        $arr[$contador]['id_usuario'] = $row->id_usuario;
        $arr[$contador]['nombre_persona'] = $row->nombre_persona;
        $arr[$contador]['value'] = $row->usuario;
        $contador++;
    }
    echo json_encode($arr); 
?>