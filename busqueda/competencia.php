<?php
    require '../entorno/conexion.php';
    require '../modelo/administrador/competencias.M.php';

    $arr = array();
    $contador = 0;
    $com = new Competencias();
    $com->setNombre($_REQUEST['term']);
    $com->consultarPorNombre();
    $numeroRegistros = $com->conn->obtenerNumeroRegistros();
    while($row = $com->conn->obtenerObjeto()){
        $arr[$contador]['id_competencia'] = $row->id_competencia;
        $arr[$contador]['value'] = $row->nombre_competencia; 
        $contador++;
    }
    echo json_encode($arr); 
?>