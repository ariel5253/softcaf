<?php
    require '../entorno/conexion.php';
    require '../modelo/administrador/redesConocimiento.M.php';

    $arr = array();
    $contador = 0;
    $red = new RedesConocimiento();
    $red->setNombre($_REQUEST['term']);
    $red->consultarPorNombre();
    $numeroRegistros = $red->conn->obtenerNumeroRegistros();
    while($row = $red->conn->obtenerObjeto()){
        $arr[$contador]['id_red'] = $row->id_redes_conocimiento_pk;
        $arr[$contador]['value'] = $row->nombre;
        $contador++;
    }
    echo json_encode($arr); 
?>