<?php
    require '../entorno/conexion.php';
    require '../modelo/administrador/personas.M.php';

    $arr = array();
    $contador = 0;
    $persona = new Personas();
    $persona->setNombres($_REQUEST['term']);
    $persona->consultarPorNombre();
    $numeroRegistros = $persona->conn->obtenerNumeroRegistros();
    while($row = $persona->conn->obtenerObjeto()){
        $arr[$contador]['id_instructor'] = $row->id_instructor;
        $arr[$contador]['documento'] = $row->documento;
        $arr[$contador]['value'] = $row->nombre_persona;
        $contador++;
    }
    echo json_encode($arr); 
?>