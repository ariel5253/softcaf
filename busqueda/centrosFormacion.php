<?php
    require '../entorno/conexion.php';
    require '../modelo/administrador/centrosFormacion.M.php';

    $arr = array();
    $contador = 0;
    $centro = new CentrosFormacion();
    $centro->setNombre($_REQUEST['term']);
    $centro->consultarPorNombre();
    $numeroRegistros = $centro->conn->obtenerNumeroRegistros();
    while($row = $centro->conn->obtenerObjeto()){
        $arr[$contador]['id_centro'] = $row->id_centros_formacion_pk ;
        $arr[$contador]['value'] = $row->nombre;
        $contador++;
    }
    echo json_encode($arr); 
?>