<?php
    require '../entorno/conexion.php';
    require '../modelo/administrador/personas.M.php';

    $arr = array();
    $contador = 0;
    $persona = new Personas();
    $persona->setNumeroDocumento($_REQUEST['term']);
    $persona->consultarPorDocumento();
    $numeroRegistros = $persona->conn->obtenerNumeroRegistros();
    while($row = $persona->conn->obtenerObjeto()){
        $arr[$contador]['id_instructor'] = $row->id_instructor;
        $arr[$contador]['nombre_persona'] = $row->nombre_persona;
        $arr[$contador]['value'] = $row->documento;
        $contador++;
    }
    echo json_encode($arr); 
?>