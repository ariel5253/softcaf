<?php
    require '../entorno/conexion.php';
    require '../modelo/administrador/programasFormacion.M.php';

    $arr = array();
    $contador = 0;
    $programa = new ProgramasFormacion();
    $programa->setNombre($_REQUEST['term']);
    $programa->consultarPorNombre();
    $numeroRegistros = $programa->conn->obtenerNumeroRegistros();
    while($row = $programa->conn->obtenerObjeto()){
        $arr[$contador]['id_programa'] = $row->id_programas_formacion_pk;
        $arr[$contador]['value'] = $row->nombre;
        $contador++;
    }
    echo json_encode($arr); 
?>