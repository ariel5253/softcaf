<?php
    require '../entorno/conexion.php';
    require '../modelo/administrador/municipios.M.php';

    $arrMunicipio = array();
    $contador = 0;
    $municipio = new Municipios();
    $municipio->setNombre($_REQUEST['term']);
    $municipio->consultarPorNombre();
    $numeroRegistros = $municipio->conn->obtenerNumeroRegistros();
    while($row = $municipio->conn->obtenerObjeto()){
        $arrMunicipio[$contador]['id_municipio'] = $row->id_municipio_pk;
        $arrMunicipio[$contador]['value'] = $row->nombre;
        $contador++;
    }
    echo json_encode($arrMunicipio); 
?>
