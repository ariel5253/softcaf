<?php
    require '../entorno/conexion.php';
    require '../modelo/administrador/fichas.M.php';

    $arr = array();
    $contador = 0;
    $ficha = new Fichas();
    $ficha->setCodigo($_REQUEST['term']);
    $ficha->consultarPorCodigo();
    $numeroRegistros = $ficha->conn->obtenerNumeroRegistros();
    while($row = $ficha->conn->obtenerObjeto()){
        $arr[$contador]['id_ficha'] = $row->id_ficha;
        $arr[$contador]['nombre_red'] = $row->nombre_red;
        $arr[$contador]['nombre_programa'] = $row->nombre_programa;
        $arr[$contador]['value'] = $row->numero_ficha; 
        $contador++;
    }
    echo json_encode($arr); 
?>